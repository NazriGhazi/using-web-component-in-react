import "reflect-metadata";
import "zone.js";

declare namespace JSX {
  interface IntrinsicElements {
    [elemName: string]: any;
  }
}
