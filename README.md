This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## About this project

this project use the bundle js (web components) from angular. The js file can be found in folder 'public'.

Import the script in index.html and add the custom element into index.html / App.js

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm start
```

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.
