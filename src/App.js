import React from "react";
// import logo from "./logo.svg";
import "./App.css";

function App() {
  return (
    <div className="App">
      <ele-navbar></ele-navbar>
      <ele-header></ele-header>
      <ele-footer></ele-footer>
    </div>
  );
}

export default App;
