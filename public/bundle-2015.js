(window.webpackJsonp = window.webpackJsonp || []).push([
  [1],
  {
    0: function (e, t, n) {
      e.exports = n("zUnb");
    },
    zUnb: function (e, t, n) {
      "use strict";
      function r(e) {
        return "function" == typeof e;
      }
      n.r(t);
      let s = !1;
      const o = {
        Promise: void 0,
        set useDeprecatedSynchronousErrorHandling(e) {
          if (e) {
            const e = new Error();
            console.warn(
              "DEPRECATED! RxJS was set to use deprecated synchronous error handling behavior by code at: \n" +
                e.stack
            );
          } else
            s &&
              console.log(
                "RxJS: Back to a better error behavior. Thank you. <3"
              );
          s = e;
        },
        get useDeprecatedSynchronousErrorHandling() {
          return s;
        },
      };
      function i(e) {
        setTimeout(() => {
          throw e;
        }, 0);
      }
      const l = {
          closed: !0,
          next(e) {},
          error(e) {
            if (o.useDeprecatedSynchronousErrorHandling) throw e;
            i(e);
          },
          complete() {},
        },
        a = (() =>
          Array.isArray || ((e) => e && "number" == typeof e.length))();
      function c(e) {
        return null !== e && "object" == typeof e;
      }
      const u = (() => {
        function e(e) {
          return (
            Error.call(this),
            (this.message = e
              ? `${e.length} errors occurred during unsubscription:\n${e
                  .map((e, t) => `${t + 1}) ${e.toString()}`)
                  .join("\n  ")}`
              : ""),
            (this.name = "UnsubscriptionError"),
            (this.errors = e),
            this
          );
        }
        return (e.prototype = Object.create(Error.prototype)), e;
      })();
      let h = (() => {
        class e {
          constructor(e) {
            (this.closed = !1),
              (this._parentOrParents = null),
              (this._subscriptions = null),
              e && (this._unsubscribe = e);
          }
          unsubscribe() {
            let t;
            if (this.closed) return;
            let {
              _parentOrParents: n,
              _unsubscribe: s,
              _subscriptions: o,
            } = this;
            if (
              ((this.closed = !0),
              (this._parentOrParents = null),
              (this._subscriptions = null),
              n instanceof e)
            )
              n.remove(this);
            else if (null !== n)
              for (let e = 0; e < n.length; ++e) n[e].remove(this);
            if (r(s))
              try {
                s.call(this);
              } catch (i) {
                t = i instanceof u ? d(i.errors) : [i];
              }
            if (a(o)) {
              let e = -1,
                n = o.length;
              for (; ++e < n; ) {
                const n = o[e];
                if (c(n))
                  try {
                    n.unsubscribe();
                  } catch (i) {
                    (t = t || []),
                      i instanceof u ? (t = t.concat(d(i.errors))) : t.push(i);
                  }
              }
            }
            if (t) throw new u(t);
          }
          add(t) {
            let n = t;
            if (!t) return e.EMPTY;
            switch (typeof t) {
              case "function":
                n = new e(t);
              case "object":
                if (
                  n === this ||
                  n.closed ||
                  "function" != typeof n.unsubscribe
                )
                  return n;
                if (this.closed) return n.unsubscribe(), n;
                if (!(n instanceof e)) {
                  const t = n;
                  (n = new e()), (n._subscriptions = [t]);
                }
                break;
              default:
                throw new Error(
                  "unrecognized teardown " + t + " added to Subscription."
                );
            }
            let { _parentOrParents: r } = n;
            if (null === r) n._parentOrParents = this;
            else if (r instanceof e) {
              if (r === this) return n;
              n._parentOrParents = [r, this];
            } else {
              if (-1 !== r.indexOf(this)) return n;
              r.push(this);
            }
            const s = this._subscriptions;
            return null === s ? (this._subscriptions = [n]) : s.push(n), n;
          }
          remove(e) {
            const t = this._subscriptions;
            if (t) {
              const n = t.indexOf(e);
              -1 !== n && t.splice(n, 1);
            }
          }
        }
        return (
          (e.EMPTY = (function (e) {
            return (e.closed = !0), e;
          })(new e())),
          e
        );
      })();
      function d(e) {
        return e.reduce((e, t) => e.concat(t instanceof u ? t.errors : t), []);
      }
      const p = (() =>
        "function" == typeof Symbol
          ? Symbol("rxSubscriber")
          : "@@rxSubscriber_" + Math.random())();
      class f extends h {
        constructor(e, t, n) {
          switch (
            (super(),
            (this.syncErrorValue = null),
            (this.syncErrorThrown = !1),
            (this.syncErrorThrowable = !1),
            (this.isStopped = !1),
            arguments.length)
          ) {
            case 0:
              this.destination = l;
              break;
            case 1:
              if (!e) {
                this.destination = l;
                break;
              }
              if ("object" == typeof e) {
                e instanceof f
                  ? ((this.syncErrorThrowable = e.syncErrorThrowable),
                    (this.destination = e),
                    e.add(this))
                  : ((this.syncErrorThrowable = !0),
                    (this.destination = new m(this, e)));
                break;
              }
            default:
              (this.syncErrorThrowable = !0),
                (this.destination = new m(this, e, t, n));
          }
        }
        [p]() {
          return this;
        }
        static create(e, t, n) {
          const r = new f(e, t, n);
          return (r.syncErrorThrowable = !1), r;
        }
        next(e) {
          this.isStopped || this._next(e);
        }
        error(e) {
          this.isStopped || ((this.isStopped = !0), this._error(e));
        }
        complete() {
          this.isStopped || ((this.isStopped = !0), this._complete());
        }
        unsubscribe() {
          this.closed || ((this.isStopped = !0), super.unsubscribe());
        }
        _next(e) {
          this.destination.next(e);
        }
        _error(e) {
          this.destination.error(e), this.unsubscribe();
        }
        _complete() {
          this.destination.complete(), this.unsubscribe();
        }
        _unsubscribeAndRecycle() {
          const { _parentOrParents: e } = this;
          return (
            (this._parentOrParents = null),
            this.unsubscribe(),
            (this.closed = !1),
            (this.isStopped = !1),
            (this._parentOrParents = e),
            this
          );
        }
      }
      class m extends f {
        constructor(e, t, n, s) {
          let o;
          super(), (this._parentSubscriber = e);
          let i = this;
          r(t)
            ? (o = t)
            : t &&
              ((o = t.next),
              (n = t.error),
              (s = t.complete),
              t !== l &&
                ((i = Object.create(t)),
                r(i.unsubscribe) && this.add(i.unsubscribe.bind(i)),
                (i.unsubscribe = this.unsubscribe.bind(this)))),
            (this._context = i),
            (this._next = o),
            (this._error = n),
            (this._complete = s);
        }
        next(e) {
          if (!this.isStopped && this._next) {
            const { _parentSubscriber: t } = this;
            o.useDeprecatedSynchronousErrorHandling && t.syncErrorThrowable
              ? this.__tryOrSetError(t, this._next, e) && this.unsubscribe()
              : this.__tryOrUnsub(this._next, e);
          }
        }
        error(e) {
          if (!this.isStopped) {
            const { _parentSubscriber: t } = this,
              { useDeprecatedSynchronousErrorHandling: n } = o;
            if (this._error)
              n && t.syncErrorThrowable
                ? (this.__tryOrSetError(t, this._error, e), this.unsubscribe())
                : (this.__tryOrUnsub(this._error, e), this.unsubscribe());
            else if (t.syncErrorThrowable)
              n ? ((t.syncErrorValue = e), (t.syncErrorThrown = !0)) : i(e),
                this.unsubscribe();
            else {
              if ((this.unsubscribe(), n)) throw e;
              i(e);
            }
          }
        }
        complete() {
          if (!this.isStopped) {
            const { _parentSubscriber: e } = this;
            if (this._complete) {
              const t = () => this._complete.call(this._context);
              o.useDeprecatedSynchronousErrorHandling && e.syncErrorThrowable
                ? (this.__tryOrSetError(e, t), this.unsubscribe())
                : (this.__tryOrUnsub(t), this.unsubscribe());
            } else this.unsubscribe();
          }
        }
        __tryOrUnsub(e, t) {
          try {
            e.call(this._context, t);
          } catch (n) {
            if ((this.unsubscribe(), o.useDeprecatedSynchronousErrorHandling))
              throw n;
            i(n);
          }
        }
        __tryOrSetError(e, t, n) {
          if (!o.useDeprecatedSynchronousErrorHandling)
            throw new Error("bad call");
          try {
            t.call(this._context, n);
          } catch (r) {
            return o.useDeprecatedSynchronousErrorHandling
              ? ((e.syncErrorValue = r), (e.syncErrorThrown = !0), !0)
              : (i(r), !0);
          }
          return !1;
        }
        _unsubscribe() {
          const { _parentSubscriber: e } = this;
          (this._context = null),
            (this._parentSubscriber = null),
            e.unsubscribe();
        }
      }
      const y = (() =>
        ("function" == typeof Symbol && Symbol.observable) || "@@observable")();
      function g(e) {
        return e;
      }
      let _ = (() => {
        class e {
          constructor(e) {
            (this._isScalar = !1), e && (this._subscribe = e);
          }
          lift(t) {
            const n = new e();
            return (n.source = this), (n.operator = t), n;
          }
          subscribe(e, t, n) {
            const { operator: r } = this,
              s = (function (e, t, n) {
                if (e) {
                  if (e instanceof f) return e;
                  if (e[p]) return e[p]();
                }
                return e || t || n ? new f(e, t, n) : new f(l);
              })(e, t, n);
            if (
              (s.add(
                r
                  ? r.call(s, this.source)
                  : this.source ||
                    (o.useDeprecatedSynchronousErrorHandling &&
                      !s.syncErrorThrowable)
                  ? this._subscribe(s)
                  : this._trySubscribe(s)
              ),
              o.useDeprecatedSynchronousErrorHandling &&
                s.syncErrorThrowable &&
                ((s.syncErrorThrowable = !1), s.syncErrorThrown))
            )
              throw s.syncErrorValue;
            return s;
          }
          _trySubscribe(e) {
            try {
              return this._subscribe(e);
            } catch (t) {
              o.useDeprecatedSynchronousErrorHandling &&
                ((e.syncErrorThrown = !0), (e.syncErrorValue = t)),
                (function (e) {
                  for (; e; ) {
                    const { closed: t, destination: n, isStopped: r } = e;
                    if (t || r) return !1;
                    e = n && n instanceof f ? n : null;
                  }
                  return !0;
                })(e)
                  ? e.error(t)
                  : console.warn(t);
            }
          }
          forEach(e, t) {
            return new (t = b(t))((t, n) => {
              let r;
              r = this.subscribe(
                (t) => {
                  try {
                    e(t);
                  } catch (s) {
                    n(s), r && r.unsubscribe();
                  }
                },
                n,
                t
              );
            });
          }
          _subscribe(e) {
            const { source: t } = this;
            return t && t.subscribe(e);
          }
          [y]() {
            return this;
          }
          pipe(...e) {
            return 0 === e.length
              ? this
              : (0 === (t = e).length
                  ? g
                  : 1 === t.length
                  ? t[0]
                  : function (e) {
                      return t.reduce((e, t) => t(e), e);
                    })(this);
            var t;
          }
          toPromise(e) {
            return new (e = b(e))((e, t) => {
              let n;
              this.subscribe(
                (e) => (n = e),
                (e) => t(e),
                () => e(n)
              );
            });
          }
        }
        return (e.create = (t) => new e(t)), e;
      })();
      function b(e) {
        if ((e || (e = o.Promise || Promise), !e))
          throw new Error("no Promise impl found");
        return e;
      }
      const v = (() => {
        function e() {
          return (
            Error.call(this),
            (this.message = "object unsubscribed"),
            (this.name = "ObjectUnsubscribedError"),
            this
          );
        }
        return (e.prototype = Object.create(Error.prototype)), e;
      })();
      class w extends h {
        constructor(e, t) {
          super(),
            (this.subject = e),
            (this.subscriber = t),
            (this.closed = !1);
        }
        unsubscribe() {
          if (this.closed) return;
          this.closed = !0;
          const e = this.subject,
            t = e.observers;
          if (
            ((this.subject = null),
            !t || 0 === t.length || e.isStopped || e.closed)
          )
            return;
          const n = t.indexOf(this.subscriber);
          -1 !== n && t.splice(n, 1);
        }
      }
      class C extends f {
        constructor(e) {
          super(e), (this.destination = e);
        }
      }
      let x = (() => {
        class e extends _ {
          constructor() {
            super(),
              (this.observers = []),
              (this.closed = !1),
              (this.isStopped = !1),
              (this.hasError = !1),
              (this.thrownError = null);
          }
          [p]() {
            return new C(this);
          }
          lift(e) {
            const t = new E(this, this);
            return (t.operator = e), t;
          }
          next(e) {
            if (this.closed) throw new v();
            if (!this.isStopped) {
              const { observers: t } = this,
                n = t.length,
                r = t.slice();
              for (let s = 0; s < n; s++) r[s].next(e);
            }
          }
          error(e) {
            if (this.closed) throw new v();
            (this.hasError = !0), (this.thrownError = e), (this.isStopped = !0);
            const { observers: t } = this,
              n = t.length,
              r = t.slice();
            for (let s = 0; s < n; s++) r[s].error(e);
            this.observers.length = 0;
          }
          complete() {
            if (this.closed) throw new v();
            this.isStopped = !0;
            const { observers: e } = this,
              t = e.length,
              n = e.slice();
            for (let r = 0; r < t; r++) n[r].complete();
            this.observers.length = 0;
          }
          unsubscribe() {
            (this.isStopped = !0), (this.closed = !0), (this.observers = null);
          }
          _trySubscribe(e) {
            if (this.closed) throw new v();
            return super._trySubscribe(e);
          }
          _subscribe(e) {
            if (this.closed) throw new v();
            return this.hasError
              ? (e.error(this.thrownError), h.EMPTY)
              : this.isStopped
              ? (e.complete(), h.EMPTY)
              : (this.observers.push(e), new w(this, e));
          }
          asObservable() {
            const e = new _();
            return (e.source = this), e;
          }
        }
        return (e.create = (e, t) => new E(e, t)), e;
      })();
      class E extends x {
        constructor(e, t) {
          super(), (this.destination = e), (this.source = t);
        }
        next(e) {
          const { destination: t } = this;
          t && t.next && t.next(e);
        }
        error(e) {
          const { destination: t } = this;
          t && t.error && this.destination.error(e);
        }
        complete() {
          const { destination: e } = this;
          e && e.complete && this.destination.complete();
        }
        _subscribe(e) {
          const { source: t } = this;
          return t ? this.source.subscribe(e) : h.EMPTY;
        }
      }
      function k(e) {
        return e && "function" == typeof e.schedule;
      }
      class T extends f {
        constructor(e, t, n) {
          super(),
            (this.parent = e),
            (this.outerValue = t),
            (this.outerIndex = n),
            (this.index = 0);
        }
        _next(e) {
          this.parent.notifyNext(
            this.outerValue,
            e,
            this.outerIndex,
            this.index++,
            this
          );
        }
        _error(e) {
          this.parent.notifyError(e, this), this.unsubscribe();
        }
        _complete() {
          this.parent.notifyComplete(this), this.unsubscribe();
        }
      }
      const S = (e) => (t) => {
        for (let n = 0, r = e.length; n < r && !t.closed; n++) t.next(e[n]);
        t.complete();
      };
      function I() {
        return "function" == typeof Symbol && Symbol.iterator
          ? Symbol.iterator
          : "@@iterator";
      }
      const A = I();
      const O = (e) => {
        if (e && "function" == typeof e[y])
          return (
            (o = e),
            (e) => {
              const t = o[y]();
              if ("function" != typeof t.subscribe)
                throw new TypeError(
                  "Provided object does not correctly implement Symbol.observable"
                );
              return t.subscribe(e);
            }
          );
        if ((t = e) && "number" == typeof t.length && "function" != typeof t)
          return S(e);
        var t, n, r, s, o;
        if (
          (n = e) &&
          "function" != typeof n.subscribe &&
          "function" == typeof n.then
        )
          return (
            (s = e),
            (e) => (
              s
                .then(
                  (t) => {
                    e.closed || (e.next(t), e.complete());
                  },
                  (t) => e.error(t)
                )
                .then(null, i),
              e
            )
          );
        if (e && "function" == typeof e[A])
          return (
            (r = e),
            (e) => {
              const t = r[A]();
              for (;;) {
                const n = t.next();
                if (n.done) {
                  e.complete();
                  break;
                }
                if ((e.next(n.value), e.closed)) break;
              }
              return (
                "function" == typeof t.return &&
                  e.add(() => {
                    t.return && t.return();
                  }),
                e
              );
            }
          );
        {
          const t = c(e) ? "an invalid object" : `'${e}'`;
          throw new TypeError(
            `You provided ${t} where a stream was expected.` +
              " You can provide an Observable, Promise, Array, or Iterable."
          );
        }
      };
      class P extends f {
        notifyNext(e, t, n, r, s) {
          this.destination.next(t);
        }
        notifyError(e, t) {
          this.destination.error(e);
        }
        notifyComplete(e) {
          this.destination.complete();
        }
      }
      function N(e, t) {
        return function (n) {
          if ("function" != typeof e)
            throw new TypeError(
              "argument is not a function. Are you looking for `mapTo()`?"
            );
          return n.lift(new D(e, t));
        };
      }
      class D {
        constructor(e, t) {
          (this.project = e), (this.thisArg = t);
        }
        call(e, t) {
          return t.subscribe(new M(e, this.project, this.thisArg));
        }
      }
      class M extends f {
        constructor(e, t, n) {
          super(e),
            (this.project = t),
            (this.count = 0),
            (this.thisArg = n || this);
        }
        _next(e) {
          let t;
          try {
            t = this.project.call(this.thisArg, e, this.count++);
          } catch (n) {
            return void this.destination.error(n);
          }
          this.destination.next(t);
        }
      }
      function R(e, t) {
        return new _((n) => {
          const r = new h();
          let s = 0;
          return (
            r.add(
              t.schedule(function () {
                s !== e.length
                  ? (n.next(e[s++]), n.closed || r.add(this.schedule()))
                  : n.complete();
              })
            ),
            r
          );
        });
      }
      function j(e, t, n = Number.POSITIVE_INFINITY) {
        return "function" == typeof t
          ? (r) =>
              r.pipe(
                j((n, r) => {
                  return ((s = e(n, r)), s instanceof _ ? s : new _(O(s))).pipe(
                    N((e, s) => t(n, e, r, s))
                  );
                  var s;
                }, n)
              )
          : ("number" == typeof t && (n = t), (t) => t.lift(new H(e, n)));
      }
      class H {
        constructor(e, t = Number.POSITIVE_INFINITY) {
          (this.project = e), (this.concurrent = t);
        }
        call(e, t) {
          return t.subscribe(new F(e, this.project, this.concurrent));
        }
      }
      class F extends P {
        constructor(e, t, n = Number.POSITIVE_INFINITY) {
          super(e),
            (this.project = t),
            (this.concurrent = n),
            (this.hasCompleted = !1),
            (this.buffer = []),
            (this.active = 0),
            (this.index = 0);
        }
        _next(e) {
          this.active < this.concurrent
            ? this._tryNext(e)
            : this.buffer.push(e);
        }
        _tryNext(e) {
          let t;
          const n = this.index++;
          try {
            t = this.project(e, n);
          } catch (r) {
            return void this.destination.error(r);
          }
          this.active++, this._innerSub(t, e, n);
        }
        _innerSub(e, t, n) {
          const r = new T(this, t, n),
            s = this.destination;
          s.add(r);
          const o = (function (e, t, n, r, s = new T(e, n, r)) {
            if (!s.closed) return t instanceof _ ? t.subscribe(s) : O(t)(s);
          })(this, e, void 0, void 0, r);
          o !== r && s.add(o);
        }
        _complete() {
          (this.hasCompleted = !0),
            0 === this.active &&
              0 === this.buffer.length &&
              this.destination.complete(),
            this.unsubscribe();
        }
        notifyNext(e, t, n, r, s) {
          this.destination.next(t);
        }
        notifyComplete(e) {
          const t = this.buffer;
          this.remove(e),
            this.active--,
            t.length > 0
              ? this._next(t.shift())
              : 0 === this.active &&
                this.hasCompleted &&
                this.destination.complete();
        }
      }
      function V(e, t) {
        return t ? R(e, t) : new _(S(e));
      }
      function L(...e) {
        let t = Number.POSITIVE_INFINITY,
          n = null,
          r = e[e.length - 1];
        return (
          k(r)
            ? ((n = e.pop()),
              e.length > 1 &&
                "number" == typeof e[e.length - 1] &&
                (t = e.pop()))
            : "number" == typeof r && (t = e.pop()),
          null === n && 1 === e.length && e[0] instanceof _
            ? e[0]
            : (function (e = Number.POSITIVE_INFINITY) {
                return j(g, e);
              })(t)(V(e, n))
        );
      }
      function z() {
        return function (e) {
          return e.lift(new U(e));
        };
      }
      class U {
        constructor(e) {
          this.connectable = e;
        }
        call(e, t) {
          const { connectable: n } = this;
          n._refCount++;
          const r = new B(e, n),
            s = t.subscribe(r);
          return r.closed || (r.connection = n.connect()), s;
        }
      }
      class B extends f {
        constructor(e, t) {
          super(e), (this.connectable = t);
        }
        _unsubscribe() {
          const { connectable: e } = this;
          if (!e) return void (this.connection = null);
          this.connectable = null;
          const t = e._refCount;
          if (t <= 0) return void (this.connection = null);
          if (((e._refCount = t - 1), t > 1))
            return void (this.connection = null);
          const { connection: n } = this,
            r = e._connection;
          (this.connection = null), !r || (n && r !== n) || r.unsubscribe();
        }
      }
      class $ extends _ {
        constructor(e, t) {
          super(),
            (this.source = e),
            (this.subjectFactory = t),
            (this._refCount = 0),
            (this._isComplete = !1);
        }
        _subscribe(e) {
          return this.getSubject().subscribe(e);
        }
        getSubject() {
          const e = this._subject;
          return (
            (e && !e.isStopped) || (this._subject = this.subjectFactory()),
            this._subject
          );
        }
        connect() {
          let e = this._connection;
          return (
            e ||
              ((this._isComplete = !1),
              (e = this._connection = new h()),
              e.add(this.source.subscribe(new q(this.getSubject(), this))),
              e.closed && ((this._connection = null), (e = h.EMPTY))),
            e
          );
        }
        refCount() {
          return z()(this);
        }
      }
      const Z = (() => {
        const e = $.prototype;
        return {
          operator: { value: null },
          _refCount: { value: 0, writable: !0 },
          _subject: { value: null, writable: !0 },
          _connection: { value: null, writable: !0 },
          _subscribe: { value: e._subscribe },
          _isComplete: { value: e._isComplete, writable: !0 },
          getSubject: { value: e.getSubject },
          connect: { value: e.connect },
          refCount: { value: e.refCount },
        };
      })();
      class q extends C {
        constructor(e, t) {
          super(e), (this.connectable = t);
        }
        _error(e) {
          this._unsubscribe(), super._error(e);
        }
        _complete() {
          (this.connectable._isComplete = !0),
            this._unsubscribe(),
            super._complete();
        }
        _unsubscribe() {
          const e = this.connectable;
          if (e) {
            this.connectable = null;
            const t = e._connection;
            (e._refCount = 0),
              (e._subject = null),
              (e._connection = null),
              t && t.unsubscribe();
          }
        }
      }
      function W() {
        return new x();
      }
      function K(e) {
        return { toString: e }.toString();
      }
      function J(e, t, n) {
        return K(() => {
          const r = (function (e) {
            return function (...t) {
              if (e) {
                const n = e(...t);
                for (const e in n) this[e] = n[e];
              }
            };
          })(t);
          function s(...e) {
            if (this instanceof s) return r.apply(this, e), this;
            const t = new s(...e);
            return (n.annotation = t), n;
            function n(e, n, r) {
              const s = e.hasOwnProperty("__parameters__")
                ? e.__parameters__
                : Object.defineProperty(e, "__parameters__", { value: [] })
                    .__parameters__;
              for (; s.length <= r; ) s.push(null);
              return (s[r] = s[r] || []).push(t), e;
            }
          }
          return (
            n && (s.prototype = Object.create(n.prototype)),
            (s.prototype.ngMetadataName = e),
            (s.annotationCls = s),
            s
          );
        });
      }
      const Q = J("Inject", (e) => ({ token: e })),
        G = J("Optional"),
        X = J("Self"),
        Y = J("SkipSelf");
      var ee = (function (e) {
        return (
          (e[(e.Default = 0)] = "Default"),
          (e[(e.Host = 1)] = "Host"),
          (e[(e.Self = 2)] = "Self"),
          (e[(e.SkipSelf = 4)] = "SkipSelf"),
          (e[(e.Optional = 8)] = "Optional"),
          e
        );
      })({});
      function te(e) {
        for (let t in e) if (e[t] === te) return t;
        throw Error("Could not find renamed property on target object.");
      }
      function ne(e) {
        return {
          token: e.token,
          providedIn: e.providedIn || null,
          factory: e.factory,
          value: void 0,
        };
      }
      function re(e) {
        return {
          factory: e.factory,
          providers: e.providers || [],
          imports: e.imports || [],
        };
      }
      function se(e) {
        return oe(e, e[le]) || oe(e, e[ue]);
      }
      function oe(e, t) {
        return t && t.token === e ? t : null;
      }
      function ie(e) {
        return e && (e.hasOwnProperty(ae) || e.hasOwnProperty(he))
          ? e[ae]
          : null;
      }
      const le = te({ ɵprov: te }),
        ae = te({ ɵinj: te }),
        ce = te({ ɵprovFallback: te }),
        ue = te({ ngInjectableDef: te }),
        he = te({ ngInjectorDef: te });
      function de(e) {
        if ("string" == typeof e) return e;
        if (Array.isArray(e)) return "[" + e.map(de).join(", ") + "]";
        if (null == e) return "" + e;
        if (e.overriddenName) return `${e.overriddenName}`;
        if (e.name) return `${e.name}`;
        const t = e.toString();
        if (null == t) return "" + t;
        const n = t.indexOf("\n");
        return -1 === n ? t : t.substring(0, n);
      }
      function pe(e, t) {
        return null == e || "" === e
          ? null === t
            ? ""
            : t
          : null == t || "" === t
          ? e
          : e + " " + t;
      }
      const fe = te({ __forward_ref__: te });
      function me(e) {
        return (
          (e.__forward_ref__ = me),
          (e.toString = function () {
            return de(this());
          }),
          e
        );
      }
      function ye(e) {
        return "function" == typeof (t = e) &&
          t.hasOwnProperty(fe) &&
          t.__forward_ref__ === me
          ? e()
          : e;
        var t;
      }
      const ge = "undefined" != typeof globalThis && globalThis,
        _e = "undefined" != typeof window && window,
        be =
          "undefined" != typeof self &&
          "undefined" != typeof WorkerGlobalScope &&
          self instanceof WorkerGlobalScope &&
          self,
        ve = "undefined" != typeof global && global,
        we = ge || ve || _e || be,
        Ce = te({ ɵcmp: te }),
        xe = te({ ɵdir: te }),
        Ee = te({ ɵpipe: te }),
        ke = te({ ɵmod: te }),
        Te = te({ ɵloc: te }),
        Se = te({ ɵfac: te }),
        Ie = te({ __NG_ELEMENT_ID__: te });
      class Ae {
        constructor(e, t) {
          (this._desc = e),
            (this.ngMetadataName = "InjectionToken"),
            (this.ɵprov = void 0),
            "number" == typeof t
              ? (this.__NG_ELEMENT_ID__ = t)
              : void 0 !== t &&
                (this.ɵprov = ne({
                  token: this,
                  providedIn: t.providedIn || "root",
                  factory: t.factory,
                }));
        }
        toString() {
          return `InjectionToken ${this._desc}`;
        }
      }
      const Oe = new Ae("INJECTOR", -1),
        Pe = {},
        Ne = /\n/gm,
        De = te({ provide: String, useValue: te });
      let Me,
        Re = void 0;
      function je(e) {
        const t = Re;
        return (Re = e), t;
      }
      function He(e) {
        const t = Me;
        return (Me = e), t;
      }
      function Fe(e, t = ee.Default) {
        if (void 0 === Re)
          throw new Error("inject() must be called from an injection context");
        return null === Re
          ? Le(e, void 0, t)
          : Re.get(e, t & ee.Optional ? null : void 0, t);
      }
      function Ve(e, t = ee.Default) {
        return (Me || Fe)(ye(e), t);
      }
      function Le(e, t, n) {
        const r = se(e);
        if (r && "root" == r.providedIn)
          return void 0 === r.value ? (r.value = r.factory()) : r.value;
        if (n & ee.Optional) return null;
        if (void 0 !== t) return t;
        throw new Error(`Injector: NOT_FOUND [${de(e)}]`);
      }
      function ze(e) {
        const t = [];
        for (let n = 0; n < e.length; n++) {
          const r = ye(e[n]);
          if (Array.isArray(r)) {
            if (0 === r.length)
              throw new Error("Arguments array must have arguments.");
            let e = void 0,
              n = ee.Default;
            for (let t = 0; t < r.length; t++) {
              const s = r[t];
              s instanceof G || "Optional" === s.ngMetadataName || s === G
                ? (n |= ee.Optional)
                : s instanceof Y || "SkipSelf" === s.ngMetadataName || s === Y
                ? (n |= ee.SkipSelf)
                : s instanceof X || "Self" === s.ngMetadataName || s === X
                ? (n |= ee.Self)
                : (e = s instanceof Q || s === Q ? s.token : s);
            }
            t.push(Ve(e, n));
          } else t.push(Ve(r));
        }
        return t;
      }
      class Ue {
        get(e, t = Pe) {
          if (t === Pe) {
            const t = new Error(`NullInjectorError: No provider for ${de(e)}!`);
            throw ((t.name = "NullInjectorError"), t);
          }
          return t;
        }
      }
      class Be {}
      function $e(e, t) {
        e.forEach((e) => (Array.isArray(e) ? $e(e, t) : t(e)));
      }
      const Ze = (function () {
          var e = { OnPush: 0, Default: 1 };
          return (e[e.OnPush] = "OnPush"), (e[e.Default] = "Default"), e;
        })(),
        qe = (function () {
          var e = { Emulated: 0, Native: 1, None: 2, ShadowDom: 3 };
          return (
            (e[e.Emulated] = "Emulated"),
            (e[e.Native] = "Native"),
            (e[e.None] = "None"),
            (e[e.ShadowDom] = "ShadowDom"),
            e
          );
        })(),
        We = {},
        Ke = [];
      let Je = 0;
      function Qe(e) {
        return K(() => {
          const t = e.type,
            n = t.prototype,
            r = {},
            s = {
              type: t,
              providersResolver: null,
              decls: e.decls,
              vars: e.vars,
              factory: null,
              template: e.template || null,
              consts: e.consts || null,
              ngContentSelectors: e.ngContentSelectors,
              hostBindings: e.hostBindings || null,
              hostVars: e.hostVars || 0,
              hostAttrs: e.hostAttrs || null,
              contentQueries: e.contentQueries || null,
              declaredInputs: r,
              inputs: null,
              outputs: null,
              exportAs: e.exportAs || null,
              onChanges: null,
              onInit: n.ngOnInit || null,
              doCheck: n.ngDoCheck || null,
              afterContentInit: n.ngAfterContentInit || null,
              afterContentChecked: n.ngAfterContentChecked || null,
              afterViewInit: n.ngAfterViewInit || null,
              afterViewChecked: n.ngAfterViewChecked || null,
              onDestroy: n.ngOnDestroy || null,
              onPush: e.changeDetection === Ze.OnPush,
              directiveDefs: null,
              pipeDefs: null,
              selectors: e.selectors || Ke,
              viewQuery: e.viewQuery || null,
              features: e.features || null,
              data: e.data || {},
              encapsulation: e.encapsulation || qe.Emulated,
              id: "c",
              styles: e.styles || Ke,
              _: null,
              setInput: null,
              schemas: e.schemas || null,
              tView: null,
            },
            o = e.directives,
            i = e.features,
            l = e.pipes;
          return (
            (s.id += Je++),
            (s.inputs = tt(e.inputs, r)),
            (s.outputs = tt(e.outputs)),
            i && i.forEach((e) => e(s)),
            (s.directiveDefs = o
              ? () => ("function" == typeof o ? o() : o).map(Ge)
              : null),
            (s.pipeDefs = l
              ? () => ("function" == typeof l ? l() : l).map(Xe)
              : null),
            s
          );
        });
      }
      function Ge(e) {
        return (
          nt(e) ||
          (function (e) {
            return e[xe] || null;
          })(e)
        );
      }
      function Xe(e) {
        return (function (e) {
          return e[Ee] || null;
        })(e);
      }
      const Ye = {};
      function et(e) {
        const t = {
          type: e.type,
          bootstrap: e.bootstrap || Ke,
          declarations: e.declarations || Ke,
          imports: e.imports || Ke,
          exports: e.exports || Ke,
          transitiveCompileScopes: null,
          schemas: e.schemas || null,
          id: e.id || null,
        };
        return (
          null != e.id &&
            K(() => {
              Ye[e.id] = e.type;
            }),
          t
        );
      }
      function tt(e, t) {
        if (null == e) return We;
        const n = {};
        for (const r in e)
          if (e.hasOwnProperty(r)) {
            let s = e[r],
              o = s;
            Array.isArray(s) && ((o = s[1]), (s = s[0])),
              (n[s] = r),
              t && (t[s] = o);
          }
        return n;
      }
      function nt(e) {
        return e[Ce] || null;
      }
      function rt(e, t) {
        return e.hasOwnProperty(Se) ? e[Se] : null;
      }
      function st(e, t) {
        const n = e[ke] || null;
        if (!n && !0 === t)
          throw new Error(`Type ${de(e)} does not have '\u0275mod' property.`);
        return n;
      }
      function ot(e) {
        return Array.isArray(e) && "object" == typeof e[1];
      }
      function it(e) {
        return Array.isArray(e) && !0 === e[1];
      }
      function lt(e) {
        return 0 != (8 & e.flags);
      }
      function at(e) {
        return null !== e.template;
      }
      let ct = void 0;
      function ut(e) {
        return !!e.listen;
      }
      const ht = {
        createRenderer: (e, t) =>
          void 0 !== ct
            ? ct
            : "undefined" != typeof document
            ? document
            : void 0,
      };
      function dt(e) {
        for (; Array.isArray(e); ) e = e[0];
        return e;
      }
      function pt(e, t) {
        return dt(t[e.index]);
      }
      function ft(e, t) {
        const n = t[e];
        return ot(n) ? n : n[0];
      }
      function mt(e) {
        const t = (function (e) {
          return e.__ngContext__ || null;
        })(e);
        return t ? (Array.isArray(t) ? t : t.lView) : null;
      }
      function yt(e) {
        return 128 == (128 & e[2]);
      }
      function gt(e, t) {
        return null === e || null == t ? null : e[t];
      }
      function _t(e) {
        e[18] = 0;
      }
      function bt(e, t) {
        e[5] += t;
        let n = e,
          r = e[3];
        for (
          ;
          null !== r && ((1 === t && 1 === n[5]) || (-1 === t && 0 === n[5]));

        )
          (r[5] += t), (n = r), (r = r[3]);
      }
      const vt = {
        lFrame: Rt(null),
        bindingsEnabled: !0,
        checkNoChangesMode: !1,
      };
      function wt() {
        return vt.bindingsEnabled;
      }
      function Ct() {
        return vt.lFrame.lView;
      }
      function xt() {
        return vt.lFrame.tView;
      }
      function Et() {
        return vt.lFrame.previousOrParentTNode;
      }
      function kt(e, t) {
        (vt.lFrame.previousOrParentTNode = e), (vt.lFrame.isParent = t);
      }
      function Tt() {
        return vt.lFrame.isParent;
      }
      function St() {
        return vt.checkNoChangesMode;
      }
      function It(e) {
        vt.checkNoChangesMode = e;
      }
      function At(e, t) {
        const n = vt.lFrame;
        (n.bindingIndex = n.bindingRootIndex = e), Ot(t);
      }
      function Ot(e) {
        vt.lFrame.currentDirectiveIndex = e;
      }
      function Pt(e) {
        vt.lFrame.currentQueryIndex = e;
      }
      function Nt(e, t) {
        const n = Mt();
        (vt.lFrame = n), (n.previousOrParentTNode = t), (n.lView = e);
      }
      function Dt(e, t) {
        const n = Mt(),
          r = e[1];
        (vt.lFrame = n),
          (n.previousOrParentTNode = t),
          (n.lView = e),
          (n.tView = r),
          (n.contextLView = e),
          (n.bindingIndex = r.bindingStartIndex);
      }
      function Mt() {
        const e = vt.lFrame,
          t = null === e ? null : e.child;
        return null === t ? Rt(e) : t;
      }
      function Rt(e) {
        const t = {
          previousOrParentTNode: null,
          isParent: !0,
          lView: null,
          tView: null,
          selectedIndex: 0,
          contextLView: null,
          elementDepthCount: 0,
          currentNamespace: null,
          currentSanitizer: null,
          currentDirectiveIndex: -1,
          bindingRootIndex: -1,
          bindingIndex: -1,
          currentQueryIndex: 0,
          parent: e,
          child: null,
        };
        return null !== e && (e.child = t), t;
      }
      function jt() {
        const e = vt.lFrame;
        return (
          (vt.lFrame = e.parent),
          (e.previousOrParentTNode = null),
          (e.lView = null),
          e
        );
      }
      const Ht = jt;
      function Ft() {
        const e = jt();
        (e.isParent = !0),
          (e.tView = null),
          (e.selectedIndex = 0),
          (e.contextLView = null),
          (e.elementDepthCount = 0),
          (e.currentDirectiveIndex = -1),
          (e.currentNamespace = null),
          (e.currentSanitizer = null),
          (e.bindingRootIndex = -1),
          (e.bindingIndex = -1),
          (e.currentQueryIndex = 0);
      }
      function Vt(e) {
        vt.lFrame.selectedIndex = e;
      }
      function Lt(e, t) {
        for (let n = t.directiveStart, r = t.directiveEnd; n < r; n++) {
          const t = e.data[n];
          t.afterContentInit &&
            (e.contentHooks || (e.contentHooks = [])).push(
              -n,
              t.afterContentInit
            ),
            t.afterContentChecked &&
              ((e.contentHooks || (e.contentHooks = [])).push(
                n,
                t.afterContentChecked
              ),
              (e.contentCheckHooks || (e.contentCheckHooks = [])).push(
                n,
                t.afterContentChecked
              )),
            t.afterViewInit &&
              (e.viewHooks || (e.viewHooks = [])).push(-n, t.afterViewInit),
            t.afterViewChecked &&
              ((e.viewHooks || (e.viewHooks = [])).push(n, t.afterViewChecked),
              (e.viewCheckHooks || (e.viewCheckHooks = [])).push(
                n,
                t.afterViewChecked
              )),
            null != t.onDestroy &&
              (e.destroyHooks || (e.destroyHooks = [])).push(n, t.onDestroy);
        }
      }
      function zt(e, t, n) {
        $t(e, t, 3, n);
      }
      function Ut(e, t, n, r) {
        (3 & e[2]) === n && $t(e, t, n, r);
      }
      function Bt(e, t) {
        let n = e[2];
        (3 & n) === t && ((n &= 2047), (n += 1), (e[2] = n));
      }
      function $t(e, t, n, r) {
        const s = null != r ? r : -1;
        let o = 0;
        for (let i = void 0 !== r ? 65535 & e[18] : 0; i < t.length; i++)
          if ("number" == typeof t[i + 1]) {
            if (((o = t[i]), null != r && o >= r)) break;
          } else
            t[i] < 0 && (e[18] += 65536),
              (o < s || -1 == s) &&
                (Zt(e, n, t, i), (e[18] = (4294901760 & e[18]) + i + 2)),
              i++;
      }
      function Zt(e, t, n, r) {
        const s = n[r] < 0,
          o = n[r + 1],
          i = e[s ? -n[r] : n[r]];
        s
          ? e[2] >> 11 < e[18] >> 16 &&
            (3 & e[2]) === t &&
            ((e[2] += 2048), o.call(i))
          : o.call(i);
      }
      class qt {
        constructor(e, t, n) {
          (this.factory = e),
            (this.resolving = !1),
            (this.canSeeViewProviders = t),
            (this.injectImpl = n);
        }
      }
      function Wt(e, t, n) {
        const r = ut(e);
        let s = 0;
        for (; s < n.length; ) {
          const o = n[s];
          if ("number" == typeof o) {
            if (0 !== o) break;
            s++;
            const i = n[s++],
              l = n[s++],
              a = n[s++];
            r ? e.setAttribute(t, l, a, i) : t.setAttributeNS(i, l, a);
          } else {
            const i = o,
              l = n[++s];
            64 === i.charCodeAt(0)
              ? r && e.setProperty(t, i, l)
              : r
              ? e.setAttribute(t, i, l)
              : t.setAttribute(i, l),
              s++;
          }
        }
        return s;
      }
      function Kt(e, t) {
        if (null === t || 0 === t.length);
        else if (null === e || 0 === e.length) e = t.slice();
        else {
          let n = -1;
          for (let r = 0; r < t.length; r++) {
            const s = t[r];
            "number" == typeof s
              ? (n = s)
              : 0 === n ||
                Jt(e, n, s, null, -1 === n || 2 === n ? t[++r] : null);
          }
        }
        return e;
      }
      function Jt(e, t, n, r, s) {
        let o = 0,
          i = e.length;
        if (-1 === t) i = -1;
        else
          for (; o < e.length; ) {
            const n = e[o++];
            if ("number" == typeof n) {
              if (n === t) {
                i = -1;
                break;
              }
              if (n > t) {
                i = o - 1;
                break;
              }
            }
          }
        for (; o < e.length; ) {
          const t = e[o];
          if ("number" == typeof t) break;
          if (t === n) {
            if (null === r) return void (null !== s && (e[o + 1] = s));
            if (r === e[o + 1]) return void (e[o + 2] = s);
          }
          o++, null !== r && o++, null !== s && o++;
        }
        -1 !== i && (e.splice(i, 0, t), (o = i + 1)),
          e.splice(o++, 0, n),
          null !== r && e.splice(o++, 0, r),
          null !== s && e.splice(o++, 0, s);
      }
      function Qt(e) {
        return 32767 & e;
      }
      function Gt(e, t) {
        let n = e >> 16,
          r = t;
        for (; n > 0; ) (r = r[15]), n--;
        return r;
      }
      function Xt(e) {
        return "function" == typeof e
          ? e.name || e.toString()
          : "object" == typeof e && null != e && "function" == typeof e.type
          ? e.type.name || e.type.toString()
          : (function (e) {
              return "string" == typeof e ? e : null == e ? "" : "" + e;
            })(e);
      }
      const Yt = (() =>
        (
          ("undefined" != typeof requestAnimationFrame &&
            requestAnimationFrame) ||
          setTimeout
        ).bind(we))();
      function en(e) {
        return e instanceof Function ? e() : e;
      }
      let tn = !0;
      function nn(e) {
        const t = tn;
        return (tn = e), t;
      }
      let rn = 0;
      function sn(e, t) {
        const n = ln(e, t);
        if (-1 !== n) return n;
        const r = t[1];
        r.firstCreatePass &&
          ((e.injectorIndex = t.length),
          on(r.data, e),
          on(t, null),
          on(r.blueprint, null));
        const s = an(e, t),
          o = e.injectorIndex;
        if (-1 !== s) {
          const e = Qt(s),
            n = Gt(s, t),
            r = n[1].data;
          for (let s = 0; s < 8; s++) t[o + s] = n[e + s] | r[e + s];
        }
        return (t[o + 8] = s), o;
      }
      function on(e, t) {
        e.push(0, 0, 0, 0, 0, 0, 0, 0, t);
      }
      function ln(e, t) {
        return -1 === e.injectorIndex ||
          (e.parent && e.parent.injectorIndex === e.injectorIndex) ||
          null == t[e.injectorIndex + 8]
          ? -1
          : e.injectorIndex;
      }
      function an(e, t) {
        if (e.parent && -1 !== e.parent.injectorIndex)
          return e.parent.injectorIndex;
        let n = t[6],
          r = 1;
        for (; n && -1 === n.injectorIndex; )
          (n = (t = t[15]) ? t[6] : null), r++;
        return n ? n.injectorIndex | (r << 16) : -1;
      }
      function cn(e, t, n) {
        !(function (e, t, n) {
          let r = "string" != typeof n ? n[Ie] : n.charCodeAt(0) || 0;
          null == r && (r = n[Ie] = rn++);
          const s = 255 & r,
            o = 1 << s,
            i = 64 & s,
            l = 32 & s,
            a = t.data;
          128 & s
            ? i
              ? l
                ? (a[e + 7] |= o)
                : (a[e + 6] |= o)
              : l
              ? (a[e + 5] |= o)
              : (a[e + 4] |= o)
            : i
            ? l
              ? (a[e + 3] |= o)
              : (a[e + 2] |= o)
            : l
            ? (a[e + 1] |= o)
            : (a[e] |= o);
        })(e, t, n);
      }
      const un = {};
      function hn(e, t, n, r, s, o) {
        const i = t[1],
          l = i.data[e + 8],
          a = (function (e, t, n, r, s) {
            const o = e.providerIndexes,
              i = t.data,
              l = 65535 & o,
              a = e.directiveStart,
              c = o >> 16,
              u = s ? l + c : e.directiveEnd;
            for (let h = r ? l : l + c; h < u; h++) {
              const e = i[h];
              if ((h < a && n === e) || (h >= a && e.type === n)) return h;
            }
            if (s) {
              const e = i[a];
              if (e && at(e) && e.type === n) return a;
            }
            return null;
          })(
            l,
            i,
            n,
            null == r
              ? (function (e) {
                  return 2 == (2 & e.flags);
                })(l) && tn
              : r != i && 3 === l.type,
            s & ee.Host && o === l
          );
        return null !== a ? dn(t, i, a, l) : un;
      }
      function dn(e, t, n, r) {
        let s = e[n];
        const o = t.data;
        if (s instanceof qt) {
          const i = s;
          if (i.resolving) throw new Error(`Circular dep for ${Xt(o[n])}`);
          const l = nn(i.canSeeViewProviders);
          let a;
          (i.resolving = !0), i.injectImpl && (a = He(i.injectImpl)), Nt(e, r);
          try {
            (s = e[n] = i.factory(void 0, o, e, r)),
              t.firstCreatePass &&
                n >= r.directiveStart &&
                (function (e, t, n) {
                  const { onChanges: r, onInit: s, doCheck: o } = t;
                  r &&
                    ((n.preOrderHooks || (n.preOrderHooks = [])).push(e, r),
                    (n.preOrderCheckHooks || (n.preOrderCheckHooks = [])).push(
                      e,
                      r
                    )),
                    s &&
                      (n.preOrderHooks || (n.preOrderHooks = [])).push(-e, s),
                    o &&
                      ((n.preOrderHooks || (n.preOrderHooks = [])).push(e, o),
                      (
                        n.preOrderCheckHooks || (n.preOrderCheckHooks = [])
                      ).push(e, o));
                })(n, o[n], t);
          } finally {
            i.injectImpl && He(a), nn(l), (i.resolving = !1), Ht();
          }
        }
        return s;
      }
      function pn(e, t, n) {
        const r = 64 & e,
          s = 32 & e;
        let o;
        return (
          (o =
            128 & e
              ? r
                ? s
                  ? n[t + 7]
                  : n[t + 6]
                : s
                ? n[t + 5]
                : n[t + 4]
              : r
              ? s
                ? n[t + 3]
                : n[t + 2]
              : s
              ? n[t + 1]
              : n[t]),
          !!(o & (1 << e))
        );
      }
      function fn(e, t) {
        return !(e & ee.Self || (e & ee.Host && t));
      }
      class mn {
        constructor(e, t) {
          (this._tNode = e), (this._lView = t);
        }
        get(e, t) {
          return (function (e, t, n, r = ee.Default, s) {
            if (null !== e) {
              const s = (function (e) {
                if ("string" == typeof e) return e.charCodeAt(0) || 0;
                const t = e[Ie];
                return "number" == typeof t && t > 0 ? 255 & t : t;
              })(n);
              if ("function" == typeof s) {
                Nt(t, e);
                try {
                  const e = s();
                  if (null != e || r & ee.Optional) return e;
                  throw new Error(`No provider for ${Xt(n)}!`);
                } finally {
                  Ht();
                }
              } else if ("number" == typeof s) {
                if (-1 === s) return new mn(e, t);
                let o = null,
                  i = ln(e, t),
                  l = -1,
                  a = r & ee.Host ? t[16][6] : null;
                for (
                  (-1 === i || r & ee.SkipSelf) &&
                  ((l = -1 === i ? an(e, t) : t[i + 8]),
                  fn(r, !1)
                    ? ((o = t[1]), (i = Qt(l)), (t = Gt(l, t)))
                    : (i = -1));
                  -1 !== i;

                ) {
                  l = t[i + 8];
                  const e = t[1];
                  if (pn(s, i, e.data)) {
                    const e = hn(i, t, n, o, r, a);
                    if (e !== un) return e;
                  }
                  fn(r, t[1].data[i + 8] === a) && pn(s, i, t)
                    ? ((o = e), (i = Qt(l)), (t = Gt(l, t)))
                    : (i = -1);
                }
              }
            }
            if (
              (r & ee.Optional && void 0 === s && (s = null),
              0 == (r & (ee.Self | ee.Host)))
            ) {
              const e = t[9],
                o = He(void 0);
              try {
                return e
                  ? e.get(n, s, r & ee.Optional)
                  : Le(n, s, r & ee.Optional);
              } finally {
                He(o);
              }
            }
            if (r & ee.Optional) return s;
            throw new Error(`NodeInjector: NOT_FOUND [${Xt(n)}]`);
          })(this._tNode, this._lView, e, void 0, t);
        }
      }
      function yn(e) {
        return e.ngDebugContext;
      }
      function gn(e) {
        return e.ngOriginalError;
      }
      function _n(e, ...t) {
        e.error(...t);
      }
      class bn {
        constructor() {
          this._console = console;
        }
        handleError(e) {
          const t = this._findOriginalError(e),
            n = this._findContext(e),
            r = (function (e) {
              return e.ngErrorLogger || _n;
            })(e);
          r(this._console, "ERROR", e),
            t && r(this._console, "ORIGINAL ERROR", t),
            n && r(this._console, "ERROR CONTEXT", n);
        }
        _findContext(e) {
          return e ? (yn(e) ? yn(e) : this._findContext(gn(e))) : null;
        }
        _findOriginalError(e) {
          let t = gn(e);
          for (; t && gn(t); ) t = gn(t);
          return t;
        }
      }
      let vn = !0,
        wn = !1;
      function Cn() {
        return (wn = !0), vn;
      }
      function xn(e, t) {
        e.__ngContext__ = t;
      }
      function En(e) {
        throw new Error(
          `Multiple components match node with tagname ${e.tagName}`
        );
      }
      function kn() {
        throw new Error("Cannot mix multi providers and regular providers");
      }
      function Tn(e, t, n) {
        let r = e.length;
        for (;;) {
          const s = e.indexOf(t, n);
          if (-1 === s) return s;
          if (0 === s || e.charCodeAt(s - 1) <= 32) {
            const n = t.length;
            if (s + n === r || e.charCodeAt(s + n) <= 32) return s;
          }
          n = s + 1;
        }
      }
      function Sn(e, t, n) {
        let r = 0;
        for (; r < e.length; ) {
          let s = e[r++];
          if (n && "class" === s) {
            if (((s = e[r]), -1 !== Tn(s.toLowerCase(), t, 0))) return !0;
          } else if (1 === s) {
            for (; r < e.length && "string" == typeof (s = e[r++]); )
              if (s.toLowerCase() === t) return !0;
            return !1;
          }
        }
        return !1;
      }
      function In(e) {
        return 0 === e.type && "ng-template" !== e.tagName;
      }
      function An(e, t, n) {
        return t === (0 !== e.type || n ? e.tagName : "ng-template");
      }
      function On(e, t, n) {
        let r = 4;
        const s = e.attrs || [],
          o = (function (e) {
            for (let n = 0; n < e.length; n++)
              if (3 === (t = e[n]) || 4 === t || 6 === t) return n;
            var t;
            return e.length;
          })(s);
        let i = !1;
        for (let l = 0; l < t.length; l++) {
          const a = t[l];
          if ("number" != typeof a) {
            if (!i)
              if (4 & r) {
                if (
                  ((r = 2 | (1 & r)),
                  ("" !== a && !An(e, a, n)) || ("" === a && 1 === t.length))
                ) {
                  if (Pn(r)) return !1;
                  i = !0;
                }
              } else {
                const c = 8 & r ? a : t[++l];
                if (8 & r && null !== e.attrs) {
                  if (!Sn(e.attrs, c, n)) {
                    if (Pn(r)) return !1;
                    i = !0;
                  }
                  continue;
                }
                const u = Nn(8 & r ? "class" : a, s, In(e), n);
                if (-1 === u) {
                  if (Pn(r)) return !1;
                  i = !0;
                  continue;
                }
                if ("" !== c) {
                  let e;
                  e = u > o ? "" : s[u + 1].toLowerCase();
                  const t = 8 & r ? e : null;
                  if ((t && -1 !== Tn(t, c, 0)) || (2 & r && c !== e)) {
                    if (Pn(r)) return !1;
                    i = !0;
                  }
                }
              }
          } else {
            if (!i && !Pn(r) && !Pn(a)) return !1;
            if (i && Pn(a)) continue;
            (i = !1), (r = a | (1 & r));
          }
        }
        return Pn(r) || i;
      }
      function Pn(e) {
        return 0 == (1 & e);
      }
      function Nn(e, t, n, r) {
        if (null === t) return -1;
        let s = 0;
        if (r || !n) {
          let n = !1;
          for (; s < t.length; ) {
            const r = t[s];
            if (r === e) return s;
            if (3 === r || 6 === r) n = !0;
            else {
              if (1 === r || 2 === r) {
                let e = t[++s];
                for (; "string" == typeof e; ) e = t[++s];
                continue;
              }
              if (4 === r) break;
              if (0 === r) {
                s += 4;
                continue;
              }
            }
            s += n ? 1 : 2;
          }
          return -1;
        }
        return (function (e, t) {
          let n = e.indexOf(4);
          if (n > -1)
            for (n++; n < e.length; ) {
              const r = e[n];
              if ("number" == typeof r) return -1;
              if (r === t) return n;
              n++;
            }
          return -1;
        })(t, e);
      }
      function Dn(e, t, n = !1) {
        for (let r = 0; r < t.length; r++) if (On(e, t[r], n)) return !0;
        return !1;
      }
      function Mn(e, t) {
        return e ? ":not(" + t.trim() + ")" : t;
      }
      function Rn(e) {
        let t = e[0],
          n = 1,
          r = 2,
          s = "",
          o = !1;
        for (; n < e.length; ) {
          let i = e[n];
          if ("string" == typeof i)
            if (2 & r) {
              const t = e[++n];
              s += "[" + i + (t.length > 0 ? '="' + t + '"' : "") + "]";
            } else 8 & r ? (s += "." + i) : 4 & r && (s += " " + i);
          else
            "" === s || Pn(i) || ((t += Mn(o, s)), (s = "")),
              (r = i),
              (o = o || !Pn(r));
          n++;
        }
        return "" !== s && (t += Mn(o, s)), t;
      }
      const jn = {};
      function Hn(e) {
        const t = e[3];
        return it(t) ? t[3] : t;
      }
      function Fn(e, t) {
        const n = e.contentQueries;
        if (null !== n)
          for (let r = 0; r < n.length; r += 2) {
            const s = n[r],
              o = n[r + 1];
            if (-1 !== o) {
              const n = e.data[o];
              Pt(s), n.contentQueries(2, t[o], o);
            }
          }
      }
      function Vn(e, t, n) {
        return ut(t)
          ? t.createElement(e, n)
          : null === n
          ? t.createElement(e)
          : t.createElementNS(n, e);
      }
      function Ln(e, t, n, r, s, o, i, l, a, c) {
        const u = t.blueprint.slice();
        return (
          (u[0] = s),
          (u[2] = 140 | r),
          _t(u),
          (u[3] = u[15] = e),
          (u[8] = n),
          (u[10] = i || (e && e[10])),
          (u[11] = l || (e && e[11])),
          (u[12] = a || (e && e[12]) || null),
          (u[9] = c || (e && e[9]) || null),
          (u[6] = o),
          (u[16] = 2 == t.type ? e[16] : u),
          u
        );
      }
      function zn(e, t, n, r, s, o) {
        const i = n + 20,
          l =
            e.data[i] ||
            (function (e, t, n, r, s, o) {
              const i = Et(),
                l = Tt(),
                a = l ? i : i && i.parent,
                c = (e.data[n] = Kn(0, a && a !== t ? a : null, r, n, s, o));
              return (
                null === e.firstChild && (e.firstChild = c),
                i &&
                  (!l || null != i.child || (null === c.parent && 2 !== i.type)
                    ? l || (i.next = c)
                    : (i.child = c)),
                c
              );
            })(e, t, i, r, s, o);
        return kt(l, !0), l;
      }
      function Un(e, t, n) {
        Dt(t, t[6]);
        try {
          const r = e.viewQuery;
          null !== r && fr(1, r, n);
          const s = e.template;
          null !== s && Zn(e, t, s, 1, n),
            e.firstCreatePass && (e.firstCreatePass = !1),
            e.staticContentQueries && Fn(e, t),
            e.staticViewQueries && fr(2, e.viewQuery, n);
          const o = e.components;
          null !== o &&
            (function (e, t) {
              for (let n = 0; n < t.length; n++) ur(e, t[n]);
            })(t, o);
        } catch (r) {
          throw (e.firstCreatePass && (e.incompleteFirstPass = !0), r);
        } finally {
          (t[2] &= -5), Ft();
        }
      }
      function Bn(e, t, n, r) {
        const s = t[2];
        if (256 == (256 & s)) return;
        Dt(t, t[6]);
        const o = St();
        try {
          _t(t),
            (vt.lFrame.bindingIndex = e.bindingStartIndex),
            null !== n && Zn(e, t, n, 2, r);
          const i = 3 == (3 & s);
          if (!o)
            if (i) {
              const n = e.preOrderCheckHooks;
              null !== n && zt(t, n, null);
            } else {
              const n = e.preOrderHooks;
              null !== n && Ut(t, n, 0, null), Bt(t, 0);
            }
          if (
            ((function (e) {
              for (let t = lr(e); null !== t; t = ar(t))
                if (0 != (1 & t[2])) {
                  const e = t[9];
                  for (let t = 0; t < e.length; t++) {
                    const n = e[t],
                      r = n[3];
                    0 == (1024 & n[2]) && bt(r, 1), (n[2] |= 1024);
                  }
                }
            })(t),
            (function (e) {
              for (let t = lr(e); null !== t; t = ar(t))
                for (let e = 10; e < t.length; e++) {
                  const n = t[e],
                    r = n[1];
                  yt(n) && Bn(r, n, r.template, n[8]);
                }
            })(t),
            null !== e.contentQueries && Fn(e, t),
            !o)
          )
            if (i) {
              const n = e.contentCheckHooks;
              null !== n && zt(t, n);
            } else {
              const n = e.contentHooks;
              null !== n && Ut(t, n, 1), Bt(t, 1);
            }
          !(function (e, t) {
            try {
              const n = e.expandoInstructions;
              if (null !== n) {
                let r = e.expandoStartIndex,
                  s = -1,
                  o = -1;
                for (let e = 0; e < n.length; e++) {
                  const i = n[e];
                  "number" == typeof i
                    ? i <= 0
                      ? ((o = 0 - i), Vt(o), (r += 9 + n[++e]), (s = r))
                      : (r += i)
                    : (null !== i && (At(r, s), i(2, t[s])), s++);
                }
              }
            } finally {
              Vt(-1);
            }
          })(e, t);
          const l = e.components;
          null !== l &&
            (function (e, t) {
              for (let n = 0; n < t.length; n++) cr(e, t[n]);
            })(t, l);
          const a = e.viewQuery;
          if ((null !== a && fr(2, a, r), !o))
            if (i) {
              const n = e.viewCheckHooks;
              null !== n && zt(t, n);
            } else {
              const n = e.viewHooks;
              null !== n && Ut(t, n, 2), Bt(t, 2);
            }
          !0 === e.firstUpdatePass && (e.firstUpdatePass = !1),
            o || (t[2] &= -73),
            1024 & t[2] && ((t[2] &= -1025), bt(t[3], -1));
        } finally {
          Ft();
        }
      }
      function $n(e, t, n, r) {
        const s = t[10],
          o = !St(),
          i = 4 == (4 & t[2]);
        try {
          o && !i && s.begin && s.begin(), i && Un(e, t, r), Bn(e, t, n, r);
        } finally {
          o && !i && s.end && s.end();
        }
      }
      function Zn(e, t, n, r, s) {
        const o = vt.lFrame.selectedIndex;
        try {
          Vt(-1),
            2 & r &&
              t.length > 20 &&
              (function (e, t, n, r) {
                if (!r)
                  if (3 == (3 & t[2])) {
                    const n = e.preOrderCheckHooks;
                    null !== n && zt(t, n, 0);
                  } else {
                    const n = e.preOrderHooks;
                    null !== n && Ut(t, n, 0, 0);
                  }
                Vt(0);
              })(e, t, 0, St()),
            n(r, s);
        } finally {
          Vt(o);
        }
      }
      function qn(e) {
        const t = e.tView;
        return null === t || t.incompleteFirstPass
          ? (e.tView = Wn(
              1,
              -1,
              e.template,
              e.decls,
              e.vars,
              e.directiveDefs,
              e.pipeDefs,
              e.viewQuery,
              e.schemas,
              e.consts
            ))
          : t;
      }
      function Wn(e, t, n, r, s, o, i, l, a, c) {
        const u = 20 + r,
          h = u + s,
          d = (function (e, t) {
            const n = [];
            for (let r = 0; r < t; r++) n.push(r < e ? null : jn);
            return n;
          })(u, h);
        return (d[1] = {
          type: e,
          id: t,
          blueprint: d,
          template: n,
          queries: null,
          viewQuery: l,
          node: null,
          data: d.slice().fill(null, u),
          bindingStartIndex: u,
          expandoStartIndex: h,
          expandoInstructions: null,
          firstCreatePass: !0,
          firstUpdatePass: !0,
          staticViewQueries: !1,
          staticContentQueries: !1,
          preOrderHooks: null,
          preOrderCheckHooks: null,
          contentHooks: null,
          contentCheckHooks: null,
          viewHooks: null,
          viewCheckHooks: null,
          destroyHooks: null,
          cleanup: null,
          contentQueries: null,
          components: null,
          directiveRegistry: "function" == typeof o ? o() : o,
          pipeRegistry: "function" == typeof i ? i() : i,
          firstChild: null,
          schemas: a,
          consts: c,
          incompleteFirstPass: !1,
        });
      }
      function Kn(e, t, n, r, s, o) {
        return {
          type: n,
          index: r,
          injectorIndex: t ? t.injectorIndex : -1,
          directiveStart: -1,
          directiveEnd: -1,
          directiveStylingLast: -1,
          propertyBindings: null,
          flags: 0,
          providerIndexes: 0,
          tagName: s,
          attrs: o,
          mergedAttrs: null,
          localNames: null,
          initialInputs: void 0,
          inputs: null,
          outputs: null,
          tViews: null,
          next: null,
          projectionNext: null,
          child: null,
          parent: t,
          projection: null,
          styles: null,
          residualStyles: void 0,
          classes: null,
          residualClasses: void 0,
          classBindings: 0,
          styleBindings: 0,
        };
      }
      function Jn(e, t, n) {
        for (let r in e)
          if (e.hasOwnProperty(r)) {
            const s = e[r];
            (n = null === n ? {} : n).hasOwnProperty(r)
              ? n[r].push(t, s)
              : (n[r] = [t, s]);
          }
        return n;
      }
      function Qn(e, t) {
        const n = e.expandoInstructions;
        n.push(t.hostBindings), 0 !== t.hostVars && n.push(t.hostVars);
      }
      function Gn(e, t, n) {
        for (let r = 0; r < n; r++)
          t.push(jn), e.blueprint.push(jn), e.data.push(null);
      }
      function Xn(e, t) {
        null !== e.hostBindings && e.hostBindings(1, t);
      }
      function Yn(e, t, n) {
        const r = 20 - t.index,
          s = e.data.length - (65535 & t.providerIndexes);
        (e.expandoInstructions || (e.expandoInstructions = [])).push(r, s, n);
      }
      function er(e, t) {
        (t.flags |= 2), (e.components || (e.components = [])).push(t.index);
      }
      function tr(e, t, n) {
        if (n) {
          if (t.exportAs)
            for (let r = 0; r < t.exportAs.length; r++) n[t.exportAs[r]] = e;
          at(t) && (n[""] = e);
        }
      }
      function nr(e, t, n) {
        (e.flags |= 1),
          (e.directiveStart = t),
          (e.directiveEnd = t + n),
          (e.providerIndexes = t);
      }
      function rr(e, t, n) {
        e.data.push(n);
        const r = n.factory || (n.factory = rt(n.type)),
          s = new qt(r, at(n), null);
        e.blueprint.push(s), t.push(s);
      }
      function sr(e, t, n) {
        const r = pt(t, e),
          s = qn(n),
          o = e[10],
          i = hr(
            e,
            Ln(e, s, null, n.onPush ? 64 : 16, r, t, o, o.createRenderer(r, n))
          );
        e[t.index] = i;
      }
      function or(e, t, n, r, s, o) {
        const i = o[t];
        if (null !== i) {
          const e = r.setInput;
          for (let t = 0; t < i.length; ) {
            const s = i[t++],
              o = i[t++],
              l = i[t++];
            null !== e ? r.setInput(n, l, s, o) : (n[o] = l);
          }
        }
      }
      function ir(e, t) {
        let n = null,
          r = 0;
        for (; r < t.length; ) {
          const s = t[r];
          if (0 !== s)
            if (5 !== s) {
              if ("number" == typeof s) break;
              e.hasOwnProperty(s) &&
                (null === n && (n = []), n.push(s, e[s], t[r + 1])),
                (r += 2);
            } else r += 2;
          else r += 4;
        }
        return n;
      }
      function lr(e) {
        let t = e[13];
        for (; null !== t && (!it(t) || t[2] >> 1 != -1); ) t = t[4];
        return t;
      }
      function ar(e) {
        let t = e[4];
        for (; null !== t && (!it(t) || t[2] >> 1 != -1); ) t = t[4];
        return t;
      }
      function cr(e, t) {
        const n = ft(t, e);
        if (yt(n)) {
          const e = n[1];
          80 & n[2]
            ? Bn(e, n, e.template, n[8])
            : n[5] > 0 &&
              (function e(t) {
                for (let r = lr(t); null !== r; r = ar(r))
                  for (let t = 10; t < r.length; t++) {
                    const n = r[t];
                    if (1024 & n[2]) {
                      const e = n[1];
                      Bn(e, n, e.template, n[8]);
                    } else n[5] > 0 && e(n);
                  }
                const n = t[1].components;
                if (null !== n)
                  for (let r = 0; r < n.length; r++) {
                    const s = ft(n[r], t);
                    yt(s) && s[5] > 0 && e(s);
                  }
              })(n);
        }
      }
      function ur(e, t) {
        const n = ft(t, e),
          r = n[1];
        !(function (e, t) {
          for (let n = t.length; n < e.blueprint.length; n++)
            t.push(e.blueprint[n]);
        })(r, n),
          Un(r, n, n[8]);
      }
      function hr(e, t) {
        return e[13] ? (e[14][4] = t) : (e[13] = t), (e[14] = t), t;
      }
      function dr(e, t, n) {
        const r = t[10];
        r.begin && r.begin();
        try {
          Bn(e, t, e.template, n);
        } catch (s) {
          throw (
            ((function (e, t) {
              const n = e[9],
                r = n ? n.get(bn, null) : null;
              r && r.handleError(t);
            })(t, s),
            s)
          );
        } finally {
          r.end && r.end();
        }
      }
      function pr(e) {
        !(function (e) {
          for (let t = 0; t < e.components.length; t++) {
            const n = e.components[t],
              r = mt(n),
              s = r[1];
            $n(s, r, s.template, n);
          }
        })(e[8]);
      }
      function fr(e, t, n) {
        Pt(0), t(e, n);
      }
      const mr = (() => Promise.resolve(null))();
      function yr(e, t) {
        const n = t[3];
        return -1 === e.index ? (it(n) ? n : null) : n;
      }
      function gr(e, t, n, r, s) {
        if (null != r) {
          let o,
            i = !1;
          it(r) ? (o = r) : ot(r) && ((i = !0), (r = r[0]));
          const l = dt(r);
          0 === e && null !== n
            ? null == s
              ? wr(t, n, l)
              : vr(t, n, l, s || null)
            : 1 === e && null !== n
            ? vr(t, n, l, s || null)
            : 2 === e
            ? (function (e, t, n) {
                const r = xr(e, t);
                r &&
                  (function (e, t, n, r) {
                    ut(e) ? e.removeChild(t, n, r) : t.removeChild(n);
                  })(e, r, t, n);
              })(t, l, i)
            : 3 === e && t.destroyNode(l),
            null != o &&
              (function (e, t, n, r, s) {
                const o = n[7];
                o !== dt(n) && gr(t, e, r, o, s);
                for (let i = 10; i < n.length; i++) {
                  const s = n[i];
                  Tr(s[1], s, e, t, r, o);
                }
              })(t, e, o, n, s);
        }
      }
      function _r(e, t) {
        let n;
        return ot(e) && (n = e[6]) && 2 === n.type
          ? yr(n, e)
          : e[3] === t
          ? null
          : e[3];
      }
      function br(e, t) {
        if (!(256 & t[2])) {
          (t[2] &= -129),
            (t[2] |= 256),
            (function (e, t) {
              let n;
              if (null != e && null != (n = e.destroyHooks))
                for (let r = 0; r < n.length; r += 2) {
                  const e = t[n[r]];
                  if (!(e instanceof qt)) {
                    const t = n[r + 1];
                    if (Array.isArray(t))
                      for (let n = 0; n < t.length; n += 2)
                        t[n + 1].call(e[t[n]]);
                    else t.call(e);
                  }
                }
            })(e, t),
            (function (e, t) {
              const n = e.cleanup;
              if (null !== n) {
                const e = t[7];
                for (let r = 0; r < n.length - 1; r += 2)
                  if ("string" == typeof n[r]) {
                    const s = n[r + 1],
                      o = "function" == typeof s ? s(t) : dt(t[s]),
                      i = e[n[r + 2]],
                      l = n[r + 3];
                    "boolean" == typeof l
                      ? o.removeEventListener(n[r], i, l)
                      : l >= 0
                      ? e[l]()
                      : e[-l].unsubscribe(),
                      (r += 2);
                  } else n[r].call(e[n[r + 1]]);
                t[7] = null;
              }
            })(e, t);
          const n = t[6];
          n && 3 === n.type && ut(t[11]) && t[11].destroy();
          const r = t[17];
          if (null !== r && it(t[3])) {
            r !== t[3] &&
              (function (e, t) {
                const n = e[9],
                  r = n.indexOf(t);
                1024 & t[2] && bt(t[3], -1), n.splice(r, 1);
              })(r, t);
            const n = t[19];
            null !== n && n.detachView(e);
          }
        }
      }
      function vr(e, t, n, r) {
        ut(e) ? e.insertBefore(t, n, r) : t.insertBefore(n, r, !0);
      }
      function wr(e, t, n) {
        ut(e) ? e.appendChild(t, n) : t.appendChild(n);
      }
      function Cr(e, t, n, r) {
        null !== r ? vr(e, t, n, r) : wr(e, t, n);
      }
      function xr(e, t) {
        return ut(e) ? e.parentNode(t) : t.parentNode;
      }
      function Er(e, t, n, r) {
        const s = (function (e, t, n) {
          let r = t.parent;
          for (; null != r && (4 === r.type || 5 === r.type); )
            r = (t = r).parent;
          if (null == r) {
            const e = n[6];
            return 2 === e.type
              ? (function (e, t) {
                  const n = yr(e, t);
                  return n ? xr(t[11], n[7]) : null;
                })(e, n)
              : n[0];
          }
          if (t && 5 === t.type && 4 & t.flags) return pt(t, n).parentNode;
          if (2 & r.flags) {
            const t = e.data,
              n = t[t[r.index].directiveStart].encapsulation;
            if (n !== qe.ShadowDom && n !== qe.Native) return null;
          }
          return pt(r, n);
        })(e, r, t);
        if (null != s) {
          const e = t[11],
            o = (function (e, t) {
              if (2 === e.type) {
                const n = yr(e, t);
                return null === n
                  ? null
                  : (function e(t, n) {
                      const r = 10 + t + 1;
                      if (r < n.length) {
                        const t = n[r],
                          s = t[1].firstChild;
                        if (null !== s)
                          return (function t(n, r) {
                            if (null !== r) {
                              const s = r.type;
                              if (3 === s) return pt(r, n);
                              if (0 === s) return e(-1, n[r.index]);
                              if (4 === s || 5 === s) {
                                const s = r.child;
                                if (null !== s) return t(n, s);
                                {
                                  const t = n[r.index];
                                  return it(t) ? e(-1, t) : dt(t);
                                }
                              }
                              {
                                const e = n[16],
                                  s = e[6],
                                  o = Hn(e),
                                  i = s.projection[r.projection];
                                return null != i ? t(o, i) : t(n, r.next);
                              }
                            }
                            return null;
                          })(t, s);
                      }
                      return n[7];
                    })(n.indexOf(t, 10) - 10, n);
              }
              return 4 === e.type || 5 === e.type ? pt(e, t) : null;
            })(r.parent || t[6], t);
          if (Array.isArray(n))
            for (let t = 0; t < n.length; t++) Cr(e, s, n[t], o);
          else Cr(e, s, n, o);
        }
      }
      function kr(e, t, n, r, s, o, i) {
        for (; null != n; ) {
          const l = r[n.index],
            a = n.type;
          i && 0 === t && (l && xn(dt(l), r), (n.flags |= 4)),
            64 != (64 & n.flags) &&
              (4 === a || 5 === a
                ? (kr(e, t, n.child, r, s, o, !1), gr(t, e, s, l, o))
                : 1 === a
                ? Sr(e, t, r, n, s, o)
                : gr(t, e, s, l, o)),
            (n = i ? n.projectionNext : n.next);
        }
      }
      function Tr(e, t, n, r, s, o) {
        kr(n, r, e.node.child, t, s, o, !1);
      }
      function Sr(e, t, n, r, s, o) {
        const i = n[16],
          l = i[6].projection[r.projection];
        if (Array.isArray(l))
          for (let a = 0; a < l.length; a++) gr(t, e, s, l[a], o);
        else kr(e, t, l, i[3], s, o, !0);
      }
      function Ir(e, t, n) {
        ut(e) ? e.setAttribute(t, "style", n) : (t.style.cssText = n);
      }
      function Ar(e, t, n) {
        ut(e)
          ? "" === n
            ? e.removeAttribute(t, "class")
            : e.setAttribute(t, "class", n)
          : (t.className = n);
      }
      class Or extends class {
        constructor(e, t) {
          (this._lView = e),
            (this._cdRefInjectingView = t),
            (this._appRef = null),
            (this._viewContainerRef = null),
            (this._tViewNode = null);
        }
        get rootNodes() {
          const e = this._lView;
          return null == e[0]
            ? (function e(t, n, r, s, o = !1) {
                for (; null !== r; ) {
                  const i = n[r.index];
                  if ((null !== i && s.push(dt(i)), it(i)))
                    for (let t = 10; t < i.length; t++) {
                      const n = i[t],
                        r = n[1].firstChild;
                      null !== r && e(n[1], n, r, s);
                    }
                  const l = r.type;
                  if (4 === l || 5 === l) e(t, n, r.child, s);
                  else if (1 === l) {
                    const t = n[16],
                      o = t[6].projection[r.projection];
                    if (Array.isArray(o)) s.push(...o);
                    else {
                      const n = Hn(t);
                      e(n[1], n, o, s, !0);
                    }
                  }
                  r = o ? r.projectionNext : r.next;
                }
                return s;
              })(e[1], e, e[6].child, [])
            : [];
        }
        get context() {
          return this._lView[8];
        }
        get destroyed() {
          return 256 == (256 & this._lView[2]);
        }
        destroy() {
          if (this._appRef) this._appRef.detachView(this);
          else if (this._viewContainerRef) {
            const e = this._viewContainerRef.indexOf(this);
            e > -1 && this._viewContainerRef.detach(e),
              (this._viewContainerRef = null);
          }
          !(function (e, t) {
            if (!(256 & t[2])) {
              const n = t[11];
              ut(n) && n.destroyNode && Tr(e, t, n, 3, null, null),
                (function (e) {
                  let t = e[13];
                  if (!t) return br(e[1], e);
                  for (; t; ) {
                    let n = null;
                    if (ot(t)) n = t[13];
                    else {
                      const e = t[10];
                      e && (n = e);
                    }
                    if (!n) {
                      for (; t && !t[4] && t !== e; )
                        ot(t) && br(t[1], t), (t = _r(t, e));
                      null === t && (t = e),
                        ot(t) && br(t[1], t),
                        (n = t && t[4]);
                    }
                    t = n;
                  }
                })(t);
            }
          })(this._lView[1], this._lView);
        }
        onDestroy(e) {
          var t, n, r, s;
          (t = this._lView[1]),
            (r = e),
            ((s = n = this._lView), s[7] || (s[7] = [])).push(r),
            t.firstCreatePass &&
              (function (e) {
                return e.cleanup || (e.cleanup = []);
              })(t).push(n[7].length - 1, null);
        }
        markForCheck() {
          !(function (e) {
            for (; e; ) {
              e[2] |= 64;
              const t = Hn(e);
              if (0 != (512 & e[2]) && !t) return e;
              e = t;
            }
          })(this._cdRefInjectingView || this._lView);
        }
        detach() {
          this._lView[2] &= -129;
        }
        reattach() {
          this._lView[2] |= 128;
        }
        detectChanges() {
          dr(this._lView[1], this._lView, this.context);
        }
        checkNoChanges() {
          !(function (e, t, n) {
            It(!0);
            try {
              dr(e, t, n);
            } finally {
              It(!1);
            }
          })(this._lView[1], this._lView, this.context);
        }
        attachToViewContainerRef(e) {
          if (this._appRef)
            throw new Error(
              "This view is already attached directly to the ApplicationRef!"
            );
          this._viewContainerRef = e;
        }
        detachFromAppRef() {
          var e;
          (this._appRef = null),
            Tr(this._lView[1], (e = this._lView), e[11], 2, null, null);
        }
        attachToAppRef(e) {
          if (this._viewContainerRef)
            throw new Error(
              "This view is already attached to a ViewContainer!"
            );
          this._appRef = e;
        }
      } {
        constructor(e) {
          super(e), (this._view = e);
        }
        detectChanges() {
          pr(this._view);
        }
        checkNoChanges() {
          !(function (e) {
            It(!0);
            try {
              pr(e);
            } finally {
              It(!1);
            }
          })(this._view);
        }
        get context() {
          return null;
        }
      }
      let Pr;
      function Nr(e, t, n) {
        return Pr || (Pr = class extends e {}), new Pr(pt(t, n));
      }
      const Dr = new Ae("Set Injector scope."),
        Mr = {},
        Rr = {},
        jr = [];
      let Hr = void 0;
      function Fr() {
        return void 0 === Hr && (Hr = new Ue()), Hr;
      }
      function Vr(e, t = null, n = null, r) {
        return new Lr(e, n, t || Fr(), r);
      }
      class Lr {
        constructor(e, t, n, r = null) {
          (this.parent = n),
            (this.records = new Map()),
            (this.injectorDefTypes = new Set()),
            (this.onDestroy = new Set()),
            (this._destroyed = !1);
          const s = [];
          t && $e(t, (n) => this.processProvider(n, e, t)),
            $e([e], (e) => this.processInjectorType(e, [], s)),
            this.records.set(Oe, Ur(void 0, this));
          const o = this.records.get(Dr);
          (this.scope = null != o ? o.value : null),
            (this.source = r || ("object" == typeof e ? null : de(e)));
        }
        get destroyed() {
          return this._destroyed;
        }
        destroy() {
          this.assertNotDestroyed(), (this._destroyed = !0);
          try {
            this.onDestroy.forEach((e) => e.ngOnDestroy());
          } finally {
            this.records.clear(),
              this.onDestroy.clear(),
              this.injectorDefTypes.clear();
          }
        }
        get(e, t = Pe, n = ee.Default) {
          this.assertNotDestroyed();
          const r = je(this);
          try {
            if (!(n & ee.SkipSelf)) {
              let t = this.records.get(e);
              if (void 0 === t) {
                const n =
                  ("function" == typeof (s = e) ||
                    ("object" == typeof s && s instanceof Ae)) &&
                  se(e);
                (t = n && this.injectableDefInScope(n) ? Ur(zr(e), Mr) : null),
                  this.records.set(e, t);
              }
              if (null != t) return this.hydrate(e, t);
            }
            return (n & ee.Self ? Fr() : this.parent).get(
              e,
              (t = n & ee.Optional && t === Pe ? null : t)
            );
          } catch (o) {
            if ("NullInjectorError" === o.name) {
              if (
                ((o.ngTempTokenPath = o.ngTempTokenPath || []).unshift(de(e)),
                r)
              )
                throw o;
              return (function (e, t, n, r) {
                const s = e.ngTempTokenPath;
                throw (
                  (t.__source && s.unshift(t.__source),
                  (e.message = (function (e, t, n, r = null) {
                    e =
                      e && "\n" === e.charAt(0) && "\u0275" == e.charAt(1)
                        ? e.substr(2)
                        : e;
                    let s = de(t);
                    if (Array.isArray(t)) s = t.map(de).join(" -> ");
                    else if ("object" == typeof t) {
                      let e = [];
                      for (let n in t)
                        if (t.hasOwnProperty(n)) {
                          let r = t[n];
                          e.push(
                            n +
                              ":" +
                              ("string" == typeof r ? JSON.stringify(r) : de(r))
                          );
                        }
                      s = `{${e.join(", ")}}`;
                    }
                    return `${n}${r ? "(" + r + ")" : ""}[${s}]: ${e.replace(
                      Ne,
                      "\n  "
                    )}`;
                  })("\n" + e.message, s, n, r)),
                  (e.ngTokenPath = s),
                  (e.ngTempTokenPath = null),
                  e)
                );
              })(o, e, "R3InjectorError", this.source);
            }
            throw o;
          } finally {
            je(r);
          }
          var s;
        }
        _resolveInjectorDefTypes() {
          this.injectorDefTypes.forEach((e) => this.get(e));
        }
        toString() {
          const e = [];
          return (
            this.records.forEach((t, n) => e.push(de(n))),
            `R3Injector[${e.join(", ")}]`
          );
        }
        assertNotDestroyed() {
          if (this._destroyed)
            throw new Error("Injector has already been destroyed.");
        }
        processInjectorType(e, t, n) {
          if (!(e = ye(e))) return !1;
          let r = ie(e);
          const s = (null == r && e.ngModule) || void 0,
            o = void 0 === s ? e : s,
            i = -1 !== n.indexOf(o);
          if ((void 0 !== s && (r = ie(s)), null == r)) return !1;
          if (null != r.imports && !i) {
            let e;
            n.push(o);
            try {
              $e(r.imports, (r) => {
                this.processInjectorType(r, t, n) &&
                  (void 0 === e && (e = []), e.push(r));
              });
            } finally {
            }
            if (void 0 !== e)
              for (let t = 0; t < e.length; t++) {
                const { ngModule: n, providers: r } = e[t];
                $e(r, (e) => this.processProvider(e, n, r || jr));
              }
          }
          this.injectorDefTypes.add(o), this.records.set(o, Ur(r.factory, Mr));
          const l = r.providers;
          if (null != l && !i) {
            const t = e;
            $e(l, (e) => this.processProvider(e, t, l));
          }
          return void 0 !== s && void 0 !== e.providers;
        }
        processProvider(e, t, n) {
          let r = $r((e = ye(e))) ? e : ye(e && e.provide);
          const s = (function (e, t, n) {
            return Br(e)
              ? Ur(void 0, e.useValue)
              : Ur(
                  (function (e, t, n) {
                    let r = void 0;
                    if ($r(e)) {
                      const t = ye(e);
                      return rt(t) || zr(t);
                    }
                    if (Br(e)) r = () => ye(e.useValue);
                    else if ((s = e) && s.useFactory)
                      r = () => e.useFactory(...ze(e.deps || []));
                    else if (
                      (function (e) {
                        return !(!e || !e.useExisting);
                      })(e)
                    )
                      r = () => Ve(ye(e.useExisting));
                    else {
                      const s = ye(e && (e.useClass || e.provide));
                      if (
                        (s ||
                          (function (e, t, n) {
                            let r = "";
                            throw (
                              (e &&
                                t &&
                                (r = ` - only instances of Provider and Type are allowed, got: [${t
                                  .map((e) => (e == n ? "?" + n + "?" : "..."))
                                  .join(", ")}]`),
                              new Error(
                                `Invalid provider for the NgModule '${de(e)}'` +
                                  r
                              ))
                            );
                          })(t, n, e),
                        !(function (e) {
                          return !!e.deps;
                        })(e))
                      )
                        return rt(s) || zr(s);
                      r = () => new s(...ze(e.deps));
                    }
                    var s;
                    return r;
                  })(e, t, n),
                  Mr
                );
          })(e, t, n);
          if ($r(e) || !0 !== e.multi) {
            const e = this.records.get(r);
            e && void 0 !== e.multi && kn();
          } else {
            let t = this.records.get(r);
            t
              ? void 0 === t.multi && kn()
              : ((t = Ur(void 0, Mr, !0)),
                (t.factory = () => ze(t.multi)),
                this.records.set(r, t)),
              (r = e),
              t.multi.push(e);
          }
          this.records.set(r, s);
        }
        hydrate(e, t) {
          var n;
          return (
            t.value === Rr
              ? (function (e) {
                  throw new Error(`Cannot instantiate cyclic dependency! ${e}`);
                })(de(e))
              : t.value === Mr && ((t.value = Rr), (t.value = t.factory())),
            "object" == typeof t.value &&
              t.value &&
              null !== (n = t.value) &&
              "object" == typeof n &&
              "function" == typeof n.ngOnDestroy &&
              this.onDestroy.add(t.value),
            t.value
          );
        }
        injectableDefInScope(e) {
          return (
            !!e.providedIn &&
            ("string" == typeof e.providedIn
              ? "any" === e.providedIn || e.providedIn === this.scope
              : this.injectorDefTypes.has(e.providedIn))
          );
        }
      }
      function zr(e) {
        const t = se(e),
          n = null !== t ? t.factory : rt(e);
        if (null !== n) return n;
        const r = ie(e);
        if (null !== r) return r.factory;
        if (e instanceof Ae)
          throw new Error(`Token ${de(e)} is missing a \u0275prov definition.`);
        if (e instanceof Function)
          return (function (e) {
            const t = e.length;
            if (t > 0) {
              const n = (function (e, t) {
                const n = [];
                for (let r = 0; r < e; r++) n.push("?");
                return n;
              })(t);
              throw new Error(
                `Can't resolve all parameters for ${de(e)}: (${n.join(", ")}).`
              );
            }
            const n = (function (e) {
              const t = e && (e[le] || e[ue] || (e[ce] && e[ce]()));
              if (t) {
                const n = (function (e) {
                  if (e.hasOwnProperty("name")) return e.name;
                  const t = ("" + e).match(/^function\s*([^\s(]+)/);
                  return null === t ? "" : t[1];
                })(e);
                return (
                  console.warn(
                    `DEPRECATED: DI is instantiating a token "${n}" that inherits its @Injectable decorator but does not provide one itself.\n` +
                      `This will become an error in v10. Please add @Injectable() to the "${n}" class.`
                  ),
                  t
                );
              }
              return null;
            })(e);
            return null !== n ? () => n.factory(e) : () => new e();
          })(e);
        throw new Error("unreachable");
      }
      function Ur(e, t, n = !1) {
        return { factory: e, value: t, multi: n ? [] : void 0 };
      }
      function Br(e) {
        return null !== e && "object" == typeof e && De in e;
      }
      function $r(e) {
        return "function" == typeof e;
      }
      const Zr = function (e, t, n) {
        return (function (e, t = null, n = null, r) {
          const s = Vr(e, t, n, r);
          return s._resolveInjectorDefTypes(), s;
        })({ name: n }, t, e, n);
      };
      let qr = (() => {
          class e {
            static create(e, t) {
              return Array.isArray(e)
                ? Zr(e, t, "")
                : Zr(e.providers, e.parent, e.name || "");
            }
          }
          return (
            (e.THROW_IF_NOT_FOUND = Pe),
            (e.NULL = new Ue()),
            (e.ɵprov = ne({
              token: e,
              providedIn: "any",
              factory: () => Ve(Oe),
            })),
            (e.__NG_ELEMENT_ID__ = -1),
            e
          );
        })(),
        Wr = new Map();
      const Kr = new Set();
      function Jr(e) {
        return "string" == typeof e ? e : e.text();
      }
      function Qr(e, t) {
        let n = e.styles,
          r = e.classes,
          s = 0;
        for (let o = 0; o < t.length; o++) {
          const e = t[o];
          "number" == typeof e
            ? (s = e)
            : 1 == s
            ? (r = pe(r, e))
            : 2 == s && (n = pe(n, e + ": " + t[++o] + ";"));
        }
        null !== n && (e.styles = n), null !== r && (e.classes = r);
      }
      let Gr = null;
      function Xr() {
        if (!Gr) {
          const e = we.Symbol;
          if (e && e.iterator) Gr = e.iterator;
          else {
            const e = Object.getOwnPropertyNames(Map.prototype);
            for (let t = 0; t < e.length; ++t) {
              const n = e[t];
              "entries" !== n &&
                "size" !== n &&
                Map.prototype[n] === Map.prototype.entries &&
                (Gr = n);
            }
          }
        }
        return Gr;
      }
      function Yr(e, t) {
        return (
          e === t ||
          ("number" == typeof e && "number" == typeof t && isNaN(e) && isNaN(t))
        );
      }
      function es(e) {
        return (
          !!ts(e) && (Array.isArray(e) || (!(e instanceof Map) && Xr() in e))
        );
      }
      function ts(e) {
        return null !== e && ("function" == typeof e || "object" == typeof e);
      }
      function ns(e, t, n, r, s) {
        const o = s ? "class" : "style";
        !(function (e, t, n, r, s) {
          for (let o = 0; o < n.length; ) {
            const i = n[o++],
              l = n[o++],
              a = t[i],
              c = e.data[i];
            null !== c.setInput ? c.setInput(a, s, r, l) : (a[l] = s);
          }
        })(e, n, t.inputs[o], o, r);
      }
      function rs(e, t, n, r) {
        const s = Ct(),
          o = xt(),
          i = 20 + e,
          l = s[11],
          a = (s[i] = Vn(t, l, vt.lFrame.currentNamespace)),
          c = o.firstCreatePass
            ? (function (e, t, n, r, s, o, i) {
                const l = t.consts,
                  a = gt(l, o),
                  c = zn(t, n[6], e, 3, s, a);
                return (
                  (function (e, t, n, r) {
                    let s = !1;
                    if (wt()) {
                      const o = (function (e, t, n) {
                          const r = e.directiveRegistry;
                          let s = null;
                          if (r)
                            for (let o = 0; o < r.length; o++) {
                              const i = r[o];
                              Dn(n, i.selectors, !1) &&
                                (s || (s = []),
                                cn(sn(n, t), e, i.type),
                                at(i)
                                  ? (2 & n.flags && En(n),
                                    er(e, n),
                                    s.unshift(i))
                                  : s.push(i));
                            }
                          return s;
                        })(e, t, n),
                        i = null === r ? null : { "": -1 };
                      if (null !== o) {
                        let r = 0;
                        (s = !0), nr(n, e.data.length, o.length);
                        for (let e = 0; e < o.length; e++) {
                          const t = o[e];
                          t.providersResolver && t.providersResolver(t);
                        }
                        Yn(e, n, o.length);
                        let l = !1,
                          a = !1;
                        for (let s = 0; s < o.length; s++) {
                          const c = o[s];
                          (n.mergedAttrs = Kt(n.mergedAttrs, c.hostAttrs)),
                            rr(e, t, c),
                            tr(e.data.length - 1, c, i),
                            null !== c.contentQueries && (n.flags |= 8),
                            (null === c.hostBindings &&
                              null === c.hostAttrs &&
                              0 === c.hostVars) ||
                              (n.flags |= 128),
                            !l &&
                              (c.onChanges || c.onInit || c.doCheck) &&
                              ((e.preOrderHooks || (e.preOrderHooks = [])).push(
                                n.index - 20
                              ),
                              (l = !0)),
                            a ||
                              (!c.onChanges && !c.doCheck) ||
                              ((
                                e.preOrderCheckHooks ||
                                (e.preOrderCheckHooks = [])
                              ).push(n.index - 20),
                              (a = !0)),
                            Qn(e, c),
                            (r += c.hostVars);
                        }
                        !(function (e, t) {
                          const n = t.directiveEnd,
                            r = e.data,
                            s = t.attrs,
                            o = [];
                          let i = null,
                            l = null;
                          for (let a = t.directiveStart; a < n; a++) {
                            const e = r[a],
                              n = e.inputs,
                              c = null === s || In(t) ? null : ir(n, s);
                            o.push(c),
                              (i = Jn(n, a, i)),
                              (l = Jn(e.outputs, a, l));
                          }
                          null !== i &&
                            (i.hasOwnProperty("class") && (t.flags |= 16),
                            i.hasOwnProperty("style") && (t.flags |= 32)),
                            (t.initialInputs = o),
                            (t.inputs = i),
                            (t.outputs = l);
                        })(e, n),
                          Gn(e, t, r);
                      }
                      i &&
                        (function (e, t, n) {
                          if (t) {
                            const r = (e.localNames = []);
                            for (let e = 0; e < t.length; e += 2) {
                              const s = n[t[e + 1]];
                              if (null == s)
                                throw new Error(
                                  `Export of name '${t[e + 1]}' not found!`
                                );
                              r.push(t[e], s);
                            }
                          }
                        })(n, r, i);
                    }
                    n.mergedAttrs = Kt(n.mergedAttrs, n.attrs);
                  })(t, n, c, gt(l, i)),
                  null !== c.mergedAttrs && Qr(c, c.mergedAttrs),
                  null !== t.queries && t.queries.elementStart(t, c),
                  c
                );
              })(e, o, s, 0, t, n, r)
            : o.data[i];
        kt(c, !0);
        const u = c.mergedAttrs;
        null !== u && Wt(l, a, u);
        const h = c.classes;
        null !== h && Ar(l, a, h);
        const d = c.styles;
        null !== d && Ir(l, a, d),
          Er(o, s, a, c),
          0 === vt.lFrame.elementDepthCount && xn(a, s),
          vt.lFrame.elementDepthCount++,
          (function (e) {
            return 1 == (1 & e.flags);
          })(c) &&
            ((function (e, t, n) {
              wt() &&
                ((function (e, t, n, r) {
                  const s = n.directiveStart,
                    o = n.directiveEnd;
                  e.firstCreatePass || sn(n, t), xn(r, t);
                  const i = n.initialInputs;
                  for (let l = s; l < o; l++) {
                    const r = e.data[l],
                      o = at(r);
                    o && sr(t, n, r);
                    const a = dn(t, e, l, n);
                    xn(a, t),
                      null !== i && or(0, l - s, a, r, 0, i),
                      o && (ft(n.index, t)[8] = a);
                  }
                })(e, t, n, pt(n, t)),
                128 == (128 & n.flags) &&
                  (function (e, t, n) {
                    const r = n.directiveStart,
                      s = n.directiveEnd,
                      o = e.expandoInstructions,
                      i = e.firstCreatePass,
                      l = n.index - 20,
                      a = vt.lFrame.currentDirectiveIndex;
                    try {
                      Vt(l);
                      for (let n = r; n < s; n++) {
                        const r = e.data[n],
                          s = t[n];
                        Ot(n),
                          null !== r.hostBindings ||
                          0 !== r.hostVars ||
                          null !== r.hostAttrs
                            ? Xn(r, s)
                            : i && o.push(null);
                      }
                    } finally {
                      Vt(-1), Ot(a);
                    }
                  })(e, t, n));
            })(o, s, c),
            (function (e, t, n) {
              if (lt(t)) {
                const r = t.directiveEnd;
                for (let s = t.directiveStart; s < r; s++) {
                  const t = e.data[s];
                  t.contentQueries && t.contentQueries(1, n[s], s);
                }
              }
            })(o, c, s)),
          null !== r &&
            (function (e, t, n = pt) {
              const r = t.localNames;
              if (null !== r) {
                let s = t.index + 1;
                for (let o = 0; o < r.length; o += 2) {
                  const i = r[o + 1],
                    l = -1 === i ? n(t, e) : e[i];
                  e[s++] = l;
                }
              }
            })(s, c);
      }
      function ss() {
        let e = Et();
        Tt() ? (vt.lFrame.isParent = !1) : ((e = e.parent), kt(e, !1));
        const t = e;
        vt.lFrame.elementDepthCount--;
        const n = xt();
        n.firstCreatePass && (Lt(n, e), lt(e) && n.queries.elementEnd(e)),
          null !== t.classes &&
            (function (e) {
              return 0 != (16 & e.flags);
            })(t) &&
            ns(n, t, Ct(), t.classes, !0),
          null !== t.styles &&
            (function (e) {
              return 0 != (32 & e.flags);
            })(t) &&
            ns(n, t, Ct(), t.styles, !1);
      }
      function os(e, t, n, r) {
        rs(e, t, n, r), ss();
      }
      function is(e) {
        return !!e && "function" == typeof e.then;
      }
      function ls(e, t = "") {
        const n = Ct(),
          r = xt(),
          s = e + 20,
          o = r.firstCreatePass ? zn(r, n[6], e, 3, null, null) : r.data[s],
          i = (n[s] = (function (e, t) {
            return ut(t) ? t.createText(e) : t.createTextNode(e);
          })(t, n[11]));
        Er(r, n, i, o), kt(o, !1);
      }
      function as(e, t) {
        const n = mt(e)[1],
          r = n.data.length - 1;
        Lt(n, { directiveStart: r, directiveEnd: r + 1 });
      }
      class cs {
        constructor(e, t, n) {
          (this.previousValue = e),
            (this.currentValue = t),
            (this.firstChange = n);
        }
        isFirstChange() {
          return this.firstChange;
        }
      }
      class us {}
      class hs {
        resolveComponentFactory(e) {
          throw (function (e) {
            const t = Error(
              `No component factory found for ${de(
                e
              )}. Did you add it to @NgModule.entryComponents?`
            );
            return (t.ngComponent = e), t;
          })(e);
        }
      }
      let ds = (() => {
          class e {}
          return (e.NULL = new hs()), e;
        })(),
        ps = (() => {
          class e {
            constructor(e) {
              this.nativeElement = e;
            }
          }
          return (e.__NG_ELEMENT_ID__ = () => fs(e)), e;
        })();
      const fs = function (e) {
        return Nr(e, Et(), Ct());
      };
      class ms {}
      const ys = (function () {
        var e = { Important: 1, DashCase: 2 };
        return (e[e.Important] = "Important"), (e[e.DashCase] = "DashCase"), e;
      })();
      let gs = (() => {
        class e {}
        return (
          (e.ɵprov = ne({ token: e, providedIn: "root", factory: () => null })),
          e
        );
      })();
      class _s {
        constructor(e) {
          (this.full = e),
            (this.major = e.split(".")[0]),
            (this.minor = e.split(".")[1]),
            (this.patch = e.split(".").slice(2).join("."));
        }
      }
      const bs = new _s("9.1.6");
      class vs {
        constructor() {}
        supports(e) {
          return es(e);
        }
        create(e) {
          return new Cs(e);
        }
      }
      const ws = (e, t) => t;
      class Cs {
        constructor(e) {
          (this.length = 0),
            (this._linkedRecords = null),
            (this._unlinkedRecords = null),
            (this._previousItHead = null),
            (this._itHead = null),
            (this._itTail = null),
            (this._additionsHead = null),
            (this._additionsTail = null),
            (this._movesHead = null),
            (this._movesTail = null),
            (this._removalsHead = null),
            (this._removalsTail = null),
            (this._identityChangesHead = null),
            (this._identityChangesTail = null),
            (this._trackByFn = e || ws);
        }
        forEachItem(e) {
          let t;
          for (t = this._itHead; null !== t; t = t._next) e(t);
        }
        forEachOperation(e) {
          let t = this._itHead,
            n = this._removalsHead,
            r = 0,
            s = null;
          for (; t || n; ) {
            const o = !n || (t && t.currentIndex < Ts(n, r, s)) ? t : n,
              i = Ts(o, r, s),
              l = o.currentIndex;
            if (o === n) r--, (n = n._nextRemoved);
            else if (((t = t._next), null == o.previousIndex)) r++;
            else {
              s || (s = []);
              const e = i - r,
                t = l - r;
              if (e != t) {
                for (let n = 0; n < e; n++) {
                  const r = n < s.length ? s[n] : (s[n] = 0),
                    o = r + n;
                  t <= o && o < e && (s[n] = r + 1);
                }
                s[o.previousIndex] = t - e;
              }
            }
            i !== l && e(o, i, l);
          }
        }
        forEachPreviousItem(e) {
          let t;
          for (t = this._previousItHead; null !== t; t = t._nextPrevious) e(t);
        }
        forEachAddedItem(e) {
          let t;
          for (t = this._additionsHead; null !== t; t = t._nextAdded) e(t);
        }
        forEachMovedItem(e) {
          let t;
          for (t = this._movesHead; null !== t; t = t._nextMoved) e(t);
        }
        forEachRemovedItem(e) {
          let t;
          for (t = this._removalsHead; null !== t; t = t._nextRemoved) e(t);
        }
        forEachIdentityChange(e) {
          let t;
          for (
            t = this._identityChangesHead;
            null !== t;
            t = t._nextIdentityChange
          )
            e(t);
        }
        diff(e) {
          if ((null == e && (e = []), !es(e)))
            throw new Error(
              `Error trying to diff '${de(
                e
              )}'. Only arrays and iterables are allowed`
            );
          return this.check(e) ? this : null;
        }
        onDestroy() {}
        check(e) {
          this._reset();
          let t,
            n,
            r,
            s = this._itHead,
            o = !1;
          if (Array.isArray(e)) {
            this.length = e.length;
            for (let t = 0; t < this.length; t++)
              (n = e[t]),
                (r = this._trackByFn(t, n)),
                null !== s && Yr(s.trackById, r)
                  ? (o && (s = this._verifyReinsertion(s, n, r, t)),
                    Yr(s.item, n) || this._addIdentityChange(s, n))
                  : ((s = this._mismatch(s, n, r, t)), (o = !0)),
                (s = s._next);
          } else
            (t = 0),
              (function (e, t) {
                if (Array.isArray(e))
                  for (let n = 0; n < e.length; n++) t(e[n]);
                else {
                  const n = e[Xr()]();
                  let r;
                  for (; !(r = n.next()).done; ) t(r.value);
                }
              })(e, (e) => {
                (r = this._trackByFn(t, e)),
                  null !== s && Yr(s.trackById, r)
                    ? (o && (s = this._verifyReinsertion(s, e, r, t)),
                      Yr(s.item, e) || this._addIdentityChange(s, e))
                    : ((s = this._mismatch(s, e, r, t)), (o = !0)),
                  (s = s._next),
                  t++;
              }),
              (this.length = t);
          return this._truncate(s), (this.collection = e), this.isDirty;
        }
        get isDirty() {
          return (
            null !== this._additionsHead ||
            null !== this._movesHead ||
            null !== this._removalsHead ||
            null !== this._identityChangesHead
          );
        }
        _reset() {
          if (this.isDirty) {
            let e, t;
            for (
              e = this._previousItHead = this._itHead;
              null !== e;
              e = e._next
            )
              e._nextPrevious = e._next;
            for (e = this._additionsHead; null !== e; e = e._nextAdded)
              e.previousIndex = e.currentIndex;
            for (
              this._additionsHead = this._additionsTail = null,
                e = this._movesHead;
              null !== e;
              e = t
            )
              (e.previousIndex = e.currentIndex), (t = e._nextMoved);
            (this._movesHead = this._movesTail = null),
              (this._removalsHead = this._removalsTail = null),
              (this._identityChangesHead = this._identityChangesTail = null);
          }
        }
        _mismatch(e, t, n, r) {
          let s;
          return (
            null === e ? (s = this._itTail) : ((s = e._prev), this._remove(e)),
            null !==
            (e =
              null === this._linkedRecords
                ? null
                : this._linkedRecords.get(n, r))
              ? (Yr(e.item, t) || this._addIdentityChange(e, t),
                this._moveAfter(e, s, r))
              : null !==
                (e =
                  null === this._unlinkedRecords
                    ? null
                    : this._unlinkedRecords.get(n, null))
              ? (Yr(e.item, t) || this._addIdentityChange(e, t),
                this._reinsertAfter(e, s, r))
              : (e = this._addAfter(new xs(t, n), s, r)),
            e
          );
        }
        _verifyReinsertion(e, t, n, r) {
          let s =
            null === this._unlinkedRecords
              ? null
              : this._unlinkedRecords.get(n, null);
          return (
            null !== s
              ? (e = this._reinsertAfter(s, e._prev, r))
              : e.currentIndex != r &&
                ((e.currentIndex = r), this._addToMoves(e, r)),
            e
          );
        }
        _truncate(e) {
          for (; null !== e; ) {
            const t = e._next;
            this._addToRemovals(this._unlink(e)), (e = t);
          }
          null !== this._unlinkedRecords && this._unlinkedRecords.clear(),
            null !== this._additionsTail &&
              (this._additionsTail._nextAdded = null),
            null !== this._movesTail && (this._movesTail._nextMoved = null),
            null !== this._itTail && (this._itTail._next = null),
            null !== this._removalsTail &&
              (this._removalsTail._nextRemoved = null),
            null !== this._identityChangesTail &&
              (this._identityChangesTail._nextIdentityChange = null);
        }
        _reinsertAfter(e, t, n) {
          null !== this._unlinkedRecords && this._unlinkedRecords.remove(e);
          const r = e._prevRemoved,
            s = e._nextRemoved;
          return (
            null === r ? (this._removalsHead = s) : (r._nextRemoved = s),
            null === s ? (this._removalsTail = r) : (s._prevRemoved = r),
            this._insertAfter(e, t, n),
            this._addToMoves(e, n),
            e
          );
        }
        _moveAfter(e, t, n) {
          return (
            this._unlink(e),
            this._insertAfter(e, t, n),
            this._addToMoves(e, n),
            e
          );
        }
        _addAfter(e, t, n) {
          return (
            this._insertAfter(e, t, n),
            (this._additionsTail =
              null === this._additionsTail
                ? (this._additionsHead = e)
                : (this._additionsTail._nextAdded = e)),
            e
          );
        }
        _insertAfter(e, t, n) {
          const r = null === t ? this._itHead : t._next;
          return (
            (e._next = r),
            (e._prev = t),
            null === r ? (this._itTail = e) : (r._prev = e),
            null === t ? (this._itHead = e) : (t._next = e),
            null === this._linkedRecords && (this._linkedRecords = new ks()),
            this._linkedRecords.put(e),
            (e.currentIndex = n),
            e
          );
        }
        _remove(e) {
          return this._addToRemovals(this._unlink(e));
        }
        _unlink(e) {
          null !== this._linkedRecords && this._linkedRecords.remove(e);
          const t = e._prev,
            n = e._next;
          return (
            null === t ? (this._itHead = n) : (t._next = n),
            null === n ? (this._itTail = t) : (n._prev = t),
            e
          );
        }
        _addToMoves(e, t) {
          return e.previousIndex === t
            ? e
            : ((this._movesTail =
                null === this._movesTail
                  ? (this._movesHead = e)
                  : (this._movesTail._nextMoved = e)),
              e);
        }
        _addToRemovals(e) {
          return (
            null === this._unlinkedRecords &&
              (this._unlinkedRecords = new ks()),
            this._unlinkedRecords.put(e),
            (e.currentIndex = null),
            (e._nextRemoved = null),
            null === this._removalsTail
              ? ((this._removalsTail = this._removalsHead = e),
                (e._prevRemoved = null))
              : ((e._prevRemoved = this._removalsTail),
                (this._removalsTail = this._removalsTail._nextRemoved = e)),
            e
          );
        }
        _addIdentityChange(e, t) {
          return (
            (e.item = t),
            (this._identityChangesTail =
              null === this._identityChangesTail
                ? (this._identityChangesHead = e)
                : (this._identityChangesTail._nextIdentityChange = e)),
            e
          );
        }
      }
      class xs {
        constructor(e, t) {
          (this.item = e),
            (this.trackById = t),
            (this.currentIndex = null),
            (this.previousIndex = null),
            (this._nextPrevious = null),
            (this._prev = null),
            (this._next = null),
            (this._prevDup = null),
            (this._nextDup = null),
            (this._prevRemoved = null),
            (this._nextRemoved = null),
            (this._nextAdded = null),
            (this._nextMoved = null),
            (this._nextIdentityChange = null);
        }
      }
      class Es {
        constructor() {
          (this._head = null), (this._tail = null);
        }
        add(e) {
          null === this._head
            ? ((this._head = this._tail = e),
              (e._nextDup = null),
              (e._prevDup = null))
            : ((this._tail._nextDup = e),
              (e._prevDup = this._tail),
              (e._nextDup = null),
              (this._tail = e));
        }
        get(e, t) {
          let n;
          for (n = this._head; null !== n; n = n._nextDup)
            if ((null === t || t <= n.currentIndex) && Yr(n.trackById, e))
              return n;
          return null;
        }
        remove(e) {
          const t = e._prevDup,
            n = e._nextDup;
          return (
            null === t ? (this._head = n) : (t._nextDup = n),
            null === n ? (this._tail = t) : (n._prevDup = t),
            null === this._head
          );
        }
      }
      class ks {
        constructor() {
          this.map = new Map();
        }
        put(e) {
          const t = e.trackById;
          let n = this.map.get(t);
          n || ((n = new Es()), this.map.set(t, n)), n.add(e);
        }
        get(e, t) {
          const n = this.map.get(e);
          return n ? n.get(e, t) : null;
        }
        remove(e) {
          const t = e.trackById;
          return this.map.get(t).remove(e) && this.map.delete(t), e;
        }
        get isEmpty() {
          return 0 === this.map.size;
        }
        clear() {
          this.map.clear();
        }
      }
      function Ts(e, t, n) {
        const r = e.previousIndex;
        if (null === r) return r;
        let s = 0;
        return n && r < n.length && (s = n[r]), r + t + s;
      }
      class Ss {
        constructor() {}
        supports(e) {
          return e instanceof Map || ts(e);
        }
        create() {
          return new Is();
        }
      }
      class Is {
        constructor() {
          (this._records = new Map()),
            (this._mapHead = null),
            (this._appendAfter = null),
            (this._previousMapHead = null),
            (this._changesHead = null),
            (this._changesTail = null),
            (this._additionsHead = null),
            (this._additionsTail = null),
            (this._removalsHead = null),
            (this._removalsTail = null);
        }
        get isDirty() {
          return (
            null !== this._additionsHead ||
            null !== this._changesHead ||
            null !== this._removalsHead
          );
        }
        forEachItem(e) {
          let t;
          for (t = this._mapHead; null !== t; t = t._next) e(t);
        }
        forEachPreviousItem(e) {
          let t;
          for (t = this._previousMapHead; null !== t; t = t._nextPrevious) e(t);
        }
        forEachChangedItem(e) {
          let t;
          for (t = this._changesHead; null !== t; t = t._nextChanged) e(t);
        }
        forEachAddedItem(e) {
          let t;
          for (t = this._additionsHead; null !== t; t = t._nextAdded) e(t);
        }
        forEachRemovedItem(e) {
          let t;
          for (t = this._removalsHead; null !== t; t = t._nextRemoved) e(t);
        }
        diff(e) {
          if (e) {
            if (!(e instanceof Map || ts(e)))
              throw new Error(
                `Error trying to diff '${de(
                  e
                )}'. Only maps and objects are allowed`
              );
          } else e = new Map();
          return this.check(e) ? this : null;
        }
        onDestroy() {}
        check(e) {
          this._reset();
          let t = this._mapHead;
          if (
            ((this._appendAfter = null),
            this._forEach(e, (e, n) => {
              if (t && t.key === n)
                this._maybeAddToChanges(t, e),
                  (this._appendAfter = t),
                  (t = t._next);
              else {
                const r = this._getOrCreateRecordForKey(n, e);
                t = this._insertBeforeOrAppend(t, r);
              }
            }),
            t)
          ) {
            t._prev && (t._prev._next = null), (this._removalsHead = t);
            for (let e = t; null !== e; e = e._nextRemoved)
              e === this._mapHead && (this._mapHead = null),
                this._records.delete(e.key),
                (e._nextRemoved = e._next),
                (e.previousValue = e.currentValue),
                (e.currentValue = null),
                (e._prev = null),
                (e._next = null);
          }
          return (
            this._changesTail && (this._changesTail._nextChanged = null),
            this._additionsTail && (this._additionsTail._nextAdded = null),
            this.isDirty
          );
        }
        _insertBeforeOrAppend(e, t) {
          if (e) {
            const n = e._prev;
            return (
              (t._next = e),
              (t._prev = n),
              (e._prev = t),
              n && (n._next = t),
              e === this._mapHead && (this._mapHead = t),
              (this._appendAfter = e),
              e
            );
          }
          return (
            this._appendAfter
              ? ((this._appendAfter._next = t), (t._prev = this._appendAfter))
              : (this._mapHead = t),
            (this._appendAfter = t),
            null
          );
        }
        _getOrCreateRecordForKey(e, t) {
          if (this._records.has(e)) {
            const n = this._records.get(e);
            this._maybeAddToChanges(n, t);
            const r = n._prev,
              s = n._next;
            return (
              r && (r._next = s),
              s && (s._prev = r),
              (n._next = null),
              (n._prev = null),
              n
            );
          }
          const n = new As(e);
          return (
            this._records.set(e, n),
            (n.currentValue = t),
            this._addToAdditions(n),
            n
          );
        }
        _reset() {
          if (this.isDirty) {
            let e;
            for (
              this._previousMapHead = this._mapHead, e = this._previousMapHead;
              null !== e;
              e = e._next
            )
              e._nextPrevious = e._next;
            for (e = this._changesHead; null !== e; e = e._nextChanged)
              e.previousValue = e.currentValue;
            for (e = this._additionsHead; null != e; e = e._nextAdded)
              e.previousValue = e.currentValue;
            (this._changesHead = this._changesTail = null),
              (this._additionsHead = this._additionsTail = null),
              (this._removalsHead = null);
          }
        }
        _maybeAddToChanges(e, t) {
          Yr(t, e.currentValue) ||
            ((e.previousValue = e.currentValue),
            (e.currentValue = t),
            this._addToChanges(e));
        }
        _addToAdditions(e) {
          null === this._additionsHead
            ? (this._additionsHead = this._additionsTail = e)
            : ((this._additionsTail._nextAdded = e), (this._additionsTail = e));
        }
        _addToChanges(e) {
          null === this._changesHead
            ? (this._changesHead = this._changesTail = e)
            : ((this._changesTail._nextChanged = e), (this._changesTail = e));
        }
        _forEach(e, t) {
          e instanceof Map
            ? e.forEach(t)
            : Object.keys(e).forEach((n) => t(e[n], n));
        }
      }
      class As {
        constructor(e) {
          (this.key = e),
            (this.previousValue = null),
            (this.currentValue = null),
            (this._nextPrevious = null),
            (this._next = null),
            (this._prev = null),
            (this._nextAdded = null),
            (this._nextRemoved = null),
            (this._nextChanged = null);
        }
      }
      let Os = (() => {
          class e {
            constructor(e) {
              this.factories = e;
            }
            static create(t, n) {
              if (null != n) {
                const e = n.factories.slice();
                t = t.concat(e);
              }
              return new e(t);
            }
            static extend(t) {
              return {
                provide: e,
                useFactory: (n) => {
                  if (!n)
                    throw new Error(
                      "Cannot extend IterableDiffers without a parent injector"
                    );
                  return e.create(t, n);
                },
                deps: [[e, new Y(), new G()]],
              };
            }
            find(e) {
              const t = this.factories.find((t) => t.supports(e));
              if (null != t) return t;
              throw new Error(
                `Cannot find a differ supporting object '${e}' of type '${
                  ((n = e), n.name || typeof n)
                }'`
              );
              var n;
            }
          }
          return (
            (e.ɵprov = ne({
              token: e,
              providedIn: "root",
              factory: () => new e([new vs()]),
            })),
            e
          );
        })(),
        Ps = (() => {
          class e {
            constructor(e) {
              this.factories = e;
            }
            static create(t, n) {
              if (n) {
                const e = n.factories.slice();
                t = t.concat(e);
              }
              return new e(t);
            }
            static extend(t) {
              return {
                provide: e,
                useFactory: (n) => {
                  if (!n)
                    throw new Error(
                      "Cannot extend KeyValueDiffers without a parent injector"
                    );
                  return e.create(t, n);
                },
                deps: [[e, new Y(), new G()]],
              };
            }
            find(e) {
              const t = this.factories.find((t) => t.supports(e));
              if (t) return t;
              throw new Error(`Cannot find a differ supporting object '${e}'`);
            }
          }
          return (
            (e.ɵprov = ne({
              token: e,
              providedIn: "root",
              factory: () => new e([new Ss()]),
            })),
            e
          );
        })();
      const Ns = [new Ss()],
        Ds = new Os([new vs()]),
        Ms = new Ps(Ns),
        Rs = {};
      class js extends ds {
        constructor(e) {
          super(), (this.ngModule = e);
        }
        resolveComponentFactory(e) {
          const t = nt(e);
          return new Vs(t, this.ngModule);
        }
      }
      function Hs(e) {
        const t = [];
        for (let n in e)
          e.hasOwnProperty(n) && t.push({ propName: e[n], templateName: n });
        return t;
      }
      const Fs = new Ae("SCHEDULER_TOKEN", {
        providedIn: "root",
        factory: () => Yt,
      });
      class Vs extends us {
        constructor(e, t) {
          super(),
            (this.componentDef = e),
            (this.ngModule = t),
            (this.componentType = e.type),
            (this.selector = e.selectors.map(Rn).join(",")),
            (this.ngContentSelectors = e.ngContentSelectors
              ? e.ngContentSelectors
              : []),
            (this.isBoundToModule = !!t);
        }
        get inputs() {
          return Hs(this.componentDef.inputs);
        }
        get outputs() {
          return Hs(this.componentDef.outputs);
        }
        create(e, t, n, r) {
          const s = (r = r || this.ngModule)
              ? (function (e, t) {
                  return {
                    get: (n, r, s) => {
                      const o = e.get(n, Rs, s);
                      return o !== Rs || r === Rs ? o : t.get(n, r, s);
                    },
                  };
                })(e, r.injector)
              : e,
            o = s.get(ms, ht),
            i = s.get(gs, null),
            l = o.createRenderer(null, this.componentDef),
            a = this.componentDef.selectors[0][0] || "div",
            c = n
              ? (function (e, t, n) {
                  if (ut(e)) return e.selectRootElement(t, n === qe.ShadowDom);
                  let r = "string" == typeof t ? e.querySelector(t) : t;
                  return (r.textContent = ""), r;
                })(l, n, this.componentDef.encapsulation)
              : Vn(
                  a,
                  o.createRenderer(null, this.componentDef),
                  (function (e) {
                    const t = e.toLowerCase();
                    return "svg" === t
                      ? "http://www.w3.org/2000/svg"
                      : "math" === t
                      ? "http://www.w3.org/1998/MathML/"
                      : null;
                  })(a)
                ),
            u = this.componentDef.onPush ? 576 : 528,
            h =
              "string" == typeof n && /^#root-ng-internal-isolated-\d+/.test(n),
            d = {
              components: [],
              scheduler: Yt,
              clean: mr,
              playerHandler: null,
              flags: 0,
            },
            p = Wn(0, -1, null, 1, 0, null, null, null, null, null),
            f = Ln(null, p, d, u, null, null, o, l, i, s);
          let m, y;
          Dt(f, null);
          try {
            const e = (function (e, t, n, r, s, o) {
              const i = n[1];
              n[20] = e;
              const l = zn(i, null, 0, 3, null, null),
                a = (l.mergedAttrs = t.hostAttrs);
              null !== a &&
                (Qr(l, a),
                null !== e &&
                  (Wt(s, e, a),
                  null !== l.classes && Ar(s, e, l.classes),
                  null !== l.styles && Ir(s, e, l.styles)));
              const c = r.createRenderer(e, t),
                u = Ln(
                  n,
                  qn(t),
                  null,
                  t.onPush ? 64 : 16,
                  n[20],
                  l,
                  r,
                  c,
                  void 0
                );
              return (
                i.firstCreatePass &&
                  (cn(sn(l, n), i, t.type), er(i, l), nr(l, n.length, 1)),
                hr(n, u),
                (n[20] = u)
              );
            })(c, this.componentDef, f, o, l);
            if (c)
              if (n) Wt(l, c, ["ng-version", bs.full]);
              else {
                const { attrs: e, classes: t } = (function (e) {
                  const t = [],
                    n = [];
                  let r = 1,
                    s = 2;
                  for (; r < e.length; ) {
                    let o = e[r];
                    if ("string" == typeof o)
                      2 === s
                        ? "" !== o && t.push(o, e[++r])
                        : 8 === s && n.push(o);
                    else {
                      if (!Pn(s)) break;
                      s = o;
                    }
                    r++;
                  }
                  return { attrs: t, classes: n };
                })(this.componentDef.selectors[0]);
                e && Wt(l, c, e), t && t.length > 0 && Ar(l, c, t.join(" "));
              }
            if (((y = p.data[20]), void 0 !== t)) {
              const e = (y.projection = []);
              for (let n = 0; n < this.ngContentSelectors.length; n++) {
                const r = t[n];
                e.push(null != r ? Array.from(r) : null);
              }
            }
            (m = (function (e, t, n, r, s) {
              const o = n[1],
                i = (function (e, t, n) {
                  const r = Et();
                  e.firstCreatePass &&
                    (n.providersResolver && n.providersResolver(n),
                    Yn(e, r, 1),
                    rr(e, t, n));
                  const s = dn(t, e, t.length - 1, r);
                  xn(s, t);
                  const o = pt(r, t);
                  return o && xn(o, t), s;
                })(o, n, t);
              r.components.push(i),
                (e[8] = i),
                s && s.forEach((e) => e(i, t)),
                t.contentQueries && t.contentQueries(1, i, n.length - 1);
              const l = Et();
              if (
                o.firstCreatePass &&
                (null !== t.hostBindings || null !== t.hostAttrs)
              ) {
                Vt(l.index - 20);
                const e = n[1];
                Qn(e, t), Gn(e, n, t.hostVars), Xn(t, i);
              }
              return i;
            })(e, this.componentDef, f, d, [as])),
              Un(p, f, null);
          } finally {
            Ft();
          }
          const g = new Ls(this.componentType, m, Nr(ps, y, f), f, y);
          return (n && !h) || (g.hostView._tViewNode.child = y), g;
        }
      }
      class Ls extends class {} {
        constructor(e, t, n, r, s) {
          super(),
            (this.location = n),
            (this._rootLView = r),
            (this._tNode = s),
            (this.destroyCbs = []),
            (this.instance = t),
            (this.hostView = this.changeDetectorRef = new Or(r)),
            (this.hostView._tViewNode = (function (e, t, n, r) {
              let s = e.node;
              return (
                null == s && (e.node = s = Kn(0, null, 2, -1, null, null)),
                (r[6] = s)
              );
            })(r[1], 0, 0, r)),
            (this.componentType = e);
        }
        get injector() {
          return new mn(this._tNode, this._rootLView);
        }
        destroy() {
          this.destroyCbs &&
            (this.destroyCbs.forEach((e) => e()),
            (this.destroyCbs = null),
            !this.hostView.destroyed && this.hostView.destroy());
        }
        onDestroy(e) {
          this.destroyCbs && this.destroyCbs.push(e);
        }
      }
      const zs = void 0;
      var Us = [
        "en",
        [["a", "p"], ["AM", "PM"], zs],
        [["AM", "PM"], zs, zs],
        [
          ["S", "M", "T", "W", "T", "F", "S"],
          ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
          [
            "Sunday",
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday",
          ],
          ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
        ],
        zs,
        [
          ["J", "F", "M", "A", "M", "J", "J", "A", "S", "O", "N", "D"],
          [
            "Jan",
            "Feb",
            "Mar",
            "Apr",
            "May",
            "Jun",
            "Jul",
            "Aug",
            "Sep",
            "Oct",
            "Nov",
            "Dec",
          ],
          [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December",
          ],
        ],
        zs,
        [
          ["B", "A"],
          ["BC", "AD"],
          ["Before Christ", "Anno Domini"],
        ],
        0,
        [6, 0],
        ["M/d/yy", "MMM d, y", "MMMM d, y", "EEEE, MMMM d, y"],
        ["h:mm a", "h:mm:ss a", "h:mm:ss a z", "h:mm:ss a zzzz"],
        ["{1}, {0}", zs, "{1} 'at' {0}", zs],
        [
          ".",
          ",",
          ";",
          "%",
          "+",
          "-",
          "E",
          "\xd7",
          "\u2030",
          "\u221e",
          "NaN",
          ":",
        ],
        ["#,##0.###", "#,##0%", "\xa4#,##0.00", "#E0"],
        "USD",
        "$",
        "US Dollar",
        {},
        "ltr",
        function (e) {
          let t = Math.floor(Math.abs(e)),
            n = e.toString().replace(/^[^.]*\.?/, "").length;
          return 1 === t && 0 === n ? 1 : 5;
        },
      ];
      let Bs = {};
      function $s(e) {
        return (
          e in Bs ||
            (Bs[e] =
              we.ng &&
              we.ng.common &&
              we.ng.common.locales &&
              we.ng.common.locales[e]),
          Bs[e]
        );
      }
      const Zs = (function () {
        var e = {
          LocaleId: 0,
          DayPeriodsFormat: 1,
          DayPeriodsStandalone: 2,
          DaysFormat: 3,
          DaysStandalone: 4,
          MonthsFormat: 5,
          MonthsStandalone: 6,
          Eras: 7,
          FirstDayOfWeek: 8,
          WeekendRange: 9,
          DateFormat: 10,
          TimeFormat: 11,
          DateTimeFormat: 12,
          NumberSymbols: 13,
          NumberFormats: 14,
          CurrencyCode: 15,
          CurrencySymbol: 16,
          CurrencyName: 17,
          Currencies: 18,
          Directionality: 19,
          PluralCase: 20,
          ExtraData: 21,
        };
        return (
          (e[e.LocaleId] = "LocaleId"),
          (e[e.DayPeriodsFormat] = "DayPeriodsFormat"),
          (e[e.DayPeriodsStandalone] = "DayPeriodsStandalone"),
          (e[e.DaysFormat] = "DaysFormat"),
          (e[e.DaysStandalone] = "DaysStandalone"),
          (e[e.MonthsFormat] = "MonthsFormat"),
          (e[e.MonthsStandalone] = "MonthsStandalone"),
          (e[e.Eras] = "Eras"),
          (e[e.FirstDayOfWeek] = "FirstDayOfWeek"),
          (e[e.WeekendRange] = "WeekendRange"),
          (e[e.DateFormat] = "DateFormat"),
          (e[e.TimeFormat] = "TimeFormat"),
          (e[e.DateTimeFormat] = "DateTimeFormat"),
          (e[e.NumberSymbols] = "NumberSymbols"),
          (e[e.NumberFormats] = "NumberFormats"),
          (e[e.CurrencyCode] = "CurrencyCode"),
          (e[e.CurrencySymbol] = "CurrencySymbol"),
          (e[e.CurrencyName] = "CurrencyName"),
          (e[e.Currencies] = "Currencies"),
          (e[e.Directionality] = "Directionality"),
          (e[e.PluralCase] = "PluralCase"),
          (e[e.ExtraData] = "ExtraData"),
          e
        );
      })();
      let qs = "en-US";
      function Ws(e) {
        var t, n;
        (n = "Expected localeId to be defined"),
          null == (t = e) &&
            (function (e, t, n, r) {
              throw new Error(
                `ASSERTION ERROR: ${e}` + ` [Expected=> null != ${t} <=Actual]`
              );
            })(n, t),
          "string" == typeof e && (qs = e.toLowerCase().replace(/_/g, "-"));
      }
      const Ks = new Map();
      class Js extends Be {
        constructor(e, t) {
          super(),
            (this._parent = t),
            (this._bootstrapComponents = []),
            (this.injector = this),
            (this.destroyCbs = []),
            (this.componentFactoryResolver = new js(this));
          const n = st(e),
            r = e[Te] || null;
          r && Ws(r),
            (this._bootstrapComponents = en(n.bootstrap)),
            (this._r3Injector = Vr(
              e,
              t,
              [
                { provide: Be, useValue: this },
                { provide: ds, useValue: this.componentFactoryResolver },
              ],
              de(e)
            )),
            this._r3Injector._resolveInjectorDefTypes(),
            (this.instance = this.get(e));
        }
        get(e, t = qr.THROW_IF_NOT_FOUND, n = ee.Default) {
          return e === qr || e === Be || e === Oe
            ? this
            : this._r3Injector.get(e, t, n);
        }
        destroy() {
          const e = this._r3Injector;
          !e.destroyed && e.destroy(),
            this.destroyCbs.forEach((e) => e()),
            (this.destroyCbs = null);
        }
        onDestroy(e) {
          this.destroyCbs.push(e);
        }
      }
      class Qs extends class {} {
        constructor(e) {
          super(),
            (this.moduleType = e),
            null !== st(e) &&
              (function e(t) {
                if (null !== t.ɵmod.id) {
                  const e = t.ɵmod.id;
                  (function (e, t, n) {
                    if (t && t !== n)
                      throw new Error(
                        `Duplicate module registered for ${e} - ${de(
                          t
                        )} vs ${de(t.name)}`
                      );
                  })(e, Ks.get(e), t),
                    Ks.set(e, t);
                }
                let n = t.ɵmod.imports;
                n instanceof Function && (n = n()), n && n.forEach((t) => e(t));
              })(e);
        }
        create(e) {
          return new Js(this.moduleType, e);
        }
      }
      class Gs extends x {
        constructor(e = !1) {
          super(), (this.__isAsync = e);
        }
        emit(e) {
          super.next(e);
        }
        subscribe(e, t, n) {
          let r,
            s = (e) => null,
            o = () => null;
          e && "object" == typeof e
            ? ((r = this.__isAsync
                ? (t) => {
                    setTimeout(() => e.next(t));
                  }
                : (t) => {
                    e.next(t);
                  }),
              e.error &&
                (s = this.__isAsync
                  ? (t) => {
                      setTimeout(() => e.error(t));
                    }
                  : (t) => {
                      e.error(t);
                    }),
              e.complete &&
                (o = this.__isAsync
                  ? () => {
                      setTimeout(() => e.complete());
                    }
                  : () => {
                      e.complete();
                    }))
            : ((r = this.__isAsync
                ? (t) => {
                    setTimeout(() => e(t));
                  }
                : (t) => {
                    e(t);
                  }),
              t &&
                (s = this.__isAsync
                  ? (e) => {
                      setTimeout(() => t(e));
                    }
                  : (e) => {
                      t(e);
                    }),
              n &&
                (o = this.__isAsync
                  ? () => {
                      setTimeout(() => n());
                    }
                  : () => {
                      n();
                    }));
          const i = super.subscribe(r, s, o);
          return e instanceof h && e.add(i), i;
        }
      }
      const Xs = new Ae("Application Initializer");
      let Ys = (() => {
        class e {
          constructor(e) {
            (this.appInits = e),
              (this.initialized = !1),
              (this.done = !1),
              (this.donePromise = new Promise((e, t) => {
                (this.resolve = e), (this.reject = t);
              }));
          }
          runInitializers() {
            if (this.initialized) return;
            const e = [],
              t = () => {
                (this.done = !0), this.resolve();
              };
            if (this.appInits)
              for (let n = 0; n < this.appInits.length; n++) {
                const t = this.appInits[n]();
                is(t) && e.push(t);
              }
            Promise.all(e)
              .then(() => {
                t();
              })
              .catch((e) => {
                this.reject(e);
              }),
              0 === e.length && t(),
              (this.initialized = !0);
          }
        }
        return (
          (e.ɵfac = function (t) {
            return new (t || e)(Ve(Xs, 8));
          }),
          (e.ɵprov = ne({ token: e, factory: e.ɵfac })),
          e
        );
      })();
      const eo = new Ae("AppId"),
        to = {
          provide: eo,
          useFactory: function () {
            return `${no()}${no()}${no()}`;
          },
          deps: [],
        };
      function no() {
        return String.fromCharCode(97 + Math.floor(25 * Math.random()));
      }
      const ro = new Ae("Platform Initializer"),
        so = new Ae("Platform ID"),
        oo = new Ae("appBootstrapListener");
      let io = (() => {
        class e {
          log(e) {
            console.log(e);
          }
          warn(e) {
            console.warn(e);
          }
        }
        return (
          (e.ɵfac = function (t) {
            return new (t || e)();
          }),
          (e.ɵprov = ne({ token: e, factory: e.ɵfac })),
          e
        );
      })();
      const lo = new Ae("LocaleId"),
        ao = new Ae("DefaultCurrencyCode");
      class co {
        constructor(e, t) {
          (this.ngModuleFactory = e), (this.componentFactories = t);
        }
      }
      const uo = function (e) {
          return new Qs(e);
        },
        ho = uo,
        po = function (e) {
          return Promise.resolve(uo(e));
        },
        fo = function (e) {
          const t = uo(e),
            n = en(st(e).declarations).reduce((e, t) => {
              const n = nt(t);
              return n && e.push(new Vs(n)), e;
            }, []);
          return new co(t, n);
        },
        mo = fo,
        yo = function (e) {
          return Promise.resolve(fo(e));
        };
      let go = (() => {
        class e {
          constructor() {
            (this.compileModuleSync = ho),
              (this.compileModuleAsync = po),
              (this.compileModuleAndAllComponentsSync = mo),
              (this.compileModuleAndAllComponentsAsync = yo);
          }
          clearCache() {}
          clearCacheFor(e) {}
          getModuleId(e) {}
        }
        return (
          (e.ɵfac = function (t) {
            return new (t || e)();
          }),
          (e.ɵprov = ne({ token: e, factory: e.ɵfac })),
          e
        );
      })();
      const _o = new Ae("compilerOptions"),
        bo = (() => Promise.resolve(0))();
      function vo(e) {
        "undefined" == typeof Zone
          ? bo.then(() => {
              e && e.apply(null, null);
            })
          : Zone.current.scheduleMicroTask("scheduleMicrotask", e);
      }
      class wo {
        constructor({
          enableLongStackTrace: e = !1,
          shouldCoalesceEventChangeDetection: t = !1,
        }) {
          if (
            ((this.hasPendingMacrotasks = !1),
            (this.hasPendingMicrotasks = !1),
            (this.isStable = !0),
            (this.onUnstable = new Gs(!1)),
            (this.onMicrotaskEmpty = new Gs(!1)),
            (this.onStable = new Gs(!1)),
            (this.onError = new Gs(!1)),
            "undefined" == typeof Zone)
          )
            throw new Error("In this configuration Angular requires Zone.js");
          Zone.assertZonePatched(),
            (this._nesting = 0),
            (this._outer = this._inner = Zone.current),
            Zone.wtfZoneSpec &&
              (this._inner = this._inner.fork(Zone.wtfZoneSpec)),
            Zone.TaskTrackingZoneSpec &&
              (this._inner = this._inner.fork(new Zone.TaskTrackingZoneSpec())),
            e &&
              Zone.longStackTraceZoneSpec &&
              (this._inner = this._inner.fork(Zone.longStackTraceZoneSpec)),
            (this.shouldCoalesceEventChangeDetection = t),
            (this.lastRequestAnimationFrameId = -1),
            (this.nativeRequestAnimationFrame = (function () {
              let e = we.requestAnimationFrame,
                t = we.cancelAnimationFrame;
              if ("undefined" != typeof Zone && e && t) {
                const n = e[Zone.__symbol__("OriginalDelegate")];
                n && (e = n);
                const r = t[Zone.__symbol__("OriginalDelegate")];
                r && (t = r);
              }
              return {
                nativeRequestAnimationFrame: e,
                nativeCancelAnimationFrame: t,
              };
            })().nativeRequestAnimationFrame),
            (function (e) {
              const t =
                !!e.shouldCoalesceEventChangeDetection &&
                e.nativeRequestAnimationFrame &&
                (() => {
                  !(function (e) {
                    -1 === e.lastRequestAnimationFrameId &&
                      ((e.lastRequestAnimationFrameId = e.nativeRequestAnimationFrame.call(
                        we,
                        () => {
                          (e.lastRequestAnimationFrameId = -1), ko(e), Eo(e);
                        }
                      )),
                      ko(e));
                  })(e);
                });
              e._inner = e._inner.fork({
                name: "angular",
                properties: { isAngularZone: !0, maybeDelayChangeDetection: t },
                onInvokeTask: (n, r, s, o, i, l) => {
                  try {
                    return To(e), n.invokeTask(s, o, i, l);
                  } finally {
                    t && "eventTask" === o.type && t(), So(e);
                  }
                },
                onInvoke: (t, n, r, s, o, i, l) => {
                  try {
                    return To(e), t.invoke(r, s, o, i, l);
                  } finally {
                    So(e);
                  }
                },
                onHasTask: (t, n, r, s) => {
                  t.hasTask(r, s),
                    n === r &&
                      ("microTask" == s.change
                        ? ((e._hasPendingMicrotasks = s.microTask),
                          ko(e),
                          Eo(e))
                        : "macroTask" == s.change &&
                          (e.hasPendingMacrotasks = s.macroTask));
                },
                onHandleError: (t, n, r, s) => (
                  t.handleError(r, s),
                  e.runOutsideAngular(() => e.onError.emit(s)),
                  !1
                ),
              });
            })(this);
        }
        static isInAngularZone() {
          return !0 === Zone.current.get("isAngularZone");
        }
        static assertInAngularZone() {
          if (!wo.isInAngularZone())
            throw new Error("Expected to be in Angular Zone, but it is not!");
        }
        static assertNotInAngularZone() {
          if (wo.isInAngularZone())
            throw new Error("Expected to not be in Angular Zone, but it is!");
        }
        run(e, t, n) {
          return this._inner.run(e, t, n);
        }
        runTask(e, t, n, r) {
          const s = this._inner,
            o = s.scheduleEventTask("NgZoneEvent: " + r, e, xo, Co, Co);
          try {
            return s.runTask(o, t, n);
          } finally {
            s.cancelTask(o);
          }
        }
        runGuarded(e, t, n) {
          return this._inner.runGuarded(e, t, n);
        }
        runOutsideAngular(e) {
          return this._outer.run(e);
        }
      }
      function Co() {}
      const xo = {};
      function Eo(e) {
        if (0 == e._nesting && !e.hasPendingMicrotasks && !e.isStable)
          try {
            e._nesting++, e.onMicrotaskEmpty.emit(null);
          } finally {
            if ((e._nesting--, !e.hasPendingMicrotasks))
              try {
                e.runOutsideAngular(() => e.onStable.emit(null));
              } finally {
                e.isStable = !0;
              }
          }
      }
      function ko(e) {
        e.hasPendingMicrotasks = !!(
          e._hasPendingMicrotasks ||
          (e.shouldCoalesceEventChangeDetection &&
            -1 !== e.lastRequestAnimationFrameId)
        );
      }
      function To(e) {
        e._nesting++,
          e.isStable && ((e.isStable = !1), e.onUnstable.emit(null));
      }
      function So(e) {
        e._nesting--, Eo(e);
      }
      class Io {
        constructor() {
          (this.hasPendingMicrotasks = !1),
            (this.hasPendingMacrotasks = !1),
            (this.isStable = !0),
            (this.onUnstable = new Gs()),
            (this.onMicrotaskEmpty = new Gs()),
            (this.onStable = new Gs()),
            (this.onError = new Gs());
        }
        run(e, t, n) {
          return e.apply(t, n);
        }
        runGuarded(e, t, n) {
          return e.apply(t, n);
        }
        runOutsideAngular(e) {
          return e();
        }
        runTask(e, t, n, r) {
          return e.apply(t, n);
        }
      }
      let Ao = (() => {
          class e {
            constructor(e) {
              (this._ngZone = e),
                (this._pendingCount = 0),
                (this._isZoneStable = !0),
                (this._didWork = !1),
                (this._callbacks = []),
                (this.taskTrackingZone = null),
                this._watchAngularEvents(),
                e.run(() => {
                  this.taskTrackingZone =
                    "undefined" == typeof Zone
                      ? null
                      : Zone.current.get("TaskTrackingZone");
                });
            }
            _watchAngularEvents() {
              this._ngZone.onUnstable.subscribe({
                next: () => {
                  (this._didWork = !0), (this._isZoneStable = !1);
                },
              }),
                this._ngZone.runOutsideAngular(() => {
                  this._ngZone.onStable.subscribe({
                    next: () => {
                      wo.assertNotInAngularZone(),
                        vo(() => {
                          (this._isZoneStable = !0),
                            this._runCallbacksIfReady();
                        });
                    },
                  });
                });
            }
            increasePendingRequestCount() {
              return (
                (this._pendingCount += 1),
                (this._didWork = !0),
                this._pendingCount
              );
            }
            decreasePendingRequestCount() {
              if (((this._pendingCount -= 1), this._pendingCount < 0))
                throw new Error("pending async requests below zero");
              return this._runCallbacksIfReady(), this._pendingCount;
            }
            isStable() {
              return (
                this._isZoneStable &&
                0 === this._pendingCount &&
                !this._ngZone.hasPendingMacrotasks
              );
            }
            _runCallbacksIfReady() {
              if (this.isStable())
                vo(() => {
                  for (; 0 !== this._callbacks.length; ) {
                    let e = this._callbacks.pop();
                    clearTimeout(e.timeoutId), e.doneCb(this._didWork);
                  }
                  this._didWork = !1;
                });
              else {
                let e = this.getPendingTasks();
                (this._callbacks = this._callbacks.filter(
                  (t) =>
                    !t.updateCb ||
                    !t.updateCb(e) ||
                    (clearTimeout(t.timeoutId), !1)
                )),
                  (this._didWork = !0);
              }
            }
            getPendingTasks() {
              return this.taskTrackingZone
                ? this.taskTrackingZone.macroTasks.map((e) => ({
                    source: e.source,
                    creationLocation: e.creationLocation,
                    data: e.data,
                  }))
                : [];
            }
            addCallback(e, t, n) {
              let r = -1;
              t &&
                t > 0 &&
                (r = setTimeout(() => {
                  (this._callbacks = this._callbacks.filter(
                    (e) => e.timeoutId !== r
                  )),
                    e(this._didWork, this.getPendingTasks());
                }, t)),
                this._callbacks.push({ doneCb: e, timeoutId: r, updateCb: n });
            }
            whenStable(e, t, n) {
              if (n && !this.taskTrackingZone)
                throw new Error(
                  'Task tracking zone is required when passing an update callback to whenStable(). Is "zone.js/dist/task-tracking.js" loaded?'
                );
              this.addCallback(e, t, n), this._runCallbacksIfReady();
            }
            getPendingRequestCount() {
              return this._pendingCount;
            }
            findProviders(e, t, n) {
              return [];
            }
          }
          return (
            (e.ɵfac = function (t) {
              return new (t || e)(Ve(wo));
            }),
            (e.ɵprov = ne({ token: e, factory: e.ɵfac })),
            e
          );
        })(),
        Oo = (() => {
          class e {
            constructor() {
              (this._applications = new Map()), Do.addToWindow(this);
            }
            registerApplication(e, t) {
              this._applications.set(e, t);
            }
            unregisterApplication(e) {
              this._applications.delete(e);
            }
            unregisterAllApplications() {
              this._applications.clear();
            }
            getTestability(e) {
              return this._applications.get(e) || null;
            }
            getAllTestabilities() {
              return Array.from(this._applications.values());
            }
            getAllRootElements() {
              return Array.from(this._applications.keys());
            }
            findTestabilityInTree(e, t = !0) {
              return Do.findTestabilityInTree(this, e, t);
            }
          }
          return (
            (e.ɵfac = function (t) {
              return new (t || e)();
            }),
            (e.ɵprov = ne({ token: e, factory: e.ɵfac })),
            e
          );
        })();
      class Po {
        addToWindow(e) {}
        findTestabilityInTree(e, t, n) {
          return null;
        }
      }
      let No,
        Do = new Po(),
        Mo = function (e, t, n) {
          const r = e.get(_o, []).concat(t),
            s = new Qs(n);
          if (0 === Wr.size) return Promise.resolve(s);
          const o = (function (e) {
            const t = [];
            return e.forEach((e) => e && t.push(...e)), t;
          })(r.map((e) => e.providers));
          if (0 === o.length) return Promise.resolve(s);
          const i = (function () {
              const e = we.ng;
              if (!e || !e.ɵcompilerFacade)
                throw new Error(
                  "Angular JIT compilation failed: '@angular/compiler' not loaded!\n  - JIT compilation is discouraged for production use-cases! Consider AOT mode instead.\n  - Did you bootstrap using '@angular/platform-browser-dynamic' or '@angular/platform-server'?\n  - Alternatively provide the compiler with 'import \"@angular/compiler\";' before bootstrapping."
                );
              return e.ɵcompilerFacade;
            })(),
            l = qr.create({ providers: o }).get(i.ResourceLoader);
          return (function (e) {
            const t = [],
              n = new Map();
            function r(e) {
              let t = n.get(e);
              if (!t) {
                const r = ((e) => Promise.resolve(l.get(e)))(e);
                n.set(e, (t = r.then(Jr)));
              }
              return t;
            }
            return (
              Wr.forEach((e, n) => {
                const s = [];
                e.templateUrl &&
                  s.push(
                    r(e.templateUrl).then((t) => {
                      e.template = t;
                    })
                  );
                const o = e.styleUrls,
                  i = e.styles || (e.styles = []),
                  l = e.styles.length;
                o &&
                  o.forEach((t, n) => {
                    i.push(""),
                      s.push(
                        r(t).then((r) => {
                          (i[l + n] = r),
                            o.splice(o.indexOf(t), 1),
                            0 == o.length && (e.styleUrls = void 0);
                        })
                      );
                  });
                const a = Promise.all(s).then(() =>
                  (function (e) {
                    Kr.delete(e);
                  })(n)
                );
                t.push(a);
              }),
              (Wr = new Map()),
              Promise.all(t).then(() => {})
            );
          })().then(() => s);
        };
      const Ro = new Ae("AllowMultipleToken");
      function jo(e, t, n = []) {
        const r = `Platform: ${t}`,
          s = new Ae(r);
        return (t = []) => {
          let o = Ho();
          if (!o || o.injector.get(Ro, !1))
            if (e) e(n.concat(t).concat({ provide: s, useValue: !0 }));
            else {
              const e = n
                .concat(t)
                .concat(
                  { provide: s, useValue: !0 },
                  { provide: Dr, useValue: "platform" }
                );
              !(function (e) {
                if (No && !No.destroyed && !No.injector.get(Ro, !1))
                  throw new Error(
                    "There can be only one platform. Destroy the previous one to create a new one."
                  );
                No = e.get(Fo);
                const t = e.get(ro, null);
                t && t.forEach((e) => e());
              })(qr.create({ providers: e, name: r }));
            }
          return (function (e) {
            const t = Ho();
            if (!t) throw new Error("No platform exists!");
            if (!t.injector.get(e, null))
              throw new Error(
                "A platform with a different configuration has been created. Please destroy it first."
              );
            return t;
          })(s);
        };
      }
      function Ho() {
        return No && !No.destroyed ? No : null;
      }
      let Fo = (() => {
        class e {
          constructor(e) {
            (this._injector = e),
              (this._modules = []),
              (this._destroyListeners = []),
              (this._destroyed = !1);
          }
          bootstrapModuleFactory(e, t) {
            const n = (function (e, t) {
                let n;
                return (
                  (n =
                    "noop" === e
                      ? new Io()
                      : ("zone.js" === e ? void 0 : e) ||
                        new wo({
                          enableLongStackTrace: Cn(),
                          shouldCoalesceEventChangeDetection: t,
                        })),
                  n
                );
              })(t ? t.ngZone : void 0, (t && t.ngZoneEventCoalescing) || !1),
              r = [{ provide: wo, useValue: n }];
            return n.run(() => {
              const t = qr.create({
                  providers: r,
                  parent: this.injector,
                  name: e.moduleType.name,
                }),
                s = e.create(t),
                o = s.injector.get(bn, null);
              if (!o)
                throw new Error(
                  "No ErrorHandler. Is platform module (BrowserModule) included?"
                );
              return (
                s.onDestroy(() => zo(this._modules, s)),
                n.runOutsideAngular(() =>
                  n.onError.subscribe({
                    next: (e) => {
                      o.handleError(e);
                    },
                  })
                ),
                (function (e, t, n) {
                  try {
                    const r = n();
                    return is(r)
                      ? r.catch((n) => {
                          throw (
                            (t.runOutsideAngular(() => e.handleError(n)), n)
                          );
                        })
                      : r;
                  } catch (r) {
                    throw (t.runOutsideAngular(() => e.handleError(r)), r);
                  }
                })(o, n, () => {
                  const e = s.injector.get(Ys);
                  return (
                    e.runInitializers(),
                    e.donePromise.then(
                      () => (
                        Ws(s.injector.get(lo, "en-US") || "en-US"),
                        this._moduleDoBootstrap(s),
                        s
                      )
                    )
                  );
                })
              );
            });
          }
          bootstrapModule(e, t = []) {
            const n = Vo({}, t);
            return Mo(this.injector, n, e).then((e) =>
              this.bootstrapModuleFactory(e, n)
            );
          }
          _moduleDoBootstrap(e) {
            const t = e.injector.get(Lo);
            if (e._bootstrapComponents.length > 0)
              e._bootstrapComponents.forEach((e) => t.bootstrap(e));
            else {
              if (!e.instance.ngDoBootstrap)
                throw new Error(
                  `The module ${de(
                    e.instance.constructor
                  )} was bootstrapped, but it does not declare "@NgModule.bootstrap" components nor a "ngDoBootstrap" method. ` +
                    "Please define one of these."
                );
              e.instance.ngDoBootstrap(t);
            }
            this._modules.push(e);
          }
          onDestroy(e) {
            this._destroyListeners.push(e);
          }
          get injector() {
            return this._injector;
          }
          destroy() {
            if (this._destroyed)
              throw new Error("The platform has already been destroyed!");
            this._modules.slice().forEach((e) => e.destroy()),
              this._destroyListeners.forEach((e) => e()),
              (this._destroyed = !0);
          }
          get destroyed() {
            return this._destroyed;
          }
        }
        return (
          (e.ɵfac = function (t) {
            return new (t || e)(Ve(qr));
          }),
          (e.ɵprov = ne({ token: e, factory: e.ɵfac })),
          e
        );
      })();
      function Vo(e, t) {
        return Array.isArray(t)
          ? t.reduce(Vo, e)
          : Object.assign(Object.assign({}, e), t);
      }
      let Lo = (() => {
        class e {
          constructor(e, t, n, r, s, o) {
            (this._zone = e),
              (this._console = t),
              (this._injector = n),
              (this._exceptionHandler = r),
              (this._componentFactoryResolver = s),
              (this._initStatus = o),
              (this._bootstrapListeners = []),
              (this._views = []),
              (this._runningTick = !1),
              (this._enforceNoNewChanges = !1),
              (this._stable = !0),
              (this.componentTypes = []),
              (this.components = []),
              (this._enforceNoNewChanges = Cn()),
              this._zone.onMicrotaskEmpty.subscribe({
                next: () => {
                  this._zone.run(() => {
                    this.tick();
                  });
                },
              });
            const i = new _((e) => {
                (this._stable =
                  this._zone.isStable &&
                  !this._zone.hasPendingMacrotasks &&
                  !this._zone.hasPendingMicrotasks),
                  this._zone.runOutsideAngular(() => {
                    e.next(this._stable), e.complete();
                  });
              }),
              l = new _((e) => {
                let t;
                this._zone.runOutsideAngular(() => {
                  t = this._zone.onStable.subscribe(() => {
                    wo.assertNotInAngularZone(),
                      vo(() => {
                        this._stable ||
                          this._zone.hasPendingMacrotasks ||
                          this._zone.hasPendingMicrotasks ||
                          ((this._stable = !0), e.next(!0));
                      });
                  });
                });
                const n = this._zone.onUnstable.subscribe(() => {
                  wo.assertInAngularZone(),
                    this._stable &&
                      ((this._stable = !1),
                      this._zone.runOutsideAngular(() => {
                        e.next(!1);
                      }));
                });
                return () => {
                  t.unsubscribe(), n.unsubscribe();
                };
              });
            this.isStable = L(
              i,
              l.pipe((e) => {
                return z()(
                  ((t = W),
                  function (e) {
                    let n;
                    n =
                      "function" == typeof t
                        ? t
                        : function () {
                            return t;
                          };
                    const r = Object.create(e, Z);
                    return (r.source = e), (r.subjectFactory = n), r;
                  })(e)
                );
                var t;
              })
            );
          }
          bootstrap(e, t) {
            if (!this._initStatus.done)
              throw new Error(
                "Cannot bootstrap as there are still asynchronous initializers running. Bootstrap components in the `ngDoBootstrap` method of the root module."
              );
            let n;
            (n =
              e instanceof us
                ? e
                : this._componentFactoryResolver.resolveComponentFactory(e)),
              this.componentTypes.push(n.componentType);
            const r = n.isBoundToModule ? void 0 : this._injector.get(Be),
              s = n.create(qr.NULL, [], t || n.selector, r);
            s.onDestroy(() => {
              this._unloadComponent(s);
            });
            const o = s.injector.get(Ao, null);
            return (
              o &&
                s.injector
                  .get(Oo)
                  .registerApplication(s.location.nativeElement, o),
              this._loadComponent(s),
              Cn() &&
                this._console.log(
                  "Angular is running in the development mode. Call enableProdMode() to enable the production mode."
                ),
              s
            );
          }
          tick() {
            if (this._runningTick)
              throw new Error("ApplicationRef.tick is called recursively");
            try {
              this._runningTick = !0;
              for (let e of this._views) e.detectChanges();
              if (this._enforceNoNewChanges)
                for (let e of this._views) e.checkNoChanges();
            } catch (e) {
              this._zone.runOutsideAngular(() =>
                this._exceptionHandler.handleError(e)
              );
            } finally {
              this._runningTick = !1;
            }
          }
          attachView(e) {
            const t = e;
            this._views.push(t), t.attachToAppRef(this);
          }
          detachView(e) {
            const t = e;
            zo(this._views, t), t.detachFromAppRef();
          }
          _loadComponent(e) {
            this.attachView(e.hostView),
              this.tick(),
              this.components.push(e),
              this._injector
                .get(oo, [])
                .concat(this._bootstrapListeners)
                .forEach((t) => t(e));
          }
          _unloadComponent(e) {
            this.detachView(e.hostView), zo(this.components, e);
          }
          ngOnDestroy() {
            this._views.slice().forEach((e) => e.destroy());
          }
          get viewCount() {
            return this._views.length;
          }
        }
        return (
          (e.ɵfac = function (t) {
            return new (t || e)(Ve(wo), Ve(io), Ve(qr), Ve(bn), Ve(ds), Ve(Ys));
          }),
          (e.ɵprov = ne({ token: e, factory: e.ɵfac })),
          e
        );
      })();
      function zo(e, t) {
        const n = e.indexOf(t);
        n > -1 && e.splice(n, 1);
      }
      const Uo = jo(null, "core", [
          { provide: so, useValue: "unknown" },
          { provide: Fo, deps: [qr] },
          { provide: Oo, deps: [] },
          { provide: io, deps: [] },
        ]),
        Bo = [
          { provide: Lo, useClass: Lo, deps: [wo, io, qr, bn, ds, Ys] },
          {
            provide: Fs,
            deps: [wo],
            useFactory: function (e) {
              let t = [];
              return (
                e.onStable.subscribe(() => {
                  for (; t.length; ) t.pop()();
                }),
                function (e) {
                  t.push(e);
                }
              );
            },
          },
          { provide: Ys, useClass: Ys, deps: [[new G(), Xs]] },
          { provide: go, useClass: go, deps: [] },
          to,
          {
            provide: Os,
            useFactory: function () {
              return Ds;
            },
            deps: [],
          },
          {
            provide: Ps,
            useFactory: function () {
              return Ms;
            },
            deps: [],
          },
          {
            provide: lo,
            useFactory: function (e) {
              return (
                Ws(
                  (e =
                    e ||
                    ("undefined" != typeof $localize && $localize.locale) ||
                    "en-US")
                ),
                e
              );
            },
            deps: [[new Q(lo), new G(), new Y()]],
          },
          { provide: ao, useValue: "USD" },
        ];
      let $o = (() => {
          class e {
            constructor(e) {}
          }
          return (
            (e.ɵmod = et({ type: e })),
            (e.ɵinj = re({
              factory: function (t) {
                return new (t || e)(Ve(Lo));
              },
              providers: Bo,
            })),
            e
          );
        })(),
        Zo = null;
      function qo() {
        return Zo;
      }
      const Wo = new Ae("DocumentToken"),
        Ko = (function () {
          var e = { Zero: 0, One: 1, Two: 2, Few: 3, Many: 4, Other: 5 };
          return (
            (e[e.Zero] = "Zero"),
            (e[e.One] = "One"),
            (e[e.Two] = "Two"),
            (e[e.Few] = "Few"),
            (e[e.Many] = "Many"),
            (e[e.Other] = "Other"),
            e
          );
        })();
      class Jo {}
      let Qo = (() => {
        class e extends Jo {
          constructor(e) {
            super(), (this.locale = e);
          }
          getPluralCategory(e, t) {
            switch (
              (function (e) {
                return (function (e) {
                  const t = (function (e) {
                    return e.toLowerCase().replace(/_/g, "-");
                  })(e);
                  let n = $s(t);
                  if (n) return n;
                  const r = t.split("-")[0];
                  if (((n = $s(r)), n)) return n;
                  if ("en" === r) return Us;
                  throw new Error(`Missing locale data for the locale "${e}".`);
                })(e)[Zs.PluralCase];
              })(t || this.locale)(e)
            ) {
              case Ko.Zero:
                return "zero";
              case Ko.One:
                return "one";
              case Ko.Two:
                return "two";
              case Ko.Few:
                return "few";
              case Ko.Many:
                return "many";
              default:
                return "other";
            }
          }
        }
        return (
          (e.ɵfac = function (t) {
            return new (t || e)(Ve(lo));
          }),
          (e.ɵprov = ne({ token: e, factory: e.ɵfac })),
          e
        );
      })();
      function Go(e, t) {
        t = encodeURIComponent(t);
        for (const n of e.split(";")) {
          const e = n.indexOf("="),
            [r, s] = -1 == e ? [n, ""] : [n.slice(0, e), n.slice(e + 1)];
          if (r.trim() === t) return decodeURIComponent(s);
        }
        return null;
      }
      let Xo = (() => {
        class e {}
        return (
          (e.ɵmod = et({ type: e })),
          (e.ɵinj = re({
            factory: function (t) {
              return new (t || e)();
            },
            providers: [{ provide: Jo, useClass: Qo }],
          })),
          e
        );
      })();
      class Yo extends class extends class {} {
        constructor() {
          super();
        }
        supportsDOMEvents() {
          return !0;
        }
      } {
        static makeCurrent() {
          var e;
          (e = new Yo()), Zo || (Zo = e);
        }
        getProperty(e, t) {
          return e[t];
        }
        log(e) {
          window.console && window.console.log && window.console.log(e);
        }
        logGroup(e) {
          window.console && window.console.group && window.console.group(e);
        }
        logGroupEnd() {
          window.console &&
            window.console.groupEnd &&
            window.console.groupEnd();
        }
        onAndCancel(e, t, n) {
          return (
            e.addEventListener(t, n, !1),
            () => {
              e.removeEventListener(t, n, !1);
            }
          );
        }
        dispatchEvent(e, t) {
          e.dispatchEvent(t);
        }
        remove(e) {
          return e.parentNode && e.parentNode.removeChild(e), e;
        }
        getValue(e) {
          return e.value;
        }
        createElement(e, t) {
          return (t = t || this.getDefaultDocument()).createElement(e);
        }
        createHtmlDocument() {
          return document.implementation.createHTMLDocument("fakeTitle");
        }
        getDefaultDocument() {
          return document;
        }
        isElementNode(e) {
          return e.nodeType === Node.ELEMENT_NODE;
        }
        isShadowRoot(e) {
          return e instanceof DocumentFragment;
        }
        getGlobalEventTarget(e, t) {
          return "window" === t
            ? window
            : "document" === t
            ? e
            : "body" === t
            ? e.body
            : null;
        }
        getHistory() {
          return window.history;
        }
        getLocation() {
          return window.location;
        }
        getBaseHref(e) {
          const t =
            ti || ((ti = document.querySelector("base")), ti)
              ? ti.getAttribute("href")
              : null;
          return null == t
            ? null
            : ((n = t),
              ei || (ei = document.createElement("a")),
              ei.setAttribute("href", n),
              "/" === ei.pathname.charAt(0) ? ei.pathname : "/" + ei.pathname);
          var n;
        }
        resetBaseElement() {
          ti = null;
        }
        getUserAgent() {
          return window.navigator.userAgent;
        }
        performanceNow() {
          return window.performance && window.performance.now
            ? window.performance.now()
            : new Date().getTime();
        }
        supportsCookies() {
          return !0;
        }
        getCookie(e) {
          return Go(document.cookie, e);
        }
      }
      let ei,
        ti = null;
      const ni = new Ae("TRANSITION_ID"),
        ri = [
          {
            provide: Xs,
            useFactory: function (e, t, n) {
              return () => {
                n.get(Ys).donePromise.then(() => {
                  const n = qo();
                  Array.prototype.slice
                    .apply(t.querySelectorAll("style[ng-transition]"))
                    .filter((t) => t.getAttribute("ng-transition") === e)
                    .forEach((e) => n.remove(e));
                });
              };
            },
            deps: [ni, Wo, qr],
            multi: !0,
          },
        ];
      class si {
        static init() {
          var e;
          (e = new si()), (Do = e);
        }
        addToWindow(e) {
          (we.getAngularTestability = (t, n = !0) => {
            const r = e.findTestabilityInTree(t, n);
            if (null == r)
              throw new Error("Could not find testability for element.");
            return r;
          }),
            (we.getAllAngularTestabilities = () => e.getAllTestabilities()),
            (we.getAllAngularRootElements = () => e.getAllRootElements()),
            we.frameworkStabilizers || (we.frameworkStabilizers = []),
            we.frameworkStabilizers.push((e) => {
              const t = we.getAllAngularTestabilities();
              let n = t.length,
                r = !1;
              const s = function (t) {
                (r = r || t), n--, 0 == n && e(r);
              };
              t.forEach(function (e) {
                e.whenStable(s);
              });
            });
        }
        findTestabilityInTree(e, t, n) {
          if (null == t) return null;
          const r = e.getTestability(t);
          return null != r
            ? r
            : n
            ? qo().isShadowRoot(t)
              ? this.findTestabilityInTree(e, t.host, !0)
              : this.findTestabilityInTree(e, t.parentElement, !0)
            : null;
        }
      }
      const oi = new Ae("EventManagerPlugins");
      let ii = (() => {
        class e {
          constructor(e, t) {
            (this._zone = t),
              (this._eventNameToPlugin = new Map()),
              e.forEach((e) => (e.manager = this)),
              (this._plugins = e.slice().reverse());
          }
          addEventListener(e, t, n) {
            return this._findPluginFor(t).addEventListener(e, t, n);
          }
          addGlobalEventListener(e, t, n) {
            return this._findPluginFor(t).addGlobalEventListener(e, t, n);
          }
          getZone() {
            return this._zone;
          }
          _findPluginFor(e) {
            const t = this._eventNameToPlugin.get(e);
            if (t) return t;
            const n = this._plugins;
            for (let r = 0; r < n.length; r++) {
              const t = n[r];
              if (t.supports(e)) return this._eventNameToPlugin.set(e, t), t;
            }
            throw new Error(`No event manager plugin found for event ${e}`);
          }
        }
        return (
          (e.ɵfac = function (t) {
            return new (t || e)(Ve(oi), Ve(wo));
          }),
          (e.ɵprov = ne({ token: e, factory: e.ɵfac })),
          e
        );
      })();
      class li {
        constructor(e) {
          this._doc = e;
        }
        addGlobalEventListener(e, t, n) {
          const r = qo().getGlobalEventTarget(this._doc, e);
          if (!r)
            throw new Error(`Unsupported event target ${r} for event ${t}`);
          return this.addEventListener(r, t, n);
        }
      }
      let ai = (() => {
          class e {
            constructor() {
              this._stylesSet = new Set();
            }
            addStyles(e) {
              const t = new Set();
              e.forEach((e) => {
                this._stylesSet.has(e) || (this._stylesSet.add(e), t.add(e));
              }),
                this.onStylesAdded(t);
            }
            onStylesAdded(e) {}
            getAllStyles() {
              return Array.from(this._stylesSet);
            }
          }
          return (
            (e.ɵfac = function (t) {
              return new (t || e)();
            }),
            (e.ɵprov = ne({ token: e, factory: e.ɵfac })),
            e
          );
        })(),
        ci = (() => {
          class e extends ai {
            constructor(e) {
              super(),
                (this._doc = e),
                (this._hostNodes = new Set()),
                (this._styleNodes = new Set()),
                this._hostNodes.add(e.head);
            }
            _addStylesToHost(e, t) {
              e.forEach((e) => {
                const n = this._doc.createElement("style");
                (n.textContent = e), this._styleNodes.add(t.appendChild(n));
              });
            }
            addHost(e) {
              this._addStylesToHost(this._stylesSet, e), this._hostNodes.add(e);
            }
            removeHost(e) {
              this._hostNodes.delete(e);
            }
            onStylesAdded(e) {
              this._hostNodes.forEach((t) => this._addStylesToHost(e, t));
            }
            ngOnDestroy() {
              this._styleNodes.forEach((e) => qo().remove(e));
            }
          }
          return (
            (e.ɵfac = function (t) {
              return new (t || e)(Ve(Wo));
            }),
            (e.ɵprov = ne({ token: e, factory: e.ɵfac })),
            e
          );
        })();
      const ui = {
          svg: "http://www.w3.org/2000/svg",
          xhtml: "http://www.w3.org/1999/xhtml",
          xlink: "http://www.w3.org/1999/xlink",
          xml: "http://www.w3.org/XML/1998/namespace",
          xmlns: "http://www.w3.org/2000/xmlns/",
        },
        hi = /%COMP%/g;
      function di(e, t, n) {
        for (let r = 0; r < t.length; r++) {
          let s = t[r];
          Array.isArray(s) ? di(e, s, n) : ((s = s.replace(hi, e)), n.push(s));
        }
        return n;
      }
      function pi(e) {
        return (t) => {
          if ("__ngUnwrap__" === t) return e;
          !1 === e(t) && (t.preventDefault(), (t.returnValue = !1));
        };
      }
      let fi = (() => {
        class e {
          constructor(e, t, n) {
            (this.eventManager = e),
              (this.sharedStylesHost = t),
              (this.appId = n),
              (this.rendererByCompId = new Map()),
              (this.defaultRenderer = new mi(e));
          }
          createRenderer(e, t) {
            if (!e || !t) return this.defaultRenderer;
            switch (t.encapsulation) {
              case qe.Emulated: {
                let n = this.rendererByCompId.get(t.id);
                return (
                  n ||
                    ((n = new yi(
                      this.eventManager,
                      this.sharedStylesHost,
                      t,
                      this.appId
                    )),
                    this.rendererByCompId.set(t.id, n)),
                  n.applyToHost(e),
                  n
                );
              }
              case qe.Native:
              case qe.ShadowDom:
                return new gi(this.eventManager, this.sharedStylesHost, e, t);
              default:
                if (!this.rendererByCompId.has(t.id)) {
                  const e = di(t.id, t.styles, []);
                  this.sharedStylesHost.addStyles(e),
                    this.rendererByCompId.set(t.id, this.defaultRenderer);
                }
                return this.defaultRenderer;
            }
          }
          begin() {}
          end() {}
        }
        return (
          (e.ɵfac = function (t) {
            return new (t || e)(Ve(ii), Ve(ci), Ve(eo));
          }),
          (e.ɵprov = ne({ token: e, factory: e.ɵfac })),
          e
        );
      })();
      class mi {
        constructor(e) {
          (this.eventManager = e), (this.data = Object.create(null));
        }
        destroy() {}
        createElement(e, t) {
          return t
            ? document.createElementNS(ui[t] || t, e)
            : document.createElement(e);
        }
        createComment(e) {
          return document.createComment(e);
        }
        createText(e) {
          return document.createTextNode(e);
        }
        appendChild(e, t) {
          e.appendChild(t);
        }
        insertBefore(e, t, n) {
          e && e.insertBefore(t, n);
        }
        removeChild(e, t) {
          e && e.removeChild(t);
        }
        selectRootElement(e, t) {
          let n = "string" == typeof e ? document.querySelector(e) : e;
          if (!n)
            throw new Error(`The selector "${e}" did not match any elements`);
          return t || (n.textContent = ""), n;
        }
        parentNode(e) {
          return e.parentNode;
        }
        nextSibling(e) {
          return e.nextSibling;
        }
        setAttribute(e, t, n, r) {
          if (r) {
            t = r + ":" + t;
            const s = ui[r];
            s ? e.setAttributeNS(s, t, n) : e.setAttribute(t, n);
          } else e.setAttribute(t, n);
        }
        removeAttribute(e, t, n) {
          if (n) {
            const r = ui[n];
            r ? e.removeAttributeNS(r, t) : e.removeAttribute(`${n}:${t}`);
          } else e.removeAttribute(t);
        }
        addClass(e, t) {
          e.classList.add(t);
        }
        removeClass(e, t) {
          e.classList.remove(t);
        }
        setStyle(e, t, n, r) {
          r & ys.DashCase
            ? e.style.setProperty(t, n, r & ys.Important ? "important" : "")
            : (e.style[t] = n);
        }
        removeStyle(e, t, n) {
          n & ys.DashCase ? e.style.removeProperty(t) : (e.style[t] = "");
        }
        setProperty(e, t, n) {
          e[t] = n;
        }
        setValue(e, t) {
          e.nodeValue = t;
        }
        listen(e, t, n) {
          return "string" == typeof e
            ? this.eventManager.addGlobalEventListener(e, t, pi(n))
            : this.eventManager.addEventListener(e, t, pi(n));
        }
      }
      class yi extends mi {
        constructor(e, t, n, r) {
          super(e), (this.component = n);
          const s = di(r + "-" + n.id, n.styles, []);
          t.addStyles(s),
            (this.contentAttr = "_ngcontent-%COMP%".replace(
              hi,
              r + "-" + n.id
            )),
            (this.hostAttr = (function (e) {
              return "_nghost-%COMP%".replace(hi, e);
            })(r + "-" + n.id));
        }
        applyToHost(e) {
          super.setAttribute(e, this.hostAttr, "");
        }
        createElement(e, t) {
          const n = super.createElement(e, t);
          return super.setAttribute(n, this.contentAttr, ""), n;
        }
      }
      class gi extends mi {
        constructor(e, t, n, r) {
          super(e),
            (this.sharedStylesHost = t),
            (this.hostEl = n),
            (this.component = r),
            (this.shadowRoot =
              r.encapsulation === qe.ShadowDom
                ? n.attachShadow({ mode: "open" })
                : n.createShadowRoot()),
            this.sharedStylesHost.addHost(this.shadowRoot);
          const s = di(r.id, r.styles, []);
          for (let o = 0; o < s.length; o++) {
            const e = document.createElement("style");
            (e.textContent = s[o]), this.shadowRoot.appendChild(e);
          }
        }
        nodeOrShadowRoot(e) {
          return e === this.hostEl ? this.shadowRoot : e;
        }
        destroy() {
          this.sharedStylesHost.removeHost(this.shadowRoot);
        }
        appendChild(e, t) {
          return super.appendChild(this.nodeOrShadowRoot(e), t);
        }
        insertBefore(e, t, n) {
          return super.insertBefore(this.nodeOrShadowRoot(e), t, n);
        }
        removeChild(e, t) {
          return super.removeChild(this.nodeOrShadowRoot(e), t);
        }
        parentNode(e) {
          return this.nodeOrShadowRoot(
            super.parentNode(this.nodeOrShadowRoot(e))
          );
        }
      }
      let _i = (() => {
        class e extends li {
          constructor(e) {
            super(e);
          }
          supports(e) {
            return !0;
          }
          addEventListener(e, t, n) {
            return (
              e.addEventListener(t, n, !1),
              () => this.removeEventListener(e, t, n)
            );
          }
          removeEventListener(e, t, n) {
            return e.removeEventListener(t, n);
          }
        }
        return (
          (e.ɵfac = function (t) {
            return new (t || e)(Ve(Wo));
          }),
          (e.ɵprov = ne({ token: e, factory: e.ɵfac })),
          e
        );
      })();
      const bi = ["alt", "control", "meta", "shift"],
        vi = {
          "\b": "Backspace",
          "\t": "Tab",
          "\x7f": "Delete",
          "\x1b": "Escape",
          Del: "Delete",
          Esc: "Escape",
          Left: "ArrowLeft",
          Right: "ArrowRight",
          Up: "ArrowUp",
          Down: "ArrowDown",
          Menu: "ContextMenu",
          Scroll: "ScrollLock",
          Win: "OS",
        },
        wi = {
          A: "1",
          B: "2",
          C: "3",
          D: "4",
          E: "5",
          F: "6",
          G: "7",
          H: "8",
          I: "9",
          J: "*",
          K: "+",
          M: "-",
          N: ".",
          O: "/",
          "`": "0",
          "\x90": "NumLock",
        },
        Ci = {
          alt: (e) => e.altKey,
          control: (e) => e.ctrlKey,
          meta: (e) => e.metaKey,
          shift: (e) => e.shiftKey,
        };
      let xi = (() => {
        class e extends li {
          constructor(e) {
            super(e);
          }
          supports(t) {
            return null != e.parseEventName(t);
          }
          addEventListener(t, n, r) {
            const s = e.parseEventName(n),
              o = e.eventCallback(s.fullKey, r, this.manager.getZone());
            return this.manager
              .getZone()
              .runOutsideAngular(() => qo().onAndCancel(t, s.domEventName, o));
          }
          static parseEventName(t) {
            const n = t.toLowerCase().split("."),
              r = n.shift();
            if (0 === n.length || ("keydown" !== r && "keyup" !== r))
              return null;
            const s = e._normalizeKey(n.pop());
            let o = "";
            if (
              (bi.forEach((e) => {
                const t = n.indexOf(e);
                t > -1 && (n.splice(t, 1), (o += e + "."));
              }),
              (o += s),
              0 != n.length || 0 === s.length)
            )
              return null;
            const i = {};
            return (i.domEventName = r), (i.fullKey = o), i;
          }
          static getEventFullKey(e) {
            let t = "",
              n = (function (e) {
                let t = e.key;
                if (null == t) {
                  if (((t = e.keyIdentifier), null == t)) return "Unidentified";
                  t.startsWith("U+") &&
                    ((t = String.fromCharCode(parseInt(t.substring(2), 16))),
                    3 === e.location && wi.hasOwnProperty(t) && (t = wi[t]));
                }
                return vi[t] || t;
              })(e);
            return (
              (n = n.toLowerCase()),
              " " === n ? (n = "space") : "." === n && (n = "dot"),
              bi.forEach((r) => {
                r != n && (0, Ci[r])(e) && (t += r + ".");
              }),
              (t += n),
              t
            );
          }
          static eventCallback(t, n, r) {
            return (s) => {
              e.getEventFullKey(s) === t && r.runGuarded(() => n(s));
            };
          }
          static _normalizeKey(e) {
            switch (e) {
              case "esc":
                return "escape";
              default:
                return e;
            }
          }
        }
        return (
          (e.ɵfac = function (t) {
            return new (t || e)(Ve(Wo));
          }),
          (e.ɵprov = ne({ token: e, factory: e.ɵfac })),
          e
        );
      })();
      const Ei = jo(Uo, "browser", [
          { provide: so, useValue: "browser" },
          {
            provide: ro,
            useValue: function () {
              Yo.makeCurrent(), si.init();
            },
            multi: !0,
          },
          {
            provide: Wo,
            useFactory: function () {
              return (
                (function (e) {
                  ct = e;
                })(document),
                document
              );
            },
            deps: [],
          },
        ]),
        ki = [
          [],
          { provide: Dr, useValue: "root" },
          {
            provide: bn,
            useFactory: function () {
              return new bn();
            },
            deps: [],
          },
          { provide: oi, useClass: _i, multi: !0, deps: [Wo, wo, so] },
          { provide: oi, useClass: xi, multi: !0, deps: [Wo] },
          [],
          { provide: fi, useClass: fi, deps: [ii, ci, eo] },
          { provide: ms, useExisting: fi },
          { provide: ai, useExisting: ci },
          { provide: ci, useClass: ci, deps: [Wo] },
          { provide: Ao, useClass: Ao, deps: [wo] },
          { provide: ii, useClass: ii, deps: [oi, wo] },
          [],
        ];
      let Ti = (() => {
        class e {
          constructor(e) {
            if (e)
              throw new Error(
                "BrowserModule has already been loaded. If you need access to common directives such as NgIf and NgFor from a lazy loaded module, import CommonModule instead."
              );
          }
          static withServerTransition(t) {
            return {
              ngModule: e,
              providers: [
                { provide: eo, useValue: t.appId },
                { provide: ni, useExisting: eo },
                ri,
              ],
            };
          }
        }
        return (
          (e.ɵmod = et({ type: e })),
          (e.ɵinj = re({
            factory: function (t) {
              return new (t || e)(Ve(e, 12));
            },
            providers: ki,
            imports: [Xo, $o],
          })),
          e
        );
      })();
      "undefined" != typeof window && window;
      let Si = (() => {
        class e {
          constructor() {
            this.title = "common-web-element";
          }
        }
        return (
          (e.ɵfac = function (t) {
            return new (t || e)();
          }),
          (e.ɵcmp = Qe({
            type: e,
            selectors: [["app-root"]],
            decls: 3,
            vars: 0,
            template: function (e, t) {
              1 & e &&
                (os(0, "ele-navbar"), os(1, "ele-header"), os(2, "ele-footer"));
            },
            styles: ["[_nghost-%COMP%]{background-color:green}"],
          })),
          e
        );
      })();
      class Ii {
        constructor(e, t) {
          (this.predicate = e), (this.thisArg = t);
        }
        call(e, t) {
          return t.subscribe(new Ai(e, this.predicate, this.thisArg));
        }
      }
      class Ai extends f {
        constructor(e, t, n) {
          super(e), (this.predicate = t), (this.thisArg = n), (this.count = 0);
        }
        _next(e) {
          let t;
          try {
            t = this.predicate.call(this.thisArg, e, this.count++);
          } catch (n) {
            return void this.destination.error(n);
          }
          t && this.destination.next(e);
        }
      }
      class Oi {}
      class Pi {}
      class Ni {
        constructor(e) {
          (this.normalizedNames = new Map()),
            (this.lazyUpdate = null),
            e
              ? (this.lazyInit =
                  "string" == typeof e
                    ? () => {
                        (this.headers = new Map()),
                          e.split("\n").forEach((e) => {
                            const t = e.indexOf(":");
                            if (t > 0) {
                              const n = e.slice(0, t),
                                r = n.toLowerCase(),
                                s = e.slice(t + 1).trim();
                              this.maybeSetNormalizedName(n, r),
                                this.headers.has(r)
                                  ? this.headers.get(r).push(s)
                                  : this.headers.set(r, [s]);
                            }
                          });
                      }
                    : () => {
                        (this.headers = new Map()),
                          Object.keys(e).forEach((t) => {
                            let n = e[t];
                            const r = t.toLowerCase();
                            "string" == typeof n && (n = [n]),
                              n.length > 0 &&
                                (this.headers.set(r, n),
                                this.maybeSetNormalizedName(t, r));
                          });
                      })
              : (this.headers = new Map());
        }
        has(e) {
          return this.init(), this.headers.has(e.toLowerCase());
        }
        get(e) {
          this.init();
          const t = this.headers.get(e.toLowerCase());
          return t && t.length > 0 ? t[0] : null;
        }
        keys() {
          return this.init(), Array.from(this.normalizedNames.values());
        }
        getAll(e) {
          return this.init(), this.headers.get(e.toLowerCase()) || null;
        }
        append(e, t) {
          return this.clone({ name: e, value: t, op: "a" });
        }
        set(e, t) {
          return this.clone({ name: e, value: t, op: "s" });
        }
        delete(e, t) {
          return this.clone({ name: e, value: t, op: "d" });
        }
        maybeSetNormalizedName(e, t) {
          this.normalizedNames.has(t) || this.normalizedNames.set(t, e);
        }
        init() {
          this.lazyInit &&
            (this.lazyInit instanceof Ni
              ? this.copyFrom(this.lazyInit)
              : this.lazyInit(),
            (this.lazyInit = null),
            this.lazyUpdate &&
              (this.lazyUpdate.forEach((e) => this.applyUpdate(e)),
              (this.lazyUpdate = null)));
        }
        copyFrom(e) {
          e.init(),
            Array.from(e.headers.keys()).forEach((t) => {
              this.headers.set(t, e.headers.get(t)),
                this.normalizedNames.set(t, e.normalizedNames.get(t));
            });
        }
        clone(e) {
          const t = new Ni();
          return (
            (t.lazyInit =
              this.lazyInit && this.lazyInit instanceof Ni
                ? this.lazyInit
                : this),
            (t.lazyUpdate = (this.lazyUpdate || []).concat([e])),
            t
          );
        }
        applyUpdate(e) {
          const t = e.name.toLowerCase();
          switch (e.op) {
            case "a":
            case "s":
              let n = e.value;
              if (("string" == typeof n && (n = [n]), 0 === n.length)) return;
              this.maybeSetNormalizedName(e.name, t);
              const r = ("a" === e.op ? this.headers.get(t) : void 0) || [];
              r.push(...n), this.headers.set(t, r);
              break;
            case "d":
              const s = e.value;
              if (s) {
                let e = this.headers.get(t);
                if (!e) return;
                (e = e.filter((e) => -1 === s.indexOf(e))),
                  0 === e.length
                    ? (this.headers.delete(t), this.normalizedNames.delete(t))
                    : this.headers.set(t, e);
              } else this.headers.delete(t), this.normalizedNames.delete(t);
          }
        }
        forEach(e) {
          this.init(),
            Array.from(this.normalizedNames.keys()).forEach((t) =>
              e(this.normalizedNames.get(t), this.headers.get(t))
            );
        }
      }
      class Di {
        encodeKey(e) {
          return Mi(e);
        }
        encodeValue(e) {
          return Mi(e);
        }
        decodeKey(e) {
          return decodeURIComponent(e);
        }
        decodeValue(e) {
          return decodeURIComponent(e);
        }
      }
      function Mi(e) {
        return encodeURIComponent(e)
          .replace(/%40/gi, "@")
          .replace(/%3A/gi, ":")
          .replace(/%24/gi, "$")
          .replace(/%2C/gi, ",")
          .replace(/%3B/gi, ";")
          .replace(/%2B/gi, "+")
          .replace(/%3D/gi, "=")
          .replace(/%3F/gi, "?")
          .replace(/%2F/gi, "/");
      }
      class Ri {
        constructor(e = {}) {
          if (
            ((this.updates = null),
            (this.cloneFrom = null),
            (this.encoder = e.encoder || new Di()),
            e.fromString)
          ) {
            if (e.fromObject)
              throw new Error("Cannot specify both fromString and fromObject.");
            this.map = (function (e, t) {
              const n = new Map();
              return (
                e.length > 0 &&
                  e.split("&").forEach((e) => {
                    const r = e.indexOf("="),
                      [s, o] =
                        -1 == r
                          ? [t.decodeKey(e), ""]
                          : [
                              t.decodeKey(e.slice(0, r)),
                              t.decodeValue(e.slice(r + 1)),
                            ],
                      i = n.get(s) || [];
                    i.push(o), n.set(s, i);
                  }),
                n
              );
            })(e.fromString, this.encoder);
          } else
            e.fromObject
              ? ((this.map = new Map()),
                Object.keys(e.fromObject).forEach((t) => {
                  const n = e.fromObject[t];
                  this.map.set(t, Array.isArray(n) ? n : [n]);
                }))
              : (this.map = null);
        }
        has(e) {
          return this.init(), this.map.has(e);
        }
        get(e) {
          this.init();
          const t = this.map.get(e);
          return t ? t[0] : null;
        }
        getAll(e) {
          return this.init(), this.map.get(e) || null;
        }
        keys() {
          return this.init(), Array.from(this.map.keys());
        }
        append(e, t) {
          return this.clone({ param: e, value: t, op: "a" });
        }
        set(e, t) {
          return this.clone({ param: e, value: t, op: "s" });
        }
        delete(e, t) {
          return this.clone({ param: e, value: t, op: "d" });
        }
        toString() {
          return (
            this.init(),
            this.keys()
              .map((e) => {
                const t = this.encoder.encodeKey(e);
                return this.map
                  .get(e)
                  .map((e) => t + "=" + this.encoder.encodeValue(e))
                  .join("&");
              })
              .filter((e) => "" !== e)
              .join("&")
          );
        }
        clone(e) {
          const t = new Ri({ encoder: this.encoder });
          return (
            (t.cloneFrom = this.cloneFrom || this),
            (t.updates = (this.updates || []).concat([e])),
            t
          );
        }
        init() {
          null === this.map && (this.map = new Map()),
            null !== this.cloneFrom &&
              (this.cloneFrom.init(),
              this.cloneFrom
                .keys()
                .forEach((e) => this.map.set(e, this.cloneFrom.map.get(e))),
              this.updates.forEach((e) => {
                switch (e.op) {
                  case "a":
                  case "s":
                    const t =
                      ("a" === e.op ? this.map.get(e.param) : void 0) || [];
                    t.push(e.value), this.map.set(e.param, t);
                    break;
                  case "d":
                    if (void 0 === e.value) {
                      this.map.delete(e.param);
                      break;
                    }
                    {
                      let t = this.map.get(e.param) || [];
                      const n = t.indexOf(e.value);
                      -1 !== n && t.splice(n, 1),
                        t.length > 0
                          ? this.map.set(e.param, t)
                          : this.map.delete(e.param);
                    }
                }
              }),
              (this.cloneFrom = this.updates = null));
        }
      }
      function ji(e) {
        return "undefined" != typeof ArrayBuffer && e instanceof ArrayBuffer;
      }
      function Hi(e) {
        return "undefined" != typeof Blob && e instanceof Blob;
      }
      function Fi(e) {
        return "undefined" != typeof FormData && e instanceof FormData;
      }
      class Vi {
        constructor(e, t, n, r) {
          let s;
          if (
            ((this.url = t),
            (this.body = null),
            (this.reportProgress = !1),
            (this.withCredentials = !1),
            (this.responseType = "json"),
            (this.method = e.toUpperCase()),
            (function (e) {
              switch (e) {
                case "DELETE":
                case "GET":
                case "HEAD":
                case "OPTIONS":
                case "JSONP":
                  return !1;
                default:
                  return !0;
              }
            })(this.method) || r
              ? ((this.body = void 0 !== n ? n : null), (s = r))
              : (s = n),
            s &&
              ((this.reportProgress = !!s.reportProgress),
              (this.withCredentials = !!s.withCredentials),
              s.responseType && (this.responseType = s.responseType),
              s.headers && (this.headers = s.headers),
              s.params && (this.params = s.params)),
            this.headers || (this.headers = new Ni()),
            this.params)
          ) {
            const e = this.params.toString();
            if (0 === e.length) this.urlWithParams = t;
            else {
              const n = t.indexOf("?");
              this.urlWithParams =
                t + (-1 === n ? "?" : n < t.length - 1 ? "&" : "") + e;
            }
          } else (this.params = new Ri()), (this.urlWithParams = t);
        }
        serializeBody() {
          return null === this.body
            ? null
            : ji(this.body) ||
              Hi(this.body) ||
              Fi(this.body) ||
              "string" == typeof this.body
            ? this.body
            : this.body instanceof Ri
            ? this.body.toString()
            : "object" == typeof this.body ||
              "boolean" == typeof this.body ||
              Array.isArray(this.body)
            ? JSON.stringify(this.body)
            : this.body.toString();
        }
        detectContentTypeHeader() {
          return null === this.body
            ? null
            : Fi(this.body)
            ? null
            : Hi(this.body)
            ? this.body.type || null
            : ji(this.body)
            ? null
            : "string" == typeof this.body
            ? "text/plain"
            : this.body instanceof Ri
            ? "application/x-www-form-urlencoded;charset=UTF-8"
            : "object" == typeof this.body ||
              "number" == typeof this.body ||
              Array.isArray(this.body)
            ? "application/json"
            : null;
        }
        clone(e = {}) {
          const t = e.method || this.method,
            n = e.url || this.url,
            r = e.responseType || this.responseType,
            s = void 0 !== e.body ? e.body : this.body,
            o =
              void 0 !== e.withCredentials
                ? e.withCredentials
                : this.withCredentials,
            i =
              void 0 !== e.reportProgress
                ? e.reportProgress
                : this.reportProgress;
          let l = e.headers || this.headers,
            a = e.params || this.params;
          return (
            void 0 !== e.setHeaders &&
              (l = Object.keys(e.setHeaders).reduce(
                (t, n) => t.set(n, e.setHeaders[n]),
                l
              )),
            e.setParams &&
              (a = Object.keys(e.setParams).reduce(
                (t, n) => t.set(n, e.setParams[n]),
                a
              )),
            new Vi(t, n, s, {
              params: a,
              headers: l,
              reportProgress: i,
              responseType: r,
              withCredentials: o,
            })
          );
        }
      }
      const Li = (function () {
        var e = {
          Sent: 0,
          UploadProgress: 1,
          ResponseHeader: 2,
          DownloadProgress: 3,
          Response: 4,
          User: 5,
        };
        return (
          (e[e.Sent] = "Sent"),
          (e[e.UploadProgress] = "UploadProgress"),
          (e[e.ResponseHeader] = "ResponseHeader"),
          (e[e.DownloadProgress] = "DownloadProgress"),
          (e[e.Response] = "Response"),
          (e[e.User] = "User"),
          e
        );
      })();
      class zi {
        constructor(e, t = 200, n = "OK") {
          (this.headers = e.headers || new Ni()),
            (this.status = void 0 !== e.status ? e.status : t),
            (this.statusText = e.statusText || n),
            (this.url = e.url || null),
            (this.ok = this.status >= 200 && this.status < 300);
        }
      }
      class Ui extends zi {
        constructor(e = {}) {
          super(e), (this.type = Li.ResponseHeader);
        }
        clone(e = {}) {
          return new Ui({
            headers: e.headers || this.headers,
            status: void 0 !== e.status ? e.status : this.status,
            statusText: e.statusText || this.statusText,
            url: e.url || this.url || void 0,
          });
        }
      }
      class Bi extends zi {
        constructor(e = {}) {
          super(e),
            (this.type = Li.Response),
            (this.body = void 0 !== e.body ? e.body : null);
        }
        clone(e = {}) {
          return new Bi({
            body: void 0 !== e.body ? e.body : this.body,
            headers: e.headers || this.headers,
            status: void 0 !== e.status ? e.status : this.status,
            statusText: e.statusText || this.statusText,
            url: e.url || this.url || void 0,
          });
        }
      }
      class $i extends zi {
        constructor(e) {
          super(e, 0, "Unknown Error"),
            (this.name = "HttpErrorResponse"),
            (this.ok = !1),
            (this.message =
              this.status >= 200 && this.status < 300
                ? `Http failure during parsing for ${e.url || "(unknown url)"}`
                : `Http failure response for ${e.url || "(unknown url)"}: ${
                    e.status
                  } ${e.statusText}`),
            (this.error = e.error || null);
        }
      }
      function Zi(e, t) {
        return {
          body: t,
          headers: e.headers,
          observe: e.observe,
          params: e.params,
          reportProgress: e.reportProgress,
          responseType: e.responseType,
          withCredentials: e.withCredentials,
        };
      }
      let qi = (() => {
        class e {
          constructor(e) {
            this.handler = e;
          }
          request(e, t, n = {}) {
            let r;
            if (e instanceof Vi) r = e;
            else {
              let s = void 0;
              s = n.headers instanceof Ni ? n.headers : new Ni(n.headers);
              let o = void 0;
              n.params &&
                (o =
                  n.params instanceof Ri
                    ? n.params
                    : new Ri({ fromObject: n.params })),
                (r = new Vi(e, t, void 0 !== n.body ? n.body : null, {
                  headers: s,
                  params: o,
                  reportProgress: n.reportProgress,
                  responseType: n.responseType || "json",
                  withCredentials: n.withCredentials,
                }));
            }
            const s = (function (...e) {
              let t = e[e.length - 1];
              return k(t) ? (e.pop(), R(e, t)) : V(e);
            })(r).pipe(j((e) => this.handler.handle(e), void 0, 1));
            if (e instanceof Vi || "events" === n.observe) return s;
            const o = s.pipe(
              ((i = (e) => e instanceof Bi),
              function (e) {
                return e.lift(new Ii(i, void 0));
              })
            );
            var i;
            switch (n.observe || "body") {
              case "body":
                switch (r.responseType) {
                  case "arraybuffer":
                    return o.pipe(
                      N((e) => {
                        if (null !== e.body && !(e.body instanceof ArrayBuffer))
                          throw new Error("Response is not an ArrayBuffer.");
                        return e.body;
                      })
                    );
                  case "blob":
                    return o.pipe(
                      N((e) => {
                        if (null !== e.body && !(e.body instanceof Blob))
                          throw new Error("Response is not a Blob.");
                        return e.body;
                      })
                    );
                  case "text":
                    return o.pipe(
                      N((e) => {
                        if (null !== e.body && "string" != typeof e.body)
                          throw new Error("Response is not a string.");
                        return e.body;
                      })
                    );
                  case "json":
                  default:
                    return o.pipe(N((e) => e.body));
                }
              case "response":
                return o;
              default:
                throw new Error(
                  `Unreachable: unhandled observe type ${n.observe}}`
                );
            }
          }
          delete(e, t = {}) {
            return this.request("DELETE", e, t);
          }
          get(e, t = {}) {
            return this.request("GET", e, t);
          }
          head(e, t = {}) {
            return this.request("HEAD", e, t);
          }
          jsonp(e, t) {
            return this.request("JSONP", e, {
              params: new Ri().append(t, "JSONP_CALLBACK"),
              observe: "body",
              responseType: "json",
            });
          }
          options(e, t = {}) {
            return this.request("OPTIONS", e, t);
          }
          patch(e, t, n = {}) {
            return this.request("PATCH", e, Zi(n, t));
          }
          post(e, t, n = {}) {
            return this.request("POST", e, Zi(n, t));
          }
          put(e, t, n = {}) {
            return this.request("PUT", e, Zi(n, t));
          }
        }
        return (
          (e.ɵfac = function (t) {
            return new (t || e)(Ve(Oi));
          }),
          (e.ɵprov = ne({ token: e, factory: e.ɵfac })),
          e
        );
      })();
      class Wi {
        constructor(e, t) {
          (this.next = e), (this.interceptor = t);
        }
        handle(e) {
          return this.interceptor.intercept(e, this.next);
        }
      }
      const Ki = new Ae("HTTP_INTERCEPTORS");
      let Ji = (() => {
        class e {
          intercept(e, t) {
            return t.handle(e);
          }
        }
        return (
          (e.ɵfac = function (t) {
            return new (t || e)();
          }),
          (e.ɵprov = ne({ token: e, factory: e.ɵfac })),
          e
        );
      })();
      const Qi = /^\)\]\}',?\n/;
      class Gi {}
      let Xi = (() => {
          class e {
            constructor() {}
            build() {
              return new XMLHttpRequest();
            }
          }
          return (
            (e.ɵfac = function (t) {
              return new (t || e)();
            }),
            (e.ɵprov = ne({ token: e, factory: e.ɵfac })),
            e
          );
        })(),
        Yi = (() => {
          class e {
            constructor(e) {
              this.xhrFactory = e;
            }
            handle(e) {
              if ("JSONP" === e.method)
                throw new Error(
                  "Attempted to construct Jsonp request without JsonpClientModule installed."
                );
              return new _((t) => {
                const n = this.xhrFactory.build();
                if (
                  (n.open(e.method, e.urlWithParams),
                  e.withCredentials && (n.withCredentials = !0),
                  e.headers.forEach((e, t) =>
                    n.setRequestHeader(e, t.join(","))
                  ),
                  e.headers.has("Accept") ||
                    n.setRequestHeader(
                      "Accept",
                      "application/json, text/plain, */*"
                    ),
                  !e.headers.has("Content-Type"))
                ) {
                  const t = e.detectContentTypeHeader();
                  null !== t && n.setRequestHeader("Content-Type", t);
                }
                if (e.responseType) {
                  const t = e.responseType.toLowerCase();
                  n.responseType = "json" !== t ? t : "text";
                }
                const r = e.serializeBody();
                let s = null;
                const o = () => {
                    if (null !== s) return s;
                    const t = 1223 === n.status ? 204 : n.status,
                      r = n.statusText || "OK",
                      o = new Ni(n.getAllResponseHeaders()),
                      i =
                        (function (e) {
                          return "responseURL" in e && e.responseURL
                            ? e.responseURL
                            : /^X-Request-URL:/m.test(e.getAllResponseHeaders())
                            ? e.getResponseHeader("X-Request-URL")
                            : null;
                        })(n) || e.url;
                    return (
                      (s = new Ui({
                        headers: o,
                        status: t,
                        statusText: r,
                        url: i,
                      })),
                      s
                    );
                  },
                  i = () => {
                    let { headers: r, status: s, statusText: i, url: l } = o(),
                      a = null;
                    204 !== s &&
                      (a = void 0 === n.response ? n.responseText : n.response),
                      0 === s && (s = a ? 200 : 0);
                    let c = s >= 200 && s < 300;
                    if ("json" === e.responseType && "string" == typeof a) {
                      const e = a;
                      a = a.replace(Qi, "");
                      try {
                        a = "" !== a ? JSON.parse(a) : null;
                      } catch (u) {
                        (a = e), c && ((c = !1), (a = { error: u, text: a }));
                      }
                    }
                    c
                      ? (t.next(
                          new Bi({
                            body: a,
                            headers: r,
                            status: s,
                            statusText: i,
                            url: l || void 0,
                          })
                        ),
                        t.complete())
                      : t.error(
                          new $i({
                            error: a,
                            headers: r,
                            status: s,
                            statusText: i,
                            url: l || void 0,
                          })
                        );
                  },
                  l = (e) => {
                    const { url: r } = o(),
                      s = new $i({
                        error: e,
                        status: n.status || 0,
                        statusText: n.statusText || "Unknown Error",
                        url: r || void 0,
                      });
                    t.error(s);
                  };
                let a = !1;
                const c = (r) => {
                    a || (t.next(o()), (a = !0));
                    let s = { type: Li.DownloadProgress, loaded: r.loaded };
                    r.lengthComputable && (s.total = r.total),
                      "text" === e.responseType &&
                        n.responseText &&
                        (s.partialText = n.responseText),
                      t.next(s);
                  },
                  u = (e) => {
                    let n = { type: Li.UploadProgress, loaded: e.loaded };
                    e.lengthComputable && (n.total = e.total), t.next(n);
                  };
                return (
                  n.addEventListener("load", i),
                  n.addEventListener("error", l),
                  e.reportProgress &&
                    (n.addEventListener("progress", c),
                    null !== r &&
                      n.upload &&
                      n.upload.addEventListener("progress", u)),
                  n.send(r),
                  t.next({ type: Li.Sent }),
                  () => {
                    n.removeEventListener("error", l),
                      n.removeEventListener("load", i),
                      e.reportProgress &&
                        (n.removeEventListener("progress", c),
                        null !== r &&
                          n.upload &&
                          n.upload.removeEventListener("progress", u)),
                      n.abort();
                  }
                );
              });
            }
          }
          return (
            (e.ɵfac = function (t) {
              return new (t || e)(Ve(Gi));
            }),
            (e.ɵprov = ne({ token: e, factory: e.ɵfac })),
            e
          );
        })();
      const el = new Ae("XSRF_COOKIE_NAME"),
        tl = new Ae("XSRF_HEADER_NAME");
      class nl {}
      let rl = (() => {
          class e {
            constructor(e, t, n) {
              (this.doc = e),
                (this.platform = t),
                (this.cookieName = n),
                (this.lastCookieString = ""),
                (this.lastToken = null),
                (this.parseCount = 0);
            }
            getToken() {
              if ("server" === this.platform) return null;
              const e = this.doc.cookie || "";
              return (
                e !== this.lastCookieString &&
                  (this.parseCount++,
                  (this.lastToken = Go(e, this.cookieName)),
                  (this.lastCookieString = e)),
                this.lastToken
              );
            }
          }
          return (
            (e.ɵfac = function (t) {
              return new (t || e)(Ve(Wo), Ve(so), Ve(el));
            }),
            (e.ɵprov = ne({ token: e, factory: e.ɵfac })),
            e
          );
        })(),
        sl = (() => {
          class e {
            constructor(e, t) {
              (this.tokenService = e), (this.headerName = t);
            }
            intercept(e, t) {
              const n = e.url.toLowerCase();
              if (
                "GET" === e.method ||
                "HEAD" === e.method ||
                n.startsWith("http://") ||
                n.startsWith("https://")
              )
                return t.handle(e);
              const r = this.tokenService.getToken();
              return (
                null === r ||
                  e.headers.has(this.headerName) ||
                  (e = e.clone({ headers: e.headers.set(this.headerName, r) })),
                t.handle(e)
              );
            }
          }
          return (
            (e.ɵfac = function (t) {
              return new (t || e)(Ve(nl), Ve(tl));
            }),
            (e.ɵprov = ne({ token: e, factory: e.ɵfac })),
            e
          );
        })(),
        ol = (() => {
          class e {
            constructor(e, t) {
              (this.backend = e), (this.injector = t), (this.chain = null);
            }
            handle(e) {
              if (null === this.chain) {
                const e = this.injector.get(Ki, []);
                this.chain = e.reduceRight(
                  (e, t) => new Wi(e, t),
                  this.backend
                );
              }
              return this.chain.handle(e);
            }
          }
          return (
            (e.ɵfac = function (t) {
              return new (t || e)(Ve(Pi), Ve(qr));
            }),
            (e.ɵprov = ne({ token: e, factory: e.ɵfac })),
            e
          );
        })(),
        il = (() => {
          class e {
            static disable() {
              return {
                ngModule: e,
                providers: [{ provide: sl, useClass: Ji }],
              };
            }
            static withOptions(t = {}) {
              return {
                ngModule: e,
                providers: [
                  t.cookieName ? { provide: el, useValue: t.cookieName } : [],
                  t.headerName ? { provide: tl, useValue: t.headerName } : [],
                ],
              };
            }
          }
          return (
            (e.ɵmod = et({ type: e })),
            (e.ɵinj = re({
              factory: function (t) {
                return new (t || e)();
              },
              providers: [
                sl,
                { provide: Ki, useExisting: sl, multi: !0 },
                { provide: nl, useClass: rl },
                { provide: el, useValue: "XSRF-TOKEN" },
                { provide: tl, useValue: "X-XSRF-TOKEN" },
              ],
            })),
            e
          );
        })(),
        ll = (() => {
          class e {}
          return (
            (e.ɵmod = et({ type: e })),
            (e.ɵinj = re({
              factory: function (t) {
                return new (t || e)();
              },
              providers: [
                qi,
                { provide: Oi, useClass: ol },
                Yi,
                { provide: Pi, useExisting: Yi },
                Xi,
                { provide: Gi, useExisting: Xi },
              ],
              imports: [
                [
                  il.withOptions({
                    cookieName: "XSRF-TOKEN",
                    headerName: "X-XSRF-TOKEN",
                  }),
                ],
              ],
            })),
            e
          );
        })(),
        al = (() => {
          class e {}
          return (
            (e.ɵmod = et({ type: e })),
            (e.ɵinj = re({
              factory: function (t) {
                return new (t || e)();
              },
              providers: [],
              imports: [[Xo, ll]],
            })),
            e
          );
        })(),
        cl = (() => {
          class e {
            constructor() {}
            ngOnInit() {}
          }
          return (
            (e.ɵfac = function (t) {
              return new (t || e)();
            }),
            (e.ɵcmp = Qe({
              type: e,
              selectors: [["lib-button"]],
              decls: 2,
              vars: 0,
              consts: [[1, "button1"]],
              template: function (e, t) {
                1 & e && (rs(0, "button", 0), ls(1, "Button Works"), ss());
              },
              styles: [
                ".button1[_ngcontent-%COMP%]{background-color:#4caf50;border:none;border-radius:12px;color:#fff;padding:7px 16px;text-align:center;text-decoration:none;display:inline-block;font-size:16px;margin:4px 2px;cursor:pointer;transition-duration:.4s;box-shadow:0 8px 16px 0 rgba(0,0,0,.2),0 6px 20px 0 rgba(0,0,0,.19)}",
              ],
            })),
            e
          );
        })(),
        ul = (() => {
          class e {}
          return (
            (e.ɵmod = et({ type: e })),
            (e.ɵinj = re({
              factory: function (t) {
                return new (t || e)();
              },
              providers: [],
              imports: [[Xo, ll]],
            })),
            e
          );
        })(),
        hl = (() => {
          class e {
            constructor() {}
            ngOnInit() {}
          }
          return (
            (e.ɵfac = function (t) {
              return new (t || e)();
            }),
            (e.ɵcmp = Qe({
              type: e,
              selectors: [["lib-hello"]],
              decls: 2,
              vars: 0,
              template: function (e, t) {
                1 & e &&
                  (rs(0, "p"), ls(1, "This is from hello component"), ss());
              },
              styles: [""],
            })),
            e
          );
        })(),
        dl = (() => {
          class e {}
          return (
            (e.ɵmod = et({ type: e })),
            (e.ɵinj = re({
              factory: function (t) {
                return new (t || e)();
              },
              providers: [],
              imports: [[Xo, ll]],
            })),
            e
          );
        })(),
        pl = (() => {
          class e {
            constructor() {}
            ngOnInit() {}
          }
          return (
            (e.ɵfac = function (t) {
              return new (t || e)();
            }),
            (e.ɵcmp = Qe({
              type: e,
              selectors: [["lib-footer"]],
              decls: 7,
              vars: 0,
              consts: [
                [1, "footer"],
                ["id", "button"],
                ["id", "container"],
                ["id", "cont"],
                [1, "footer_center"],
              ],
              template: function (e, t) {
                1 & e &&
                  (rs(0, "div", 0),
                  os(1, "div", 1),
                  rs(2, "div", 2),
                  rs(3, "div", 3),
                  rs(4, "div", 4),
                  rs(5, "h4"),
                  ls(6, "This is actually a footer"),
                  ss(),
                  ss(),
                  ss(),
                  ss(),
                  ss());
              },
              styles: [
                "body[_ngcontent-%COMP%]{font-family:Source Sans Pro,sans-serif;font-weight:300;background-color:#add8e6;color:#fff}section[_ngcontent-%COMP%]{text-align:center}.footer[_ngcontent-%COMP%]   #button[_ngcontent-%COMP%]{border:12px solid #727172;border-radius:50px;margin:0 auto}.footer[_ngcontent-%COMP%]   #button[_ngcontent-%COMP%], .footer[_ngcontent-%COMP%]   #button[_ngcontent-%COMP%]:hover{width:55px;height:55px;position:relative;transition:all 1s ease}.footer[_ngcontent-%COMP%]   #button[_ngcontent-%COMP%]:hover{border:12px solid #3a3a3a}.footer[_ngcontent-%COMP%]{color:#fff;bottom:0;left:0;position:fixed;width:100%;height:2em;overflow:hidden;margin:0 auto;z-index:999}.footer[_ngcontent-%COMP%], .footer[_ngcontent-%COMP%]:hover{transition:all 1s ease}.footer[_ngcontent-%COMP%]:hover{height:10em}.footer[_ngcontent-%COMP%]   #container[_ngcontent-%COMP%]{margin-top:5px;width:100%;height:100%;position:relative;top:0;left:0;background:#3a3a3a}.footer[_ngcontent-%COMP%]   #cont[_ngcontent-%COMP%]{position:relative;top:-45px;right:190px;width:150px;height:auto;margin:0 auto}.footer_center[_ngcontent-%COMP%]{width:500px;float:left;text-align:center}.footer[_ngcontent-%COMP%]   h4[_ngcontent-%COMP%]{font-size:30px;font-weight:100;margin-top:70px}.footer[_ngcontent-%COMP%]   h4[_ngcontent-%COMP%], .footer[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]{font-family:Helvetica;margin-left:40px}",
              ],
            })),
            e
          );
        })(),
        fl = (() => {
          class e {}
          return (
            (e.ɵmod = et({ type: e })),
            (e.ɵinj = re({
              factory: function (t) {
                return new (t || e)();
              },
              providers: [],
              imports: [[Xo, ll]],
            })),
            e
          );
        })(),
        ml = (() => {
          class e {
            constructor() {}
            ngOnInit() {}
          }
          return (
            (e.ɵfac = function (t) {
              return new (t || e)();
            }),
            (e.ɵcmp = Qe({
              type: e,
              selectors: [["lib-navbar"]],
              decls: 11,
              vars: 0,
              consts: [
                [1, "topnav"],
                ["href", "#"],
                [2, "float", "right"],
                ["href", "#", 2, "float", "right"],
              ],
              template: function (e, t) {
                1 & e &&
                  (rs(0, "div", 0),
                  rs(1, "a", 1),
                  ls(2, "Link 1"),
                  ss(),
                  rs(3, "a", 1),
                  ls(4, "Link 2"),
                  ss(),
                  rs(5, "a", 1),
                  ls(6, "Link 3"),
                  ss(),
                  rs(7, "button", 2),
                  ls(8, "Click Me"),
                  ss(),
                  rs(9, "a", 3),
                  ls(10, "Link 4"),
                  ss(),
                  ss());
              },
              styles: [
                ".topnav[_ngcontent-%COMP%]{overflow:hidden;background-color:#333}.topnav[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]{float:left;display:block;color:#f2f2f2;text-align:center;padding:14px 16px;text-decoration:none}.topnav[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:hover{background-color:#ddd;color:#000}@media screen and (max-width:400px){.topnav[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]{float:none;width:100%}}.topnav[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]{border:none;outline:none;padding:8px 15px;border-radius:50px;margin:5.75px 10px;color:#333;background:#fff;box-shadow:0 3px 20px 0 rgba(0,0,0,.23)}",
              ],
            })),
            e
          );
        })(),
        yl = (() => {
          class e {}
          return (
            (e.ɵmod = et({ type: e })),
            (e.ɵinj = re({
              factory: function (t) {
                return new (t || e)();
              },
              imports: [[Xo]],
            })),
            e
          );
        })(),
        gl = (() => {
          class e {
            constructor() {}
            ngOnInit() {}
          }
          return (
            (e.ɵfac = function (t) {
              return new (t || e)();
            }),
            (e.ɵcmp = Qe({
              type: e,
              selectors: [["lib-header"]],
              decls: 11,
              vars: 0,
              consts: [[1, "overlay"]],
              template: function (e, t) {
                1 & e &&
                  (rs(0, "header"),
                  rs(1, "div", 0),
                  rs(2, "h1"),
                  ls(3, "Simply The Best"),
                  ss(),
                  rs(4, "h3"),
                  ls(5, "Reasons for Choosing US"),
                  ss(),
                  rs(6, "p"),
                  ls(
                    7,
                    " Lorem, ipsum dolor sit amet consectetur adipisicing elit. Vero nostrum quis, odio veniam itaque ullam debitis qui magnam consequatur ab. Vero nostrum quis, odio veniam itaque ullam debitis qui magnam consequatur ab. "
                  ),
                  ss(),
                  os(8, "br"),
                  rs(9, "button"),
                  ls(10, "READ MORE"),
                  ss(),
                  ss(),
                  ss());
              },
              styles: [
                '@import url("https://fonts.googleapis.com/css2?family=Dancing+Script:wght@562&display=swap");*[_ngcontent-%COMP%]{padding:0;margin:0;box-sizing:border-box}body[_ngcontent-%COMP%]{height:900px}header[_ngcontent-%COMP%]{text-align:center;width:100%;height:auto;background-size:cover;background-attachment:fixed;position:relative;overflow:hidden;border-radius:0 0 85% 85%/30%}header[_ngcontent-%COMP%]   .overlay[_ngcontent-%COMP%]{width:100%;height:100%;padding:50px;color:#fff;text-shadow:1px 1px 1px #333;background-image:linear-gradient(135deg,rgba(159,5,255,.41) 10%,rgba(253,94,8,.42))}h1[_ngcontent-%COMP%]{font-family:Dancing Script,cursive;font-size:80px}h1[_ngcontent-%COMP%], h3[_ngcontent-%COMP%], p[_ngcontent-%COMP%]{margin-bottom:30px}h3[_ngcontent-%COMP%], p[_ngcontent-%COMP%]{font-family:Open Sans,sans-serif}button[_ngcontent-%COMP%]{border:none;outline:none;padding:10px 20px;border-radius:50px;color:#333;background:#fff;margin-bottom:50px;box-shadow:0 3px 20px 0 rgba(0,0,0,.23)}button[_ngcontent-%COMP%]:hover{cursor:pointer}',
              ],
            })),
            e
          );
        })();
      const _l = (() => {
          const e = Element.prototype;
          return (
            e.matches ||
            e.matchesSelector ||
            e.mozMatchesSelector ||
            e.msMatchesSelector ||
            e.oMatchesSelector ||
            e.webkitMatchesSelector
          );
        })(),
        bl = {
          schedule(e, t) {
            const n = setTimeout(e, t);
            return () => clearTimeout(n);
          },
          scheduleBeforeRender(e) {
            if ("undefined" == typeof window) return bl.schedule(e, 0);
            if (void 0 === window.requestAnimationFrame)
              return bl.schedule(e, 16);
            const t = window.requestAnimationFrame(e);
            return () => window.cancelAnimationFrame(t);
          },
        };
      function vl(e, t, n) {
        let r = n;
        return (
          (function (e) {
            return !!e && e.nodeType === Node.ELEMENT_NODE;
          })(e) &&
            t.some(
              (t, n) =>
                !(
                  "*" === t ||
                  !(function (e, t) {
                    return _l.call(e, t);
                  })(e, t) ||
                  ((r = n), 0)
                )
            ),
          r
        );
      }
      class wl {
        constructor(e, t) {
          (this.component = e),
            (this.injector = t),
            (this.componentFactory = t.get(ds).resolveComponentFactory(e));
        }
        create(e) {
          return new Cl(this.componentFactory, e);
        }
      }
      class Cl {
        constructor(e, t) {
          (this.componentFactory = e),
            (this.injector = t),
            (this.inputChanges = null),
            (this.implementsOnChanges = !1),
            (this.scheduledChangeDetectionFn = null),
            (this.scheduledDestroyFn = null),
            (this.initialInputValues = new Map()),
            (this.unchangedInputs = new Set());
        }
        connect(e) {
          if (null !== this.scheduledDestroyFn)
            return (
              this.scheduledDestroyFn(), void (this.scheduledDestroyFn = null)
            );
          this.componentRef || this.initializeComponent(e);
        }
        disconnect() {
          this.componentRef &&
            null === this.scheduledDestroyFn &&
            (this.scheduledDestroyFn = bl.schedule(() => {
              this.componentRef &&
                (this.componentRef.destroy(), (this.componentRef = null));
            }, 10));
        }
        getInputValue(e) {
          return this.componentRef
            ? this.componentRef.instance[e]
            : this.initialInputValues.get(e);
        }
        setInputValue(e, t) {
          var n, r;
          this.componentRef
            ? (((n = t) !== (r = this.getInputValue(e)) &&
                (n == n || r == r)) ||
                (void 0 === t && this.unchangedInputs.has(e))) &&
              (this.recordInputChange(e, t),
              (this.componentRef.instance[e] = t),
              this.scheduleDetectChanges())
            : this.initialInputValues.set(e, t);
        }
        initializeComponent(e) {
          const t = qr.create({ providers: [], parent: this.injector }),
            n = (function (e, t) {
              const n = e.childNodes,
                r = t.map(() => []);
              let s = -1;
              t.some((e, t) => "*" === e && ((s = t), !0));
              for (let o = 0, i = n.length; o < i; ++o) {
                const e = n[o],
                  i = vl(e, t, s);
                -1 !== i && r[i].push(e);
              }
              return r;
            })(e, this.componentFactory.ngContentSelectors);
          (this.componentRef = this.componentFactory.create(t, n, e)),
            (this.implementsOnChanges =
              "function" == typeof this.componentRef.instance.ngOnChanges),
            this.initializeInputs(),
            this.initializeOutputs(),
            this.detectChanges(),
            this.injector.get(Lo).attachView(this.componentRef.hostView);
        }
        initializeInputs() {
          this.componentFactory.inputs.forEach(({ propName: e }) => {
            this.implementsOnChanges && this.unchangedInputs.add(e),
              this.initialInputValues.has(e) &&
                this.setInputValue(e, this.initialInputValues.get(e));
          }),
            this.initialInputValues.clear();
        }
        initializeOutputs() {
          const e = this.componentFactory.outputs.map(
            ({ propName: e, templateName: t }) =>
              this.componentRef.instance[e].pipe(
                N((e) => ({ name: t, value: e }))
              )
          );
          this.events = L(...e);
        }
        callNgOnChanges() {
          if (!this.implementsOnChanges || null === this.inputChanges) return;
          const e = this.inputChanges;
          (this.inputChanges = null), this.componentRef.instance.ngOnChanges(e);
        }
        scheduleDetectChanges() {
          this.scheduledChangeDetectionFn ||
            (this.scheduledChangeDetectionFn = bl.scheduleBeforeRender(() => {
              (this.scheduledChangeDetectionFn = null), this.detectChanges();
            }));
        }
        recordInputChange(e, t) {
          if (this.componentRef && !this.implementsOnChanges) return;
          null === this.inputChanges && (this.inputChanges = {});
          const n = this.inputChanges[e];
          if (n) return void (n.currentValue = t);
          const r = this.unchangedInputs.has(e);
          this.unchangedInputs.delete(e);
          const s = r ? void 0 : this.getInputValue(e);
          this.inputChanges[e] = new cs(s, t, r);
        }
        detectChanges() {
          this.componentRef &&
            (this.callNgOnChanges(),
            this.componentRef.changeDetectorRef.detectChanges());
        }
      }
      class xl extends HTMLElement {
        constructor() {
          super(...arguments), (this.ngElementEventsSubscription = null);
        }
      }
      function El(e, t) {
        const n = (function (e, t) {
            return t.get(ds).resolveComponentFactory(e).inputs;
          })(e, t.injector),
          r = t.strategyFactory || new wl(e, t.injector),
          s = (function (e) {
            const t = {};
            return (
              e.forEach(({ propName: e, templateName: n }) => {
                var r;
                t[
                  ((r = n), r.replace(/[A-Z]/g, (e) => `-${e.toLowerCase()}`))
                ] = e;
              }),
              t
            );
          })(n);
        class o extends xl {
          constructor(e) {
            super(), (this.ngElementStrategy = r.create(e || t.injector));
          }
          attributeChangedCallback(e, n, o, i) {
            this.ngElementStrategy ||
              (this.ngElementStrategy = r.create(t.injector)),
              this.ngElementStrategy.setInputValue(s[e], o);
          }
          connectedCallback() {
            this.ngElementStrategy ||
              (this.ngElementStrategy = r.create(t.injector)),
              this.ngElementStrategy.connect(this),
              (this.ngElementEventsSubscription = this.ngElementStrategy.events.subscribe(
                (e) => {
                  const t = (function (e, t, n) {
                    if ("function" != typeof CustomEvent) {
                      const r = e.createEvent("CustomEvent");
                      return r.initCustomEvent(t, !1, !1, n), r;
                    }
                    return new CustomEvent(t, {
                      bubbles: !1,
                      cancelable: !1,
                      detail: n,
                    });
                  })(this.ownerDocument, e.name, e.value);
                  this.dispatchEvent(t);
                }
              ));
          }
          disconnectedCallback() {
            this.ngElementStrategy && this.ngElementStrategy.disconnect(),
              this.ngElementEventsSubscription &&
                (this.ngElementEventsSubscription.unsubscribe(),
                (this.ngElementEventsSubscription = null));
          }
        }
        return (
          (o.observedAttributes = Object.keys(s)),
          n
            .map(({ propName: e }) => e)
            .forEach((e) => {
              Object.defineProperty(o.prototype, e, {
                get: function () {
                  return this.ngElementStrategy.getInputValue(e);
                },
                set: function (t) {
                  this.ngElementStrategy.setInputValue(e, t);
                },
                configurable: !0,
                enumerable: !0,
              });
            }),
          o
        );
      }
      let kl = (() => {
        class e {
          constructor(e) {
            const t = El(hl, { injector: e }),
              n = El(cl, { injector: e }),
              r = El(pl, { injector: e }),
              s = El(ml, { injector: e }),
              o = El(gl, { injector: e });
            customElements.define("ele-hello", t),
              customElements.define("ele-button", n),
              customElements.define("ele-footer", r),
              customElements.define("ele-navbar", s),
              customElements.define("ele-header", o);
          }
          ngDoBootstrap() {}
        }
        return (
          (e.ɵmod = et({ type: e, bootstrap: [Si] })),
          (e.ɵinj = re({
            factory: function (t) {
              return new (t || e)(Ve(qr));
            },
            providers: [],
            imports: [[Ti, ul, al, dl, fl, yl]],
          })),
          e
        );
      })();
      (function () {
        if (wn)
          throw new Error("Cannot enable prod mode after platform setup.");
        vn = !1;
      })(),
        Ei()
          .bootstrapModule(kl)
          .catch((e) => console.error(e));
    },
    zn8P: function (e, t) {
      function n(e) {
        return Promise.resolve().then(function () {
          var t = new Error("Cannot find module '" + e + "'");
          throw ((t.code = "MODULE_NOT_FOUND"), t);
        });
      }
      (n.keys = function () {
        return [];
      }),
        (n.resolve = n),
        (e.exports = n),
        (n.id = "zn8P");
    },
  },
  [[0, 0]],
]);
(window.webpackJsonp = window.webpackJsonp || []).push([
  [2],
  {
    2: function (e, t, n) {
      e.exports = n("hN/g");
    },
    "hN/g": function (e, t, n) {
      "use strict";
      n.r(t), n("nf2o");
    },
    nf2o: function (e, t, n) {
      var o, r;
      void 0 ===
        (r =
          "function" ==
          typeof (o = function () {
            "use strict";
            !(function (e) {
              const t = e.performance;
              function n(e) {
                t && t.mark && t.mark(e);
              }
              function o(e, n) {
                t && t.measure && t.measure(e, n);
              }
              n("Zone");
              const r = e.__Zone_symbol_prefix || "__zone_symbol__";
              function s(e) {
                return r + e;
              }
              const a = !0 === e[s("forceDuplicateZoneCheck")];
              if (e.Zone) {
                if (a || "function" != typeof e.Zone.__symbol__)
                  throw new Error("Zone already loaded.");
                return e.Zone;
              }
              class i {
                constructor(e, t) {
                  (this._parent = e),
                    (this._name = t ? t.name || "unnamed" : "<root>"),
                    (this._properties = (t && t.properties) || {}),
                    (this._zoneDelegate = new l(
                      this,
                      this._parent && this._parent._zoneDelegate,
                      t
                    ));
                }
                static assertZonePatched() {
                  if (e.Promise !== C.ZoneAwarePromise)
                    throw new Error(
                      "Zone.js has detected that ZoneAwarePromise `(window|global).Promise` has been overwritten.\nMost likely cause is that a Promise polyfill has been loaded after Zone.js (Polyfilling Promise api is not necessary when zone.js is loaded. If you must load one, do so before loading zone.js.)"
                    );
                }
                static get root() {
                  let e = i.current;
                  for (; e.parent; ) e = e.parent;
                  return e;
                }
                static get current() {
                  return z.zone;
                }
                static get currentTask() {
                  return j;
                }
                static __load_patch(t, r) {
                  if (C.hasOwnProperty(t)) {
                    if (a) throw Error("Already loaded patch: " + t);
                  } else if (!e["__Zone_disable_" + t]) {
                    const s = "Zone:" + t;
                    n(s), (C[t] = r(e, i, O)), o(s, s);
                  }
                }
                get parent() {
                  return this._parent;
                }
                get name() {
                  return this._name;
                }
                get(e) {
                  const t = this.getZoneWith(e);
                  if (t) return t._properties[e];
                }
                getZoneWith(e) {
                  let t = this;
                  for (; t; ) {
                    if (t._properties.hasOwnProperty(e)) return t;
                    t = t._parent;
                  }
                  return null;
                }
                fork(e) {
                  if (!e) throw new Error("ZoneSpec required!");
                  return this._zoneDelegate.fork(this, e);
                }
                wrap(e, t) {
                  if ("function" != typeof e)
                    throw new Error("Expecting function got: " + e);
                  const n = this._zoneDelegate.intercept(this, e, t),
                    o = this;
                  return function () {
                    return o.runGuarded(n, this, arguments, t);
                  };
                }
                run(e, t, n, o) {
                  z = { parent: z, zone: this };
                  try {
                    return this._zoneDelegate.invoke(this, e, t, n, o);
                  } finally {
                    z = z.parent;
                  }
                }
                runGuarded(e, t = null, n, o) {
                  z = { parent: z, zone: this };
                  try {
                    try {
                      return this._zoneDelegate.invoke(this, e, t, n, o);
                    } catch (r) {
                      if (this._zoneDelegate.handleError(this, r)) throw r;
                    }
                  } finally {
                    z = z.parent;
                  }
                }
                runTask(e, t, n) {
                  if (e.zone != this)
                    throw new Error(
                      "A task can only be run in the zone of creation! (Creation: " +
                        (e.zone || y).name +
                        "; Execution: " +
                        this.name +
                        ")"
                    );
                  if (e.state === v && (e.type === P || e.type === D)) return;
                  const o = e.state != E;
                  o && e._transitionTo(E, T), e.runCount++;
                  const r = j;
                  (j = e), (z = { parent: z, zone: this });
                  try {
                    e.type == D &&
                      e.data &&
                      !e.data.isPeriodic &&
                      (e.cancelFn = void 0);
                    try {
                      return this._zoneDelegate.invokeTask(this, e, t, n);
                    } catch (s) {
                      if (this._zoneDelegate.handleError(this, s)) throw s;
                    }
                  } finally {
                    e.state !== v &&
                      e.state !== Z &&
                      (e.type == P || (e.data && e.data.isPeriodic)
                        ? o && e._transitionTo(T, E)
                        : ((e.runCount = 0),
                          this._updateTaskCount(e, -1),
                          o && e._transitionTo(v, E, v))),
                      (z = z.parent),
                      (j = r);
                  }
                }
                scheduleTask(e) {
                  if (e.zone && e.zone !== this) {
                    let t = this;
                    for (; t; ) {
                      if (t === e.zone)
                        throw Error(
                          `can not reschedule task to ${this.name} which is descendants of the original zone ${e.zone.name}`
                        );
                      t = t.parent;
                    }
                  }
                  e._transitionTo(b, v);
                  const t = [];
                  (e._zoneDelegates = t), (e._zone = this);
                  try {
                    e = this._zoneDelegate.scheduleTask(this, e);
                  } catch (n) {
                    throw (
                      (e._transitionTo(Z, b, v),
                      this._zoneDelegate.handleError(this, n),
                      n)
                    );
                  }
                  return (
                    e._zoneDelegates === t && this._updateTaskCount(e, 1),
                    e.state == b && e._transitionTo(T, b),
                    e
                  );
                }
                scheduleMicroTask(e, t, n, o) {
                  return this.scheduleTask(new u(S, e, t, n, o, void 0));
                }
                scheduleMacroTask(e, t, n, o, r) {
                  return this.scheduleTask(new u(D, e, t, n, o, r));
                }
                scheduleEventTask(e, t, n, o, r) {
                  return this.scheduleTask(new u(P, e, t, n, o, r));
                }
                cancelTask(e) {
                  if (e.zone != this)
                    throw new Error(
                      "A task can only be cancelled in the zone of creation! (Creation: " +
                        (e.zone || y).name +
                        "; Execution: " +
                        this.name +
                        ")"
                    );
                  e._transitionTo(w, T, E);
                  try {
                    this._zoneDelegate.cancelTask(this, e);
                  } catch (t) {
                    throw (
                      (e._transitionTo(Z, w),
                      this._zoneDelegate.handleError(this, t),
                      t)
                    );
                  }
                  return (
                    this._updateTaskCount(e, -1),
                    e._transitionTo(v, w),
                    (e.runCount = 0),
                    e
                  );
                }
                _updateTaskCount(e, t) {
                  const n = e._zoneDelegates;
                  -1 == t && (e._zoneDelegates = null);
                  for (let o = 0; o < n.length; o++)
                    n[o]._updateTaskCount(e.type, t);
                }
              }
              i.__symbol__ = s;
              const c = {
                name: "",
                onHasTask: (e, t, n, o) => e.hasTask(n, o),
                onScheduleTask: (e, t, n, o) => e.scheduleTask(n, o),
                onInvokeTask: (e, t, n, o, r, s) => e.invokeTask(n, o, r, s),
                onCancelTask: (e, t, n, o) => e.cancelTask(n, o),
              };
              class l {
                constructor(e, t, n) {
                  (this._taskCounts = {
                    microTask: 0,
                    macroTask: 0,
                    eventTask: 0,
                  }),
                    (this.zone = e),
                    (this._parentDelegate = t),
                    (this._forkZS = n && (n && n.onFork ? n : t._forkZS)),
                    (this._forkDlgt = n && (n.onFork ? t : t._forkDlgt)),
                    (this._forkCurrZone =
                      n && (n.onFork ? this.zone : t._forkCurrZone)),
                    (this._interceptZS =
                      n && (n.onIntercept ? n : t._interceptZS)),
                    (this._interceptDlgt =
                      n && (n.onIntercept ? t : t._interceptDlgt)),
                    (this._interceptCurrZone =
                      n && (n.onIntercept ? this.zone : t._interceptCurrZone)),
                    (this._invokeZS = n && (n.onInvoke ? n : t._invokeZS)),
                    (this._invokeDlgt = n && (n.onInvoke ? t : t._invokeDlgt)),
                    (this._invokeCurrZone =
                      n && (n.onInvoke ? this.zone : t._invokeCurrZone)),
                    (this._handleErrorZS =
                      n && (n.onHandleError ? n : t._handleErrorZS)),
                    (this._handleErrorDlgt =
                      n && (n.onHandleError ? t : t._handleErrorDlgt)),
                    (this._handleErrorCurrZone =
                      n &&
                      (n.onHandleError ? this.zone : t._handleErrorCurrZone)),
                    (this._scheduleTaskZS =
                      n && (n.onScheduleTask ? n : t._scheduleTaskZS)),
                    (this._scheduleTaskDlgt =
                      n && (n.onScheduleTask ? t : t._scheduleTaskDlgt)),
                    (this._scheduleTaskCurrZone =
                      n &&
                      (n.onScheduleTask ? this.zone : t._scheduleTaskCurrZone)),
                    (this._invokeTaskZS =
                      n && (n.onInvokeTask ? n : t._invokeTaskZS)),
                    (this._invokeTaskDlgt =
                      n && (n.onInvokeTask ? t : t._invokeTaskDlgt)),
                    (this._invokeTaskCurrZone =
                      n &&
                      (n.onInvokeTask ? this.zone : t._invokeTaskCurrZone)),
                    (this._cancelTaskZS =
                      n && (n.onCancelTask ? n : t._cancelTaskZS)),
                    (this._cancelTaskDlgt =
                      n && (n.onCancelTask ? t : t._cancelTaskDlgt)),
                    (this._cancelTaskCurrZone =
                      n &&
                      (n.onCancelTask ? this.zone : t._cancelTaskCurrZone)),
                    (this._hasTaskZS = null),
                    (this._hasTaskDlgt = null),
                    (this._hasTaskDlgtOwner = null),
                    (this._hasTaskCurrZone = null);
                  const o = n && n.onHasTask;
                  (o || (t && t._hasTaskZS)) &&
                    ((this._hasTaskZS = o ? n : c),
                    (this._hasTaskDlgt = t),
                    (this._hasTaskDlgtOwner = this),
                    (this._hasTaskCurrZone = e),
                    n.onScheduleTask ||
                      ((this._scheduleTaskZS = c),
                      (this._scheduleTaskDlgt = t),
                      (this._scheduleTaskCurrZone = this.zone)),
                    n.onInvokeTask ||
                      ((this._invokeTaskZS = c),
                      (this._invokeTaskDlgt = t),
                      (this._invokeTaskCurrZone = this.zone)),
                    n.onCancelTask ||
                      ((this._cancelTaskZS = c),
                      (this._cancelTaskDlgt = t),
                      (this._cancelTaskCurrZone = this.zone)));
                }
                fork(e, t) {
                  return this._forkZS
                    ? this._forkZS.onFork(this._forkDlgt, this.zone, e, t)
                    : new i(e, t);
                }
                intercept(e, t, n) {
                  return this._interceptZS
                    ? this._interceptZS.onIntercept(
                        this._interceptDlgt,
                        this._interceptCurrZone,
                        e,
                        t,
                        n
                      )
                    : t;
                }
                invoke(e, t, n, o, r) {
                  return this._invokeZS
                    ? this._invokeZS.onInvoke(
                        this._invokeDlgt,
                        this._invokeCurrZone,
                        e,
                        t,
                        n,
                        o,
                        r
                      )
                    : t.apply(n, o);
                }
                handleError(e, t) {
                  return (
                    !this._handleErrorZS ||
                    this._handleErrorZS.onHandleError(
                      this._handleErrorDlgt,
                      this._handleErrorCurrZone,
                      e,
                      t
                    )
                  );
                }
                scheduleTask(e, t) {
                  let n = t;
                  if (this._scheduleTaskZS)
                    this._hasTaskZS &&
                      n._zoneDelegates.push(this._hasTaskDlgtOwner),
                      (n = this._scheduleTaskZS.onScheduleTask(
                        this._scheduleTaskDlgt,
                        this._scheduleTaskCurrZone,
                        e,
                        t
                      )),
                      n || (n = t);
                  else if (t.scheduleFn) t.scheduleFn(t);
                  else {
                    if (t.type != S)
                      throw new Error("Task is missing scheduleFn.");
                    k(t);
                  }
                  return n;
                }
                invokeTask(e, t, n, o) {
                  return this._invokeTaskZS
                    ? this._invokeTaskZS.onInvokeTask(
                        this._invokeTaskDlgt,
                        this._invokeTaskCurrZone,
                        e,
                        t,
                        n,
                        o
                      )
                    : t.callback.apply(n, o);
                }
                cancelTask(e, t) {
                  let n;
                  if (this._cancelTaskZS)
                    n = this._cancelTaskZS.onCancelTask(
                      this._cancelTaskDlgt,
                      this._cancelTaskCurrZone,
                      e,
                      t
                    );
                  else {
                    if (!t.cancelFn) throw Error("Task is not cancelable");
                    n = t.cancelFn(t);
                  }
                  return n;
                }
                hasTask(e, t) {
                  try {
                    this._hasTaskZS &&
                      this._hasTaskZS.onHasTask(
                        this._hasTaskDlgt,
                        this._hasTaskCurrZone,
                        e,
                        t
                      );
                  } catch (n) {
                    this.handleError(e, n);
                  }
                }
                _updateTaskCount(e, t) {
                  const n = this._taskCounts,
                    o = n[e],
                    r = (n[e] = o + t);
                  if (r < 0)
                    throw new Error("More tasks executed then were scheduled.");
                  (0 != o && 0 != r) ||
                    this.hasTask(this.zone, {
                      microTask: n.microTask > 0,
                      macroTask: n.macroTask > 0,
                      eventTask: n.eventTask > 0,
                      change: e,
                    });
                }
              }
              class u {
                constructor(t, n, o, r, s, a) {
                  if (
                    ((this._zone = null),
                    (this.runCount = 0),
                    (this._zoneDelegates = null),
                    (this._state = "notScheduled"),
                    (this.type = t),
                    (this.source = n),
                    (this.data = r),
                    (this.scheduleFn = s),
                    (this.cancelFn = a),
                    !o)
                  )
                    throw new Error("callback is not defined");
                  this.callback = o;
                  const i = this;
                  this.invoke =
                    t === P && r && r.useG
                      ? u.invokeTask
                      : function () {
                          return u.invokeTask.call(e, i, this, arguments);
                        };
                }
                static invokeTask(e, t, n) {
                  e || (e = this), I++;
                  try {
                    return e.runCount++, e.zone.runTask(e, t, n);
                  } finally {
                    1 == I && m(), I--;
                  }
                }
                get zone() {
                  return this._zone;
                }
                get state() {
                  return this._state;
                }
                cancelScheduleRequest() {
                  this._transitionTo(v, b);
                }
                _transitionTo(e, t, n) {
                  if (this._state !== t && this._state !== n)
                    throw new Error(
                      `${this.type} '${
                        this.source
                      }': can not transition to '${e}', expecting state '${t}'${
                        n ? " or '" + n + "'" : ""
                      }, was '${this._state}'.`
                    );
                  (this._state = e), e == v && (this._zoneDelegates = null);
                }
                toString() {
                  return this.data && void 0 !== this.data.handleId
                    ? this.data.handleId.toString()
                    : Object.prototype.toString.call(this);
                }
                toJSON() {
                  return {
                    type: this.type,
                    state: this.state,
                    source: this.source,
                    zone: this.zone.name,
                    runCount: this.runCount,
                  };
                }
              }
              const h = s("setTimeout"),
                p = s("Promise"),
                f = s("then");
              let d,
                g = [],
                _ = !1;
              function k(t) {
                if (0 === I && 0 === g.length)
                  if ((d || (e[p] && (d = e[p].resolve(0))), d)) {
                    let e = d[f];
                    e || (e = d.then), e.call(d, m);
                  } else e[h](m, 0);
                t && g.push(t);
              }
              function m() {
                if (!_) {
                  for (_ = !0; g.length; ) {
                    const t = g;
                    g = [];
                    for (let n = 0; n < t.length; n++) {
                      const o = t[n];
                      try {
                        o.zone.runTask(o, null, null);
                      } catch (e) {
                        O.onUnhandledError(e);
                      }
                    }
                  }
                  O.microtaskDrainDone(), (_ = !1);
                }
              }
              const y = { name: "NO ZONE" },
                v = "notScheduled",
                b = "scheduling",
                T = "scheduled",
                E = "running",
                w = "canceling",
                Z = "unknown",
                S = "microTask",
                D = "macroTask",
                P = "eventTask",
                C = {},
                O = {
                  symbol: s,
                  currentZoneFrame: () => z,
                  onUnhandledError: R,
                  microtaskDrainDone: R,
                  scheduleMicroTask: k,
                  showUncaughtError: () =>
                    !i[s("ignoreConsoleErrorUncaughtError")],
                  patchEventTarget: () => [],
                  patchOnProperties: R,
                  patchMethod: () => R,
                  bindArguments: () => [],
                  patchThen: () => R,
                  patchMacroTask: () => R,
                  setNativePromise: (e) => {
                    e && "function" == typeof e.resolve && (d = e.resolve(0));
                  },
                  patchEventPrototype: () => R,
                  isIEOrEdge: () => !1,
                  getGlobalObjects: () => {},
                  ObjectDefineProperty: () => R,
                  ObjectGetOwnPropertyDescriptor: () => {},
                  ObjectCreate: () => {},
                  ArraySlice: () => [],
                  patchClass: () => R,
                  wrapWithCurrentZone: () => R,
                  filterProperties: () => [],
                  attachOriginToPatched: () => R,
                  _redefineProperty: () => R,
                  patchCallbacks: () => R,
                };
              let z = { parent: null, zone: new i(null, null) },
                j = null,
                I = 0;
              function R() {}
              o("Zone", "Zone"), (e.Zone = i);
            })(
              ("undefined" != typeof window && window) ||
                ("undefined" != typeof self && self) ||
                global
            ),
              Zone.__load_patch("ZoneAwarePromise", (e, t, n) => {
                const o = Object.getOwnPropertyDescriptor,
                  r = Object.defineProperty,
                  s = n.symbol,
                  a = [],
                  i =
                    !0 === e[s("DISABLE_WRAPPING_UNCAUGHT_PROMISE_REJECTION")],
                  c = s("Promise"),
                  l = s("then");
                (n.onUnhandledError = (e) => {
                  if (n.showUncaughtError()) {
                    const t = e && e.rejection;
                    t
                      ? console.error(
                          "Unhandled Promise rejection:",
                          t instanceof Error ? t.message : t,
                          "; Zone:",
                          e.zone.name,
                          "; Task:",
                          e.task && e.task.source,
                          "; Value:",
                          t,
                          t instanceof Error ? t.stack : void 0
                        )
                      : console.error(e);
                  }
                }),
                  (n.microtaskDrainDone = () => {
                    for (; a.length; ) {
                      const t = a.shift();
                      try {
                        t.zone.runGuarded(() => {
                          throw t;
                        });
                      } catch (e) {
                        h(e);
                      }
                    }
                  });
                const u = s("unhandledPromiseRejectionHandler");
                function h(e) {
                  n.onUnhandledError(e);
                  try {
                    const n = t[u];
                    "function" == typeof n && n.call(this, e);
                  } catch (o) {}
                }
                function p(e) {
                  return e && e.then;
                }
                function f(e) {
                  return e;
                }
                function d(e) {
                  return D.reject(e);
                }
                const g = s("state"),
                  _ = s("value"),
                  k = s("finally"),
                  m = s("parentPromiseValue"),
                  y = s("parentPromiseState");
                function v(e, t) {
                  return (n) => {
                    try {
                      T(e, t, n);
                    } catch (o) {
                      T(e, !1, o);
                    }
                  };
                }
                const b = s("currentTaskTrace");
                function T(e, o, s) {
                  const c = (function () {
                    let e = !1;
                    return function (t) {
                      return function () {
                        e || ((e = !0), t.apply(null, arguments));
                      };
                    };
                  })();
                  if (e === s)
                    throw new TypeError("Promise resolved with itself");
                  if (null === e[g]) {
                    let h = null;
                    try {
                      ("object" != typeof s && "function" != typeof s) ||
                        (h = s && s.then);
                    } catch (u) {
                      return (
                        c(() => {
                          T(e, !1, u);
                        })(),
                        e
                      );
                    }
                    if (
                      !1 !== o &&
                      s instanceof D &&
                      s.hasOwnProperty(g) &&
                      s.hasOwnProperty(_) &&
                      null !== s[g]
                    )
                      w(s), T(e, s[g], s[_]);
                    else if (!1 !== o && "function" == typeof h)
                      try {
                        h.call(s, c(v(e, o)), c(v(e, !1)));
                      } catch (u) {
                        c(() => {
                          T(e, !1, u);
                        })();
                      }
                    else {
                      e[g] = o;
                      const c = e[_];
                      if (
                        ((e[_] = s),
                        e[k] === k &&
                          !0 === o &&
                          ((e[g] = e[y]), (e[_] = e[m])),
                        !1 === o && s instanceof Error)
                      ) {
                        const e =
                          t.currentTask &&
                          t.currentTask.data &&
                          t.currentTask.data.__creationTrace__;
                        e &&
                          r(s, b, {
                            configurable: !0,
                            enumerable: !1,
                            writable: !0,
                            value: e,
                          });
                      }
                      for (let t = 0; t < c.length; )
                        Z(e, c[t++], c[t++], c[t++], c[t++]);
                      if (0 == c.length && 0 == o) {
                        e[g] = 0;
                        let o = s;
                        if (!i)
                          try {
                            throw new Error(
                              "Uncaught (in promise): " +
                                ((l = s) &&
                                l.toString === Object.prototype.toString
                                  ? ((l.constructor && l.constructor.name) ||
                                      "") +
                                    ": " +
                                    JSON.stringify(l)
                                  : l
                                  ? l.toString()
                                  : Object.prototype.toString.call(l)) +
                                (s && s.stack ? "\n" + s.stack : "")
                            );
                          } catch (u) {
                            o = u;
                          }
                        (o.rejection = s),
                          (o.promise = e),
                          (o.zone = t.current),
                          (o.task = t.currentTask),
                          a.push(o),
                          n.scheduleMicroTask();
                      }
                    }
                  }
                  var l;
                  return e;
                }
                const E = s("rejectionHandledHandler");
                function w(e) {
                  if (0 === e[g]) {
                    try {
                      const n = t[E];
                      n &&
                        "function" == typeof n &&
                        n.call(this, { rejection: e[_], promise: e });
                    } catch (n) {}
                    e[g] = !1;
                    for (let t = 0; t < a.length; t++)
                      e === a[t].promise && a.splice(t, 1);
                  }
                }
                function Z(e, t, n, o, r) {
                  w(e);
                  const s = e[g],
                    a = s
                      ? "function" == typeof o
                        ? o
                        : f
                      : "function" == typeof r
                      ? r
                      : d;
                  t.scheduleMicroTask(
                    "Promise.then",
                    () => {
                      try {
                        const o = e[_],
                          r = !!n && k === n[k];
                        r && ((n[m] = o), (n[y] = s));
                        const i = t.run(
                          a,
                          void 0,
                          r && a !== d && a !== f ? [] : [o]
                        );
                        T(n, !0, i);
                      } catch (o) {
                        T(n, !1, o);
                      }
                    },
                    n
                  );
                }
                const S = function () {};
                class D {
                  static toString() {
                    return "function ZoneAwarePromise() { [native code] }";
                  }
                  static resolve(e) {
                    return T(new this(null), !0, e);
                  }
                  static reject(e) {
                    return T(new this(null), !1, e);
                  }
                  static race(e) {
                    let t,
                      n,
                      o = new this((e, o) => {
                        (t = e), (n = o);
                      });
                    function r(e) {
                      t(e);
                    }
                    function s(e) {
                      n(e);
                    }
                    for (let a of e)
                      p(a) || (a = this.resolve(a)), a.then(r, s);
                    return o;
                  }
                  static all(e) {
                    return D.allWithCallback(e);
                  }
                  static allSettled(e) {
                    return (this && this.prototype instanceof D
                      ? this
                      : D
                    ).allWithCallback(e, {
                      thenCallback: (e) => ({ status: "fulfilled", value: e }),
                      errorCallback: (e) => ({ status: "rejected", reason: e }),
                    });
                  }
                  static allWithCallback(e, t) {
                    let n,
                      o,
                      r = new this((e, t) => {
                        (n = e), (o = t);
                      }),
                      s = 2,
                      a = 0;
                    const i = [];
                    for (let l of e) {
                      p(l) || (l = this.resolve(l));
                      const e = a;
                      try {
                        l.then(
                          (o) => {
                            (i[e] = t ? t.thenCallback(o) : o),
                              s--,
                              0 === s && n(i);
                          },
                          (r) => {
                            t
                              ? ((i[e] = t.errorCallback(r)),
                                s--,
                                0 === s && n(i))
                              : o(r);
                          }
                        );
                      } catch (c) {
                        o(c);
                      }
                      s++, a++;
                    }
                    return (s -= 2), 0 === s && n(i), r;
                  }
                  constructor(e) {
                    const t = this;
                    if (!(t instanceof D))
                      throw new Error("Must be an instanceof Promise.");
                    (t[g] = null), (t[_] = []);
                    try {
                      e && e(v(t, !0), v(t, !1));
                    } catch (n) {
                      T(t, !1, n);
                    }
                  }
                  get [Symbol.toStringTag]() {
                    return "Promise";
                  }
                  get [Symbol.species]() {
                    return D;
                  }
                  then(e, n) {
                    let o = this.constructor[Symbol.species];
                    (o && "function" == typeof o) ||
                      (o = this.constructor || D);
                    const r = new o(S),
                      s = t.current;
                    return (
                      null == this[g]
                        ? this[_].push(s, r, e, n)
                        : Z(this, s, r, e, n),
                      r
                    );
                  }
                  catch(e) {
                    return this.then(null, e);
                  }
                  finally(e) {
                    let n = this.constructor[Symbol.species];
                    (n && "function" == typeof n) || (n = D);
                    const o = new n(S);
                    o[k] = k;
                    const r = t.current;
                    return (
                      null == this[g]
                        ? this[_].push(r, o, e, e)
                        : Z(this, r, o, e, e),
                      o
                    );
                  }
                }
                (D.resolve = D.resolve),
                  (D.reject = D.reject),
                  (D.race = D.race),
                  (D.all = D.all);
                const P = (e[c] = e.Promise),
                  C = t.__symbol__("ZoneAwarePromise");
                let O = o(e, "Promise");
                (O && !O.configurable) ||
                  (O && delete O.writable,
                  O && delete O.value,
                  O || (O = { configurable: !0, enumerable: !0 }),
                  (O.get = function () {
                    return e[C] ? e[C] : e[c];
                  }),
                  (O.set = function (t) {
                    t === D
                      ? (e[C] = t)
                      : ((e[c] = t),
                        t.prototype[l] || j(t),
                        n.setNativePromise(t));
                  }),
                  r(e, "Promise", O)),
                  (e.Promise = D);
                const z = s("thenPatched");
                function j(e) {
                  const t = e.prototype,
                    n = o(t, "then");
                  if (n && (!1 === n.writable || !n.configurable)) return;
                  const r = t.then;
                  (t[l] = r),
                    (e.prototype.then = function (e, t) {
                      return new D((e, t) => {
                        r.call(this, e, t);
                      }).then(e, t);
                    }),
                    (e[z] = !0);
                }
                if (((n.patchThen = j), P)) {
                  j(P);
                  const t = e.fetch;
                  "function" == typeof t &&
                    ((e[n.symbol("fetch")] = t),
                    (e.fetch =
                      ((I = t),
                      function () {
                        let e = I.apply(this, arguments);
                        if (e instanceof D) return e;
                        let t = e.constructor;
                        return t[z] || j(t), e;
                      })));
                }
                var I;
                return (Promise[t.__symbol__("uncaughtPromiseErrors")] = a), D;
              });
            const e = Object.getOwnPropertyDescriptor,
              t = Object.defineProperty,
              n = Object.getPrototypeOf,
              o = Object.create,
              r = Array.prototype.slice,
              s = Zone.__symbol__("addEventListener"),
              a = Zone.__symbol__("removeEventListener"),
              i = Zone.__symbol__("");
            function c(e, t) {
              return Zone.current.wrap(e, t);
            }
            function l(e, t, n, o, r) {
              return Zone.current.scheduleMacroTask(e, t, n, o, r);
            }
            const u = Zone.__symbol__,
              h = "undefined" != typeof window,
              p = h ? window : void 0,
              f = (h && p) || ("object" == typeof self && self) || global,
              d = [null];
            function g(e, t) {
              for (let n = e.length - 1; n >= 0; n--)
                "function" == typeof e[n] && (e[n] = c(e[n], t + "_" + n));
              return e;
            }
            function _(e) {
              return (
                !e ||
                (!1 !== e.writable &&
                  !("function" == typeof e.get && void 0 === e.set))
              );
            }
            const k =
                "undefined" != typeof WorkerGlobalScope &&
                self instanceof WorkerGlobalScope,
              m =
                !("nw" in f) &&
                void 0 !== f.process &&
                "[object process]" === {}.toString.call(f.process),
              y = !m && !k && !(!h || !p.HTMLElement),
              v =
                void 0 !== f.process &&
                "[object process]" === {}.toString.call(f.process) &&
                !k &&
                !(!h || !p.HTMLElement),
              b = {},
              T = function (e) {
                if (!(e = e || f.event)) return;
                let t = b[e.type];
                t || (t = b[e.type] = u("ON_PROPERTY" + e.type));
                const n = this || e.target || f,
                  o = n[t];
                let r;
                if (y && n === p && "error" === e.type) {
                  const t = e;
                  (r =
                    o &&
                    o.call(
                      this,
                      t.message,
                      t.filename,
                      t.lineno,
                      t.colno,
                      t.error
                    )),
                    !0 === r && e.preventDefault();
                } else
                  (r = o && o.apply(this, arguments)),
                    null == r || r || e.preventDefault();
                return r;
              };
            function E(n, o, r) {
              let s = e(n, o);
              if (
                (!s &&
                  r &&
                  e(r, o) &&
                  (s = { enumerable: !0, configurable: !0 }),
                !s || !s.configurable)
              )
                return;
              const a = u("on" + o + "patched");
              if (n.hasOwnProperty(a) && n[a]) return;
              delete s.writable, delete s.value;
              const i = s.get,
                c = s.set,
                l = o.substr(2);
              let h = b[l];
              h || (h = b[l] = u("ON_PROPERTY" + l)),
                (s.set = function (e) {
                  let t = this;
                  t || n !== f || (t = f),
                    t &&
                      (t[h] && t.removeEventListener(l, T),
                      c && c.apply(t, d),
                      "function" == typeof e
                        ? ((t[h] = e), t.addEventListener(l, T, !1))
                        : (t[h] = null));
                }),
                (s.get = function () {
                  let e = this;
                  if ((e || n !== f || (e = f), !e)) return null;
                  const t = e[h];
                  if (t) return t;
                  if (i) {
                    let t = i && i.call(this);
                    if (t)
                      return (
                        s.set.call(this, t),
                        "function" == typeof e.removeAttribute &&
                          e.removeAttribute(o),
                        t
                      );
                  }
                  return null;
                }),
                t(n, o, s),
                (n[a] = !0);
            }
            function w(e, t, n) {
              if (t) for (let o = 0; o < t.length; o++) E(e, "on" + t[o], n);
              else {
                const t = [];
                for (const n in e) "on" == n.substr(0, 2) && t.push(n);
                for (let o = 0; o < t.length; o++) E(e, t[o], n);
              }
            }
            const Z = u("originalInstance");
            function S(e) {
              const n = f[e];
              if (!n) return;
              (f[u(e)] = n),
                (f[e] = function () {
                  const t = g(arguments, e);
                  switch (t.length) {
                    case 0:
                      this[Z] = new n();
                      break;
                    case 1:
                      this[Z] = new n(t[0]);
                      break;
                    case 2:
                      this[Z] = new n(t[0], t[1]);
                      break;
                    case 3:
                      this[Z] = new n(t[0], t[1], t[2]);
                      break;
                    case 4:
                      this[Z] = new n(t[0], t[1], t[2], t[3]);
                      break;
                    default:
                      throw new Error("Arg list too long.");
                  }
                }),
                C(f[e], n);
              const o = new n(function () {});
              let r;
              for (r in o)
                ("XMLHttpRequest" === e && "responseBlob" === r) ||
                  (function (n) {
                    "function" == typeof o[n]
                      ? (f[e].prototype[n] = function () {
                          return this[Z][n].apply(this[Z], arguments);
                        })
                      : t(f[e].prototype, n, {
                          set: function (t) {
                            "function" == typeof t
                              ? ((this[Z][n] = c(t, e + "." + n)),
                                C(this[Z][n], t))
                              : (this[Z][n] = t);
                          },
                          get: function () {
                            return this[Z][n];
                          },
                        });
                  })(r);
              for (r in n)
                "prototype" !== r && n.hasOwnProperty(r) && (f[e][r] = n[r]);
            }
            function D(t, o, r) {
              let s = t;
              for (; s && !s.hasOwnProperty(o); ) s = n(s);
              !s && t[o] && (s = t);
              const a = u(o);
              let i = null;
              if (s && !(i = s[a]) && ((i = s[a] = s[o]), _(s && e(s, o)))) {
                const e = r(i, a, o);
                (s[o] = function () {
                  return e(this, arguments);
                }),
                  C(s[o], i);
              }
              return i;
            }
            function P(e, t, n) {
              let o = null;
              function r(e) {
                const t = e.data;
                return (
                  (t.args[t.cbIdx] = function () {
                    e.invoke.apply(this, arguments);
                  }),
                  o.apply(t.target, t.args),
                  e
                );
              }
              o = D(
                e,
                t,
                (e) =>
                  function (t, o) {
                    const s = n(t, o);
                    return s.cbIdx >= 0 && "function" == typeof o[s.cbIdx]
                      ? l(s.name, o[s.cbIdx], s, r)
                      : e.apply(t, o);
                  }
              );
            }
            function C(e, t) {
              e[u("OriginalDelegate")] = t;
            }
            let O = !1,
              z = !1;
            function j() {
              try {
                const e = p.navigator.userAgent;
                if (-1 !== e.indexOf("MSIE ") || -1 !== e.indexOf("Trident/"))
                  return !0;
              } catch (e) {}
              return !1;
            }
            function I() {
              if (O) return z;
              O = !0;
              try {
                const e = p.navigator.userAgent;
                (-1 === e.indexOf("MSIE ") &&
                  -1 === e.indexOf("Trident/") &&
                  -1 === e.indexOf("Edge/")) ||
                  (z = !0);
              } catch (e) {}
              return z;
            }
            Zone.__load_patch("toString", (e) => {
              const t = Function.prototype.toString,
                n = u("OriginalDelegate"),
                o = u("Promise"),
                r = u("Error"),
                s = function () {
                  if ("function" == typeof this) {
                    const s = this[n];
                    if (s)
                      return "function" == typeof s
                        ? t.call(s)
                        : Object.prototype.toString.call(s);
                    if (this === Promise) {
                      const n = e[o];
                      if (n) return t.call(n);
                    }
                    if (this === Error) {
                      const n = e[r];
                      if (n) return t.call(n);
                    }
                  }
                  return t.call(this);
                };
              (s[n] = t), (Function.prototype.toString = s);
              const a = Object.prototype.toString;
              Object.prototype.toString = function () {
                return this instanceof Promise
                  ? "[object Promise]"
                  : a.call(this);
              };
            });
            let R = !1;
            if ("undefined" != typeof window)
              try {
                const e = Object.defineProperty({}, "passive", {
                  get: function () {
                    R = !0;
                  },
                });
                window.addEventListener("test", e, e),
                  window.removeEventListener("test", e, e);
              } catch (ie) {
                R = !1;
              }
            const N = { useG: !0 },
              x = {},
              L = {},
              M = new RegExp("^" + i + "(\\w+)(true|false)$"),
              A = u("propagationStopped");
            function H(e, t) {
              const n = (t ? t(e) : e) + "false",
                o = (t ? t(e) : e) + "true",
                r = i + n,
                s = i + o;
              (x[e] = {}), (x[e].false = r), (x[e].true = s);
            }
            function F(e, t, o) {
              const r = (o && o.add) || "addEventListener",
                s = (o && o.rm) || "removeEventListener",
                a = (o && o.listeners) || "eventListeners",
                c = (o && o.rmAll) || "removeAllListeners",
                l = u(r),
                h = "." + r + ":",
                p = function (e, t, n) {
                  if (e.isRemoved) return;
                  const o = e.callback;
                  "object" == typeof o &&
                    o.handleEvent &&
                    ((e.callback = (e) => o.handleEvent(e)),
                    (e.originalDelegate = o)),
                    e.invoke(e, t, [n]);
                  const r = e.options;
                  r &&
                    "object" == typeof r &&
                    r.once &&
                    t[s].call(
                      t,
                      n.type,
                      e.originalDelegate ? e.originalDelegate : e.callback,
                      r
                    );
                },
                f = function (t) {
                  if (!(t = t || e.event)) return;
                  const n = this || t.target || e,
                    o = n[x[t.type].false];
                  if (o)
                    if (1 === o.length) p(o[0], n, t);
                    else {
                      const e = o.slice();
                      for (let o = 0; o < e.length && (!t || !0 !== t[A]); o++)
                        p(e[o], n, t);
                    }
                },
                d = function (t) {
                  if (!(t = t || e.event)) return;
                  const n = this || t.target || e,
                    o = n[x[t.type].true];
                  if (o)
                    if (1 === o.length) p(o[0], n, t);
                    else {
                      const e = o.slice();
                      for (let o = 0; o < e.length && (!t || !0 !== t[A]); o++)
                        p(e[o], n, t);
                    }
                };
              function g(t, o) {
                if (!t) return !1;
                let p = !0;
                o && void 0 !== o.useG && (p = o.useG);
                const g = o && o.vh;
                let _ = !0;
                o && void 0 !== o.chkDup && (_ = o.chkDup);
                let k = !1;
                o && void 0 !== o.rt && (k = o.rt);
                let y = t;
                for (; y && !y.hasOwnProperty(r); ) y = n(y);
                if ((!y && t[r] && (y = t), !y)) return !1;
                if (y[l]) return !1;
                const v = o && o.eventNameToString,
                  b = {},
                  T = (y[l] = y[r]),
                  E = (y[u(s)] = y[s]),
                  w = (y[u(a)] = y[a]),
                  Z = (y[u(c)] = y[c]);
                let S;
                function D(e, t) {
                  return !R && "object" == typeof e && e
                    ? !!e.capture
                    : R && t
                    ? "boolean" == typeof e
                      ? { capture: e, passive: !0 }
                      : e
                      ? "object" == typeof e && !1 !== e.passive
                        ? Object.assign(Object.assign({}, e), { passive: !0 })
                        : e
                      : { passive: !0 }
                    : e;
                }
                o && o.prepend && (S = y[u(o.prepend)] = y[o.prepend]);
                const P = p
                    ? function (e) {
                        if (!b.isExisting)
                          return T.call(
                            b.target,
                            b.eventName,
                            b.capture ? d : f,
                            b.options
                          );
                      }
                    : function (e) {
                        return T.call(
                          b.target,
                          b.eventName,
                          e.invoke,
                          b.options
                        );
                      },
                  O = p
                    ? function (e) {
                        if (!e.isRemoved) {
                          const t = x[e.eventName];
                          let n;
                          t && (n = t[e.capture ? "true" : "false"]);
                          const o = n && e.target[n];
                          if (o)
                            for (let r = 0; r < o.length; r++)
                              if (o[r] === e) {
                                o.splice(r, 1),
                                  (e.isRemoved = !0),
                                  0 === o.length &&
                                    ((e.allRemoved = !0), (e.target[n] = null));
                                break;
                              }
                        }
                        if (e.allRemoved)
                          return E.call(
                            e.target,
                            e.eventName,
                            e.capture ? d : f,
                            e.options
                          );
                      }
                    : function (e) {
                        return E.call(
                          e.target,
                          e.eventName,
                          e.invoke,
                          e.options
                        );
                      },
                  z =
                    o && o.diff
                      ? o.diff
                      : function (e, t) {
                          const n = typeof t;
                          return (
                            ("function" === n && e.callback === t) ||
                            ("object" === n && e.originalDelegate === t)
                          );
                        },
                  j = Zone[u("BLACK_LISTED_EVENTS")],
                  I = e[u("PASSIVE_EVENTS")],
                  A = function (t, n, r, s, a = !1, i = !1) {
                    return function () {
                      const c = this || e;
                      let l = arguments[0];
                      o && o.transferEventName && (l = o.transferEventName(l));
                      let u = arguments[1];
                      if (!u) return t.apply(this, arguments);
                      if (m && "uncaughtException" === l)
                        return t.apply(this, arguments);
                      let h = !1;
                      if ("function" != typeof u) {
                        if (!u.handleEvent) return t.apply(this, arguments);
                        h = !0;
                      }
                      if (g && !g(t, u, c, arguments)) return;
                      const f = R && !!I && -1 !== I.indexOf(l),
                        d = D(arguments[2], f);
                      if (j)
                        for (let e = 0; e < j.length; e++)
                          if (l === j[e])
                            return f
                              ? t.call(c, l, u, d)
                              : t.apply(this, arguments);
                      const k = !!d && ("boolean" == typeof d || d.capture),
                        y = !(!d || "object" != typeof d) && d.once,
                        T = Zone.current;
                      let E = x[l];
                      E || (H(l, v), (E = x[l]));
                      const w = E[k ? "true" : "false"];
                      let Z,
                        S = c[w],
                        P = !1;
                      if (S) {
                        if (((P = !0), _))
                          for (let e = 0; e < S.length; e++)
                            if (z(S[e], u)) return;
                      } else S = c[w] = [];
                      const C = c.constructor.name,
                        O = L[C];
                      O && (Z = O[l]),
                        Z || (Z = C + n + (v ? v(l) : l)),
                        (b.options = d),
                        y && (b.options.once = !1),
                        (b.target = c),
                        (b.capture = k),
                        (b.eventName = l),
                        (b.isExisting = P);
                      const M = p ? N : void 0;
                      M && (M.taskData = b);
                      const A = T.scheduleEventTask(Z, u, M, r, s);
                      return (
                        (b.target = null),
                        M && (M.taskData = null),
                        y && (d.once = !0),
                        (R || "boolean" != typeof A.options) && (A.options = d),
                        (A.target = c),
                        (A.capture = k),
                        (A.eventName = l),
                        h && (A.originalDelegate = u),
                        i ? S.unshift(A) : S.push(A),
                        a ? c : void 0
                      );
                    };
                  };
                return (
                  (y[r] = A(T, h, P, O, k)),
                  S &&
                    (y.prependListener = A(
                      S,
                      ".prependListener:",
                      function (e) {
                        return S.call(
                          b.target,
                          b.eventName,
                          e.invoke,
                          b.options
                        );
                      },
                      O,
                      k,
                      !0
                    )),
                  (y[s] = function () {
                    const t = this || e;
                    let n = arguments[0];
                    o && o.transferEventName && (n = o.transferEventName(n));
                    const r = arguments[2],
                      s = !!r && ("boolean" == typeof r || r.capture),
                      a = arguments[1];
                    if (!a) return E.apply(this, arguments);
                    if (g && !g(E, a, t, arguments)) return;
                    const c = x[n];
                    let l;
                    c && (l = c[s ? "true" : "false"]);
                    const u = l && t[l];
                    if (u)
                      for (let e = 0; e < u.length; e++) {
                        const o = u[e];
                        if (z(o, a))
                          return (
                            u.splice(e, 1),
                            (o.isRemoved = !0),
                            0 === u.length &&
                              ((o.allRemoved = !0),
                              (t[l] = null),
                              "string" == typeof n) &&
                              (t[i + "ON_PROPERTY" + n] = null),
                            o.zone.cancelTask(o),
                            k ? t : void 0
                          );
                      }
                    return E.apply(this, arguments);
                  }),
                  (y[a] = function () {
                    const t = this || e;
                    let n = arguments[0];
                    o && o.transferEventName && (n = o.transferEventName(n));
                    const r = [],
                      s = G(t, v ? v(n) : n);
                    for (let e = 0; e < s.length; e++) {
                      const t = s[e];
                      r.push(
                        t.originalDelegate ? t.originalDelegate : t.callback
                      );
                    }
                    return r;
                  }),
                  (y[c] = function () {
                    const t = this || e;
                    let n = arguments[0];
                    if (n) {
                      o && o.transferEventName && (n = o.transferEventName(n));
                      const e = x[n];
                      if (e) {
                        const o = t[e.false],
                          r = t[e.true];
                        if (o) {
                          const e = o.slice();
                          for (let t = 0; t < e.length; t++) {
                            const o = e[t];
                            this[s].call(
                              this,
                              n,
                              o.originalDelegate
                                ? o.originalDelegate
                                : o.callback,
                              o.options
                            );
                          }
                        }
                        if (r) {
                          const e = r.slice();
                          for (let t = 0; t < e.length; t++) {
                            const o = e[t];
                            this[s].call(
                              this,
                              n,
                              o.originalDelegate
                                ? o.originalDelegate
                                : o.callback,
                              o.options
                            );
                          }
                        }
                      }
                    } else {
                      const e = Object.keys(t);
                      for (let t = 0; t < e.length; t++) {
                        const n = M.exec(e[t]);
                        let o = n && n[1];
                        o && "removeListener" !== o && this[c].call(this, o);
                      }
                      this[c].call(this, "removeListener");
                    }
                    if (k) return this;
                  }),
                  C(y[r], T),
                  C(y[s], E),
                  Z && C(y[c], Z),
                  w && C(y[a], w),
                  !0
                );
              }
              let _ = [];
              for (let n = 0; n < t.length; n++) _[n] = g(t[n], o);
              return _;
            }
            function G(e, t) {
              if (!t) {
                const n = [];
                for (let o in e) {
                  const r = M.exec(o);
                  let s = r && r[1];
                  if (s && (!t || s === t)) {
                    const t = e[o];
                    if (t) for (let e = 0; e < t.length; e++) n.push(t[e]);
                  }
                }
                return n;
              }
              let n = x[t];
              n || (H(t), (n = x[t]));
              const o = e[n.false],
                r = e[n.true];
              return o ? (r ? o.concat(r) : o.slice()) : r ? r.slice() : [];
            }
            function B(e, t) {
              const n = e.Event;
              n &&
                n.prototype &&
                t.patchMethod(
                  n.prototype,
                  "stopImmediatePropagation",
                  (e) =>
                    function (t, n) {
                      (t[A] = !0), e && e.apply(t, n);
                    }
                );
            }
            function q(e, t, n, o, r) {
              const s = Zone.__symbol__(o);
              if (t[s]) return;
              const a = (t[s] = t[o]);
              (t[o] = function (s, i, c) {
                return (
                  i &&
                    i.prototype &&
                    r.forEach(function (t) {
                      const r = `${n}.${o}::` + t,
                        s = i.prototype;
                      if (s.hasOwnProperty(t)) {
                        const n = e.ObjectGetOwnPropertyDescriptor(s, t);
                        n && n.value
                          ? ((n.value = e.wrapWithCurrentZone(n.value, r)),
                            e._redefineProperty(i.prototype, t, n))
                          : s[t] && (s[t] = e.wrapWithCurrentZone(s[t], r));
                      } else s[t] && (s[t] = e.wrapWithCurrentZone(s[t], r));
                    }),
                  a.call(t, s, i, c)
                );
              }),
                e.attachOriginToPatched(t[o], a);
            }
            const W = [
                "absolutedeviceorientation",
                "afterinput",
                "afterprint",
                "appinstalled",
                "beforeinstallprompt",
                "beforeprint",
                "beforeunload",
                "devicelight",
                "devicemotion",
                "deviceorientation",
                "deviceorientationabsolute",
                "deviceproximity",
                "hashchange",
                "languagechange",
                "message",
                "mozbeforepaint",
                "offline",
                "online",
                "paint",
                "pageshow",
                "pagehide",
                "popstate",
                "rejectionhandled",
                "storage",
                "unhandledrejection",
                "unload",
                "userproximity",
                "vrdisplayconnected",
                "vrdisplaydisconnected",
                "vrdisplaypresentchange",
              ],
              U = [
                "encrypted",
                "waitingforkey",
                "msneedkey",
                "mozinterruptbegin",
                "mozinterruptend",
              ],
              V = ["load"],
              $ = [
                "blur",
                "error",
                "focus",
                "load",
                "resize",
                "scroll",
                "messageerror",
              ],
              X = ["bounce", "finish", "start"],
              J = [
                "loadstart",
                "progress",
                "abort",
                "error",
                "load",
                "progress",
                "timeout",
                "loadend",
                "readystatechange",
              ],
              Y = [
                "upgradeneeded",
                "complete",
                "abort",
                "success",
                "error",
                "blocked",
                "versionchange",
                "close",
              ],
              K = ["close", "error", "open", "message"],
              Q = ["error", "message"],
              ee = [
                "abort",
                "animationcancel",
                "animationend",
                "animationiteration",
                "auxclick",
                "beforeinput",
                "blur",
                "cancel",
                "canplay",
                "canplaythrough",
                "change",
                "compositionstart",
                "compositionupdate",
                "compositionend",
                "cuechange",
                "click",
                "close",
                "contextmenu",
                "curechange",
                "dblclick",
                "drag",
                "dragend",
                "dragenter",
                "dragexit",
                "dragleave",
                "dragover",
                "drop",
                "durationchange",
                "emptied",
                "ended",
                "error",
                "focus",
                "focusin",
                "focusout",
                "gotpointercapture",
                "input",
                "invalid",
                "keydown",
                "keypress",
                "keyup",
                "load",
                "loadstart",
                "loadeddata",
                "loadedmetadata",
                "lostpointercapture",
                "mousedown",
                "mouseenter",
                "mouseleave",
                "mousemove",
                "mouseout",
                "mouseover",
                "mouseup",
                "mousewheel",
                "orientationchange",
                "pause",
                "play",
                "playing",
                "pointercancel",
                "pointerdown",
                "pointerenter",
                "pointerleave",
                "pointerlockchange",
                "mozpointerlockchange",
                "webkitpointerlockerchange",
                "pointerlockerror",
                "mozpointerlockerror",
                "webkitpointerlockerror",
                "pointermove",
                "pointout",
                "pointerover",
                "pointerup",
                "progress",
                "ratechange",
                "reset",
                "resize",
                "scroll",
                "seeked",
                "seeking",
                "select",
                "selectionchange",
                "selectstart",
                "show",
                "sort",
                "stalled",
                "submit",
                "suspend",
                "timeupdate",
                "volumechange",
                "touchcancel",
                "touchmove",
                "touchstart",
                "touchend",
                "transitioncancel",
                "transitionend",
                "waiting",
                "wheel",
              ].concat(
                [
                  "webglcontextrestored",
                  "webglcontextlost",
                  "webglcontextcreationerror",
                ],
                ["autocomplete", "autocompleteerror"],
                ["toggle"],
                [
                  "afterscriptexecute",
                  "beforescriptexecute",
                  "DOMContentLoaded",
                  "freeze",
                  "fullscreenchange",
                  "mozfullscreenchange",
                  "webkitfullscreenchange",
                  "msfullscreenchange",
                  "fullscreenerror",
                  "mozfullscreenerror",
                  "webkitfullscreenerror",
                  "msfullscreenerror",
                  "readystatechange",
                  "visibilitychange",
                  "resume",
                ],
                W,
                [
                  "beforecopy",
                  "beforecut",
                  "beforepaste",
                  "copy",
                  "cut",
                  "paste",
                  "dragstart",
                  "loadend",
                  "animationstart",
                  "search",
                  "transitionrun",
                  "transitionstart",
                  "webkitanimationend",
                  "webkitanimationiteration",
                  "webkitanimationstart",
                  "webkittransitionend",
                ],
                [
                  "activate",
                  "afterupdate",
                  "ariarequest",
                  "beforeactivate",
                  "beforedeactivate",
                  "beforeeditfocus",
                  "beforeupdate",
                  "cellchange",
                  "controlselect",
                  "dataavailable",
                  "datasetchanged",
                  "datasetcomplete",
                  "errorupdate",
                  "filterchange",
                  "layoutcomplete",
                  "losecapture",
                  "move",
                  "moveend",
                  "movestart",
                  "propertychange",
                  "resizeend",
                  "resizestart",
                  "rowenter",
                  "rowexit",
                  "rowsdelete",
                  "rowsinserted",
                  "command",
                  "compassneedscalibration",
                  "deactivate",
                  "help",
                  "mscontentzoom",
                  "msmanipulationstatechanged",
                  "msgesturechange",
                  "msgesturedoubletap",
                  "msgestureend",
                  "msgesturehold",
                  "msgesturestart",
                  "msgesturetap",
                  "msgotpointercapture",
                  "msinertiastart",
                  "mslostpointercapture",
                  "mspointercancel",
                  "mspointerdown",
                  "mspointerenter",
                  "mspointerhover",
                  "mspointerleave",
                  "mspointermove",
                  "mspointerout",
                  "mspointerover",
                  "mspointerup",
                  "pointerout",
                  "mssitemodejumplistitemremoved",
                  "msthumbnailclick",
                  "stop",
                  "storagecommit",
                ]
              );
            function te(e, t, n) {
              if (!n || 0 === n.length) return t;
              const o = n.filter((t) => t.target === e);
              if (!o || 0 === o.length) return t;
              const r = o[0].ignoreProperties;
              return t.filter((e) => -1 === r.indexOf(e));
            }
            function ne(e, t, n, o) {
              e && w(e, te(e, t, n), o);
            }
            function oe(e, t) {
              if (m && !v) return;
              if (Zone[e.symbol("patchEvents")]) return;
              const o = "undefined" != typeof WebSocket,
                r = t.__Zone_ignore_on_properties;
              if (y) {
                const e = window,
                  t = j ? [{ target: e, ignoreProperties: ["error"] }] : [];
                ne(e, ee.concat(["messageerror"]), r ? r.concat(t) : r, n(e)),
                  ne(Document.prototype, ee, r),
                  void 0 !== e.SVGElement && ne(e.SVGElement.prototype, ee, r),
                  ne(Element.prototype, ee, r),
                  ne(HTMLElement.prototype, ee, r),
                  ne(HTMLMediaElement.prototype, U, r),
                  ne(HTMLFrameSetElement.prototype, W.concat($), r),
                  ne(HTMLBodyElement.prototype, W.concat($), r),
                  ne(HTMLFrameElement.prototype, V, r),
                  ne(HTMLIFrameElement.prototype, V, r);
                const o = e.HTMLMarqueeElement;
                o && ne(o.prototype, X, r);
                const s = e.Worker;
                s && ne(s.prototype, Q, r);
              }
              const s = t.XMLHttpRequest;
              s && ne(s.prototype, J, r);
              const a = t.XMLHttpRequestEventTarget;
              a && ne(a && a.prototype, J, r),
                "undefined" != typeof IDBIndex &&
                  (ne(IDBIndex.prototype, Y, r),
                  ne(IDBRequest.prototype, Y, r),
                  ne(IDBOpenDBRequest.prototype, Y, r),
                  ne(IDBDatabase.prototype, Y, r),
                  ne(IDBTransaction.prototype, Y, r),
                  ne(IDBCursor.prototype, Y, r)),
                o && ne(WebSocket.prototype, K, r);
            }
            Zone.__load_patch("util", (n, s, a) => {
              (a.patchOnProperties = w),
                (a.patchMethod = D),
                (a.bindArguments = g),
                (a.patchMacroTask = P);
              const l = s.__symbol__("BLACK_LISTED_EVENTS"),
                u = s.__symbol__("UNPATCHED_EVENTS");
              n[u] && (n[l] = n[u]),
                n[l] && (s[l] = s[u] = n[l]),
                (a.patchEventPrototype = B),
                (a.patchEventTarget = F),
                (a.isIEOrEdge = I),
                (a.ObjectDefineProperty = t),
                (a.ObjectGetOwnPropertyDescriptor = e),
                (a.ObjectCreate = o),
                (a.ArraySlice = r),
                (a.patchClass = S),
                (a.wrapWithCurrentZone = c),
                (a.filterProperties = te),
                (a.attachOriginToPatched = C),
                (a._redefineProperty = Object.defineProperty),
                (a.patchCallbacks = q),
                (a.getGlobalObjects = () => ({
                  globalSources: L,
                  zoneSymbolEventNames: x,
                  eventNames: ee,
                  isBrowser: y,
                  isMix: v,
                  isNode: m,
                  TRUE_STR: "true",
                  FALSE_STR: "false",
                  ZONE_SYMBOL_PREFIX: i,
                  ADD_EVENT_LISTENER_STR: "addEventListener",
                  REMOVE_EVENT_LISTENER_STR: "removeEventListener",
                }));
            });
            const re = u("zoneTask");
            function se(e, t, n, o) {
              let r = null,
                s = null;
              n += o;
              const a = {};
              function i(t) {
                const n = t.data;
                return (
                  (n.args[0] = function () {
                    try {
                      t.invoke.apply(this, arguments);
                    } finally {
                      (t.data && t.data.isPeriodic) ||
                        ("number" == typeof n.handleId
                          ? delete a[n.handleId]
                          : n.handleId && (n.handleId[re] = null));
                    }
                  }),
                  (n.handleId = r.apply(e, n.args)),
                  t
                );
              }
              function c(e) {
                return s(e.data.handleId);
              }
              (r = D(
                e,
                (t += o),
                (n) =>
                  function (r, s) {
                    if ("function" == typeof s[0]) {
                      const e = l(
                        t,
                        s[0],
                        {
                          isPeriodic: "Interval" === o,
                          delay:
                            "Timeout" === o || "Interval" === o
                              ? s[1] || 0
                              : void 0,
                          args: s,
                        },
                        i,
                        c
                      );
                      if (!e) return e;
                      const n = e.data.handleId;
                      return (
                        "number" == typeof n ? (a[n] = e) : n && (n[re] = e),
                        n &&
                          n.ref &&
                          n.unref &&
                          "function" == typeof n.ref &&
                          "function" == typeof n.unref &&
                          ((e.ref = n.ref.bind(n)),
                          (e.unref = n.unref.bind(n))),
                        "number" == typeof n || n ? n : e
                      );
                    }
                    return n.apply(e, s);
                  }
              )),
                (s = D(
                  e,
                  n,
                  (t) =>
                    function (n, o) {
                      const r = o[0];
                      let s;
                      "number" == typeof r
                        ? (s = a[r])
                        : ((s = r && r[re]), s || (s = r)),
                        s && "string" == typeof s.type
                          ? "notScheduled" !== s.state &&
                            ((s.cancelFn && s.data.isPeriodic) ||
                              0 === s.runCount) &&
                            ("number" == typeof r
                              ? delete a[r]
                              : r && (r[re] = null),
                            s.zone.cancelTask(s))
                          : t.apply(e, o);
                    }
                ));
            }
            function ae(e, t) {
              if (Zone[t.symbol("patchEventTarget")]) return;
              const {
                eventNames: n,
                zoneSymbolEventNames: o,
                TRUE_STR: r,
                FALSE_STR: s,
                ZONE_SYMBOL_PREFIX: a,
              } = t.getGlobalObjects();
              for (let c = 0; c < n.length; c++) {
                const e = n[c],
                  t = a + (e + s),
                  i = a + (e + r);
                (o[e] = {}), (o[e][s] = t), (o[e][r] = i);
              }
              const i = e.EventTarget;
              return i && i.prototype
                ? (t.patchEventTarget(e, [i && i.prototype]), !0)
                : void 0;
            }
            Zone.__load_patch("legacy", (e) => {
              const t = e[Zone.__symbol__("legacyPatch")];
              t && t();
            }),
              Zone.__load_patch("timers", (e) => {
                se(e, "set", "clear", "Timeout"),
                  se(e, "set", "clear", "Interval"),
                  se(e, "set", "clear", "Immediate");
              }),
              Zone.__load_patch("requestAnimationFrame", (e) => {
                se(e, "request", "cancel", "AnimationFrame"),
                  se(e, "mozRequest", "mozCancel", "AnimationFrame"),
                  se(e, "webkitRequest", "webkitCancel", "AnimationFrame");
              }),
              Zone.__load_patch("blocking", (e, t) => {
                const n = ["alert", "prompt", "confirm"];
                for (let o = 0; o < n.length; o++)
                  D(
                    e,
                    n[o],
                    (n, o, r) =>
                      function (o, s) {
                        return t.current.run(n, e, s, r);
                      }
                  );
              }),
              Zone.__load_patch("EventTarget", (e, t, n) => {
                !(function (e, t) {
                  t.patchEventPrototype(e, t);
                })(e, n),
                  ae(e, n);
                const o = e.XMLHttpRequestEventTarget;
                o && o.prototype && n.patchEventTarget(e, [o.prototype]),
                  S("MutationObserver"),
                  S("WebKitMutationObserver"),
                  S("IntersectionObserver"),
                  S("FileReader");
              }),
              Zone.__load_patch("on_property", (e, t, n) => {
                oe(n, e);
              }),
              Zone.__load_patch("customElements", (e, t, n) => {
                !(function (e, t) {
                  const { isBrowser: n, isMix: o } = t.getGlobalObjects();
                  (n || o) &&
                    e.customElements &&
                    "customElements" in e &&
                    t.patchCallbacks(
                      t,
                      e.customElements,
                      "customElements",
                      "define",
                      [
                        "connectedCallback",
                        "disconnectedCallback",
                        "adoptedCallback",
                        "attributeChangedCallback",
                      ]
                    );
                })(e, n);
              }),
              Zone.__load_patch("XHR", (e, t) => {
                !(function (e) {
                  const p = e.XMLHttpRequest;
                  if (!p) return;
                  const f = p.prototype;
                  let d = f[s],
                    g = f[a];
                  if (!d) {
                    const t = e.XMLHttpRequestEventTarget;
                    if (t) {
                      const e = t.prototype;
                      (d = e[s]), (g = e[a]);
                    }
                  }
                  function _(e) {
                    const o = e.data,
                      c = o.target;
                    (c[i] = !1), (c[h] = !1);
                    const l = c[r];
                    d || ((d = c[s]), (g = c[a])),
                      l && g.call(c, "readystatechange", l);
                    const u = (c[r] = () => {
                      if (c.readyState === c.DONE)
                        if (!o.aborted && c[i] && "scheduled" === e.state) {
                          const n = c[t.__symbol__("loadfalse")];
                          if (n && n.length > 0) {
                            const r = e.invoke;
                            (e.invoke = function () {
                              const n = c[t.__symbol__("loadfalse")];
                              for (let t = 0; t < n.length; t++)
                                n[t] === e && n.splice(t, 1);
                              o.aborted || "scheduled" !== e.state || r.call(e);
                            }),
                              n.push(e);
                          } else e.invoke();
                        } else o.aborted || !1 !== c[i] || (c[h] = !0);
                    });
                    return (
                      d.call(c, "readystatechange", u),
                      c[n] || (c[n] = e),
                      T.apply(c, o.args),
                      (c[i] = !0),
                      e
                    );
                  }
                  function k() {}
                  function m(e) {
                    const t = e.data;
                    return (t.aborted = !0), E.apply(t.target, t.args);
                  }
                  const y = D(
                      f,
                      "open",
                      () =>
                        function (e, t) {
                          return (
                            (e[o] = 0 == t[2]), (e[c] = t[1]), y.apply(e, t)
                          );
                        }
                    ),
                    v = u("fetchTaskAborting"),
                    b = u("fetchTaskScheduling"),
                    T = D(
                      f,
                      "send",
                      () =>
                        function (e, n) {
                          if (!0 === t.current[b]) return T.apply(e, n);
                          if (e[o]) return T.apply(e, n);
                          {
                            const t = {
                                target: e,
                                url: e[c],
                                isPeriodic: !1,
                                args: n,
                                aborted: !1,
                              },
                              o = l("XMLHttpRequest.send", k, t, _, m);
                            e &&
                              !0 === e[h] &&
                              !t.aborted &&
                              "scheduled" === o.state &&
                              o.invoke();
                          }
                        }
                    ),
                    E = D(
                      f,
                      "abort",
                      () =>
                        function (e, o) {
                          const r = e[n];
                          if (r && "string" == typeof r.type) {
                            if (
                              null == r.cancelFn ||
                              (r.data && r.data.aborted)
                            )
                              return;
                            r.zone.cancelTask(r);
                          } else if (!0 === t.current[v]) return E.apply(e, o);
                        }
                    );
                })(e);
                const n = u("xhrTask"),
                  o = u("xhrSync"),
                  r = u("xhrListener"),
                  i = u("xhrScheduled"),
                  c = u("xhrURL"),
                  h = u("xhrErrorBeforeScheduled");
              }),
              Zone.__load_patch("geolocation", (t) => {
                t.navigator &&
                  t.navigator.geolocation &&
                  (function (t, n) {
                    const o = t.constructor.name;
                    for (let r = 0; r < n.length; r++) {
                      const s = n[r],
                        a = t[s];
                      if (a) {
                        if (!_(e(t, s))) continue;
                        t[s] = ((e) => {
                          const t = function () {
                            return e.apply(this, g(arguments, o + "." + s));
                          };
                          return C(t, e), t;
                        })(a);
                      }
                    }
                  })(t.navigator.geolocation, [
                    "getCurrentPosition",
                    "watchPosition",
                  ]);
              }),
              Zone.__load_patch("PromiseRejectionEvent", (e, t) => {
                function n(t) {
                  return function (n) {
                    G(e, t).forEach((o) => {
                      const r = e.PromiseRejectionEvent;
                      if (r) {
                        const e = new r(t, {
                          promise: n.promise,
                          reason: n.rejection,
                        });
                        o.invoke(e);
                      }
                    });
                  };
                }
                e.PromiseRejectionEvent &&
                  ((t[u("unhandledPromiseRejectionHandler")] = n(
                    "unhandledrejection"
                  )),
                  (t[u("rejectionHandledHandler")] = n("rejectionhandled")));
              });
          })
            ? o.call(t, n, t, e)
            : o) || (e.exports = r);
    },
  },
  [[2, 0]],
]);
!(function (e) {
  function r(r) {
    for (
      var n, l, f = r[0], i = r[1], p = r[2], c = 0, s = [];
      c < f.length;
      c++
    )
      (l = f[c]),
        Object.prototype.hasOwnProperty.call(o, l) && o[l] && s.push(o[l][0]),
        (o[l] = 0);
    for (n in i) Object.prototype.hasOwnProperty.call(i, n) && (e[n] = i[n]);
    for (a && a(r); s.length; ) s.shift()();
    return u.push.apply(u, p || []), t();
  }
  function t() {
    for (var e, r = 0; r < u.length; r++) {
      for (var t = u[r], n = !0, f = 1; f < t.length; f++)
        0 !== o[t[f]] && (n = !1);
      n && (u.splice(r--, 1), (e = l((l.s = t[0]))));
    }
    return e;
  }
  var n = {},
    o = { 0: 0 },
    u = [];
  function l(r) {
    if (n[r]) return n[r].exports;
    var t = (n[r] = { i: r, l: !1, exports: {} });
    return e[r].call(t.exports, t, t.exports, l), (t.l = !0), t.exports;
  }
  (l.m = e),
    (l.c = n),
    (l.d = function (e, r, t) {
      l.o(e, r) || Object.defineProperty(e, r, { enumerable: !0, get: t });
    }),
    (l.r = function (e) {
      "undefined" != typeof Symbol &&
        Symbol.toStringTag &&
        Object.defineProperty(e, Symbol.toStringTag, { value: "Module" }),
        Object.defineProperty(e, "__esModule", { value: !0 });
    }),
    (l.t = function (e, r) {
      if ((1 & r && (e = l(e)), 8 & r)) return e;
      if (4 & r && "object" == typeof e && e && e.__esModule) return e;
      var t = Object.create(null);
      if (
        (l.r(t),
        Object.defineProperty(t, "default", { enumerable: !0, value: e }),
        2 & r && "string" != typeof e)
      )
        for (var n in e)
          l.d(
            t,
            n,
            function (r) {
              return e[r];
            }.bind(null, n)
          );
      return t;
    }),
    (l.n = function (e) {
      var r =
        e && e.__esModule
          ? function () {
              return e.default;
            }
          : function () {
              return e;
            };
      return l.d(r, "a", r), r;
    }),
    (l.o = function (e, r) {
      return Object.prototype.hasOwnProperty.call(e, r);
    }),
    (l.p = "");
  var f = (window.webpackJsonp = window.webpackJsonp || []),
    i = f.push.bind(f);
  (f.push = r), (f = f.slice());
  for (var p = 0; p < f.length; p++) r(f[p]);
  var a = i;
  t();
})([]);
